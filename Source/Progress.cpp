/*
 * Progress.cpp
 *
 *  Created on: Dec 17, 2010
 *      Author: Jarrod Chesney
 */

#include "Progress.h"

namespace orm
{
  namespace v1
  {

    Progress::Progress()
    {
    }

    Progress::~Progress()
    {
    }

    void
    Progress::SetMax(uint32_t max)
    {
    }
    // ----------------------------------------------------------------------------

    void
    Progress::Reset()
    {
    }
    // ----------------------------------------------------------------------------

    void
    Progress::Incriment(uint32_t incriment)
    {
    }
    // ----------------------------------------------------------------------------

    void
    Progress::SetDescription(const std::string desc)
    {
    }
    // ----------------------------------------------------------------------------

    Progress&
    Progress::AddSubProgress()
    {
      boost::shared_ptr< Progress > progress(new Progress());
      mSubProgresses.push_back(progress);
      return *progress.get();
    }
  // ----------------------------------------------------------------------------

  }
}
