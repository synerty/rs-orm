/*
 * Attribute.h
 *
 *  Created on: Dec 17, 2010
 *      Author: Jarrod Chesney
 */

#ifndef _ORM_V1_MAPPED_PROPERTY_H_
#define _ORM_V1_MAPPED_PROPERTY_H_

#include <string>
#include <assert.h>
#include <stdint.h>

#include <boost/lexical_cast.hpp>
#include <boost/format.hpp>

#include <QtCore/qvariant.h>

#include "PropertyBase.h"
#include "PropertyPrivate.h"
#include "Exceptions.h"

namespace orm
{
  namespace v1
  {
    template<typename ModelT, class StorableT, typename UnusedUniqueT,
        typename DbValT, typename CppValT>
      class MappedProperty;

    template<typename ModelT, class StorableT, typename UnusedUniqueT,
        typename DbValT, typename CppValT>
      class MappedProperty : public PropertyBase< ModelT, StorableT,
          UnusedUniqueT, CppValT, MappedProperty< ModelT, StorableT,
              UnusedUniqueT, DbValT, CppValT > >
      {
        public:
          MappedProperty();

          virtual
          ~MappedProperty();

          /*! Setup Static
           * Setup the static variables and return and instance of this
           * @param fieldName
           * @param map
           * @param isPrimaryKey
           * @param isValidIfNull
           * @param defaultValue
           * @return a new instance
           */
          static AttrI::PtrT
          setupStatic(const std::string fieldName,
              const std::map< DbValT, CppValT > map,
              const bool isPrimaryKey = false,
              const bool isValidIfNull = true,
              const boost::optional< CppValT > defaultValue = boost::optional<
                  CppValT >());

        public:
          /// ---------------
          /// Convieniance setter

          void
          setFromMap(const DbValT& value);

          const DbValT&
          getFromMap() const;

        public:
          /// ---------------
          /// AttributeI interface

          virtual bool
          Bind(QSqlQuery& qry, const int& bindIndex);

          virtual bool
          Get(QSqlQuery& qry, int fieldIndex);

        private:

          typedef PropertyBase<
              ModelT,
              StorableT,
              UnusedUniqueT,
              CppValT,
              MappedProperty< ModelT, StorableT, UnusedUniqueT, DbValT, CppValT > >
              BasePropT;

          typedef std::map< DbValT, CppValT > MapT;

          typedef __property_private ::BindGetDelegate< DbValT >
              BindGetDelegateT;

          static std::map< DbValT, CppValT > sMap;

      };
  }
}

//! Adds the orm property to the class
#define ORM_V1_DECLARE_MAPPED_PROPERTY(_get_name_, _name_, _db_type_, _cpp_type_) \
    /* BEGIN _name_ PROPERTY */ \
    private: \
       class __ ## _name_ ## UniqueProp; \
      \
    public: \
       typedef orm::v1::MappedProperty< \
           ModelT \
           , ThisStorableT \
           , __ ## _name_ ## UniqueProp \
           , _db_type_ \
           , _cpp_type_ \
           > _name_ ## PropT; \
       typedef std::map< _db_type_ , _cpp_type_  > _name_ ## PropMapT; \
      \
      virtual _cpp_type_ _get_name_() const; \
      \
      virtual _db_type_ _get_name_ ## FromMap() const; \
      \
      virtual void set ## _name_(const _cpp_type_& value); \
      \
      virtual void set ## _name_ ## FromMap(const _db_type_& value); \
      \
      virtual void set ## _name_ ## Null(); \
      \
      virtual bool is ## _name_ ## Null() const; \
    private: \
      _name_ ## PropT mProp ## _name_; \
    /* END _name_ PROPERTY */

//! Adds the orm property to the class
#define ORM_V1_DECLARE_MAPPED_PROPERTY_INHERITED(_get_name_, _name_, _db_type_, _cpp_type_) \
    /* BEGIN _name_ inherited PROPERTY */ \
    ORM_V1_DECLARE_MAPPED_PROPERTY( _get_name_, _name_, _db_type_, _cpp_type_) \
    /* END _name_ inherited PROPERTY */


#define ORM_V1_DEFINE_MAPPED_PROPERTY(_stable_t_, _get_name_, _name_, _db_type_, _cpp_type_) \
    /* BEGIN _name_ PROPERTY */ \
      \
      _cpp_type_ \
      _stable_t_::_get_name_() const \
      { \
        return mProp ## _name_.get(); \
      } \
      \
      _db_type_ \
      _stable_t_::_get_name_ ## FromMap() const \
      { \
        return mProp ## _name_.getFromMap(); \
      } \
      \
      void \
      _stable_t_::set ## _name_(const _cpp_type_& value) \
      { \
        mProp ## _name_.set(value); \
      } \
      \
      void \
      _stable_t_::set ## _name_ ## FromMap(const _db_type_& value) \
      { \
        mProp ## _name_.setFromMap(value); \
      } \
      \
      void \
      _stable_t_::set ## _name_ ## Null() \
      { \
        mProp ## _name_.setNull(); \
      } \
      \
      bool \
      _stable_t_::is ## _name_ ## Null() const \
      { \
        return mProp ## _name_.isNull(); \
      } \
    /* END _name_ PROPERTY */

//! Adds the orm property to the class
#define ORM_V1_DEFINE_MAPPED_PROPERTY_INHERITED(_storable_t_, _get_name_, _name_, _db_type_, _cpp_type_) \
    /* BEGIN _name_ inherited PROPERTY */ \
      \
      _cpp_type_ \
      _storable_t_::_get_name_() const \
      { \
        return mProp ## _name_.get(); \
      } \
      \
      _db_type_ \
      _storable_t_::_get_name_ ## FromMap() const \
      { \
        return mProp ## _name_.getFromMap(); \
      } \
      \
      void \
      _storable_t_::set ## _name_(const _cpp_type_& value) \
      { \
        BaseStorableT::set ## _name_(value); \
        mProp ## _name_.set(value); \
      } \
      \
      void \
      _storable_t_::set ## _name_ ## FromMap(const _db_type_& value) \
      { \
        BaseStorableT::set ## _name_ ## From Map(value); \
        mProp ## _name_.setFromMap(value); \
      } \
      \
      bool \
      _storable_t_::\is ## _name_ ## Null() const \
      { \
        return mProp ## _name_.isNull(); \
      } \
      \
      void \
      _storable_t_::set ## _name_ ## Null()\
      { \
        BaseStorableT::set ## _name_ ## Null(); \
        mProp ## _name_.setNull(); \
      } \
    /* END _name_ inherited PROPERTY */

#ifndef USE_ORM_V1_DLL
#include "MappedProperty.ini"
#endif

#endif /* _ORM_V1_MAPPED_PROPERTY_H_ */
