/*
 * SqlOpts.h
 *
 *  Created on: Feb 3, 2011
 *      Author: Jarrod Chesney
 */

#ifndef _ORM_V1_SQL_OPTS_H_
#define _ORM_V1_SQL_OPTS_H_

#if defined(BUILD_ORM_V1_DLL)
#define DECL_ORM_V1_DIR __declspec(dllexport)
#elif defined(USE_ORM_V1_DLL)
#define DECL_ORM_V1_DIR __declspec(dllimport)
#else
#define DECL_ORM_V1_DIR
#endif

#include <string>
#include <vector>

#include <boost/weak_ptr.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/enable_shared_from_this.hpp>

/*! orm::SqlOpts Brief Description
 * Long comment about the class
 *
 */
namespace orm
{
  namespace v1
  {
    class SqlOpts;
    /// -----------------------------------------------------------------------

    struct SqlJoin
    {
        typedef std::vector< SqlJoin > ListT;

        enum JoinTypeE
        {
          InnerJoin,
          LeftJoin,
          RightJoin,
          OuterJoin
        };

        // From side
        std::string fromField;
        // NOTE : From dataset is the SqlOpts containing the join struct

        // To side
        boost::weak_ptr< SqlOpts > toSqlOpt;
        std::string toField;

        // Join type between the two sides
        JoinTypeE joinType;

        SqlJoin(const std::string fromField_,
            boost::weak_ptr< SqlOpts > toSqlOpt_,
            const std::string toField_,
            const JoinTypeE joinType_ = InnerJoin);
    };
    /// -----------------------------------------------------------------------

    /*! Sql Options
     * This class contains the information required to construct a result set
     * in SQL. In most cases this is a table or a query.
     */
    class SqlOpts : public boost::enable_shared_from_this< SqlOpts >
    {
      public:
        typedef boost::shared_ptr< SqlOpts > PtrT;
        typedef boost::shared_ptr< SqlOpts > WptrT;
        typedef std::vector< std::string > ConditionListT;

        enum InheritanceTypeE
        {
          NoInheritance,
          ConcreteInheritance,
          JoinedInheritance
        };

      public:
        SqlOpts();
        virtual
        ~SqlOpts();

      public:

        std::string
        name() const;

        /*! Set Descriptive name
         * This name is used for log messages and represents this object in
         * dynamic displays
         * @param name
         */
        void
        setName(const std::string& name);

        std::string
        tableName() const;

        void
        setTableName(const std::string& tableName);

        std::string
        querySql() const;

        void
        setQuerySql(const std::string& querySql);

        /*! Result Set Alias
         * The alias to assign to this table name / query sql in a select query.
         * @return alias
         */
        std::string
        sqlAlias() const;

      public:

        void
        setSelectDistinct(const bool isDistinct);

        bool
        isSelectDistinct() const;

        void
        addCondition(std::string condition);

        /* FIXME, The with clause should really be a list of subqueries
         * This allows the with clause to be built.
         * StorageSql.cpp will need to be coded to accept this format.
         * and StorageSql also deals with recursive SqlOpts so it will
         * be able to deal with that properly.
         */
        void
        setWithClause(std::string with);

        std::string
        withClause() const;

        void
        clearConditions();

        const ConditionListT&
        conditions() const;

        void
        addJoin(const SqlJoin join);

        const SqlJoin::ListT&
        joins() const;

      public:
        /*! Set Ineritance Type
         * Describes the inheritance relationship between this table
         * and the table this talbe inherits from.
         * @param type
         */
        void
        setInheritanceType(const InheritanceTypeE type);

        /*! Ineritance Type
         * Describes the inheritance relationship between this table
         * and the table this talbe inherits from.
         */
        InheritanceTypeE
        inheritanceType() const;

      private:
        bool mSelectDistinct;
        InheritanceTypeE mInheritanceType;

        std::string mName;
        std::string mTableName;
        std::string mQuerySql;
        std::string mSqlAlias;
        std::string mSqlWith;

        ConditionListT mConditions;
        SqlJoin::ListT mJoins;
    };
  /// -----------------------------------------------------------------------

  }
}

#endif /* _ORM_V1_SQL_OPTS_H_ */
