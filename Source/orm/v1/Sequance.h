/*
 * Sequance.h
 *
 *  Created on: Feb 4, 2011
 *      Author: Jarrod Chesney
 */

#ifndef _ORM_V1_SEQUANCE_H_
#define _ORM_V1_SEQUANCE_H_

#if defined(BUILD_ORM_V1_DLL)
#define DECL_ORM_V1_DIR __declspec(dllexport)
#elif defined(USE_ORM_V1_DLL)
#define DECL_ORM_V1_DIR __declspec(dllimport)
#else
#define DECL_ORM_V1_DIR
#endif

#include <string>
#include <stdint.h>

class QSqlDatabase;

/*! orm::v1::Sequance Brief Description
 * Long comment about the class
 *
 */
namespace orm
{
  namespace v1
  {

    class DECL_ORM_V1_DIR Sequance
    {
      public:
        Sequance(const std::string& name, QSqlDatabase& db);
        virtual
        ~Sequance();

      public:
        uint64_t
        next();

      private:
        std::string mName;
        QSqlDatabase& mDb;
    };

  }
}

#endif /* _ORM_V1_SEQUANCE_H_ */
