namespace orm
{
  namespace v1
  {
    /// -----------------------------------------------------------------------
    //            Implementation of RelatedProperty
    /// -----------------------------------------------------------------------
    /// ---------------
    /// Defines to ease the reading of initialisers

#define CLASS_RELATED_PROPERTY  \
    RelatedProperty< \
      ModelT, \
      StorableT, \
      UnusedUniqueT, \
      RelatedStorableT, \
      DbValT >
    // CLASS_RELATED_PROPERTY

    /// -----------------------------------------------------------------------

    /// ---------------
    /// Initialisers
    /// -----------------------------------------------------------------------

    // Initilise pointer to member function variable and assign null
    template<typename ModelT, class StorableT, typename UnusedUniqueT,
        class RelatedStorableT, typename DbValT>
      DbValT // Return
      // Class and variable name
      (RelatedStorableT::*CLASS_RELATED_PROPERTY::sDbValGetter)//
          () // Arguments
          const // Constness of function
          = NULL;
    /// -----------------------------------------------------------------------

    // Initilise pointer to member function variable and assign null
    template<typename ModelT, class StorableT, typename UnusedUniqueT,
        class RelatedStorableT, typename DbValT>
      boost::shared_ptr< RelatedStorableT > // Return
      (ModelT::*CLASS_RELATED_PROPERTY::sObjectGetter) // Class and variable name
          (const DbValT&) // Arguments
          = NULL;
    /// -----------------------------------------------------------------------

    // Initilise pointer to member function variable and assign null
    template<typename ModelT, class StorableT, typename UnusedUniqueT,
        class RelatedStorableT, typename DbValT>
      void // Return
      // Class and variable name
      (RelatedStorableT::*CLASS_RELATED_PROPERTY::sRelatedToManyVisitFunction) //
          (RelatedToManyVisitorT&) // Arguments
          = NULL;
    /// -----------------------------------------------------------------------

    template<typename ModelT, class StorableT, typename UnusedUniqueT,
        class RelatedStorableT, typename DbValT>
      RelatedProperty< ModelT, StorableT, UnusedUniqueT, RelatedStorableT,
          DbValT >::RelatedProperty() :
        mIsNull(true), //
            mIsInitialised(false), //
            mBindGetDelegate(*this), //
            mRelatedToMany(NULL), //
            mStorable(NULL), //
            mStorableConfig(NULL)

      {
      }
    /// -----------------------------------------------------------------------

    template<typename ModelT, class StorableT, typename UnusedUniqueT,
        class RelatedStorableT, typename DbValT>
      RelatedProperty< ModelT, StorableT, UnusedUniqueT, RelatedStorableT,
          DbValT >::~RelatedProperty()
      {
        // Set the value nothing so it will remove its self from the
        // related to many.
        if (mRelatedToMany != NULL)
        {
          assert(mObjectValue.get());
          uintptr_t ptrVal = reinterpret_cast< uintptr_t > (mStorable);
          mRelatedToMany->removeReference(ptrVal);
        }
      }
    /// -----------------------------------------------------------------------

    template<typename ModelT, class StorableT, typename UnusedUniqueT,
        class RelatedStorableT, typename DbValT>
      void
      RelatedProperty< ModelT, StorableT, UnusedUniqueT, RelatedStorableT,
          DbValT >::setup(StorableT* storable)
      {
        assert(storable);
        assert(storable->config());
        mStorable = storable;
        mStorableConfig = storable->config();
        mStorableConfig->addField(this);
      }
    /// -----------------------------------------------------------------------

    template<typename ModelT, class StorableT, typename UnusedUniqueT,
        class RelatedStorableT, typename DbValT>
      AttrI::PtrT
      RelatedProperty< ModelT, StorableT, UnusedUniqueT, RelatedStorableT,
          DbValT >::setupStatic(const std::string fieldName,
          DbValueGetterT dbValGetter,
          ObjectGetterT objectGetter,
          RelatedToManyVisitFunctionT relatedToManyVisitFunction,
          bool isPrimaryKey,
          bool isValidIfNull,
          boost::optional< DbValT > defaultValue)
      {
        std::auto_ptr < ThisT > newThis(new ThisT());
        newThis->mAttrMetaDelegate.setup(fieldName,
            isPrimaryKey,
            isValidIfNull,
            defaultValue);
        sDbValGetter = dbValGetter;
        sObjectGetter = objectGetter;
        sRelatedToManyVisitFunction = relatedToManyVisitFunction;
        return AttrI::PtrT(newThis.release());
      }
    /// -----------------------------------------------------------------------

    template<typename ModelT, class StorableT, typename UnusedUniqueT,
        class RelatedStorableT, typename DbValT>
      const boost::shared_ptr< RelatedStorableT >&
      RelatedProperty< ModelT, StorableT, UnusedUniqueT, RelatedStorableT,
          DbValT >::get() const
      {
        if (!IsValidIfNull() && mIsNull)
          throw AccessingNullValueException(mStorableConfig, FieldName());

        assert( mStorableConfig);
        mStorableConfig->state().lockForRead(); // Make sure its not in use
        mStorableConfig->state().unlock();
        return mObjectValue;
      }
    /// -----------------------------------------------------------------------

    template<typename ModelT, class StorableT, typename UnusedUniqueT,
        class RelatedStorableT, typename DbValT>
      void
      RelatedProperty< ModelT, StorableT, UnusedUniqueT, RelatedStorableT,
          DbValT >::set(const DbValT& value,
          boost::shared_ptr< ModelT > supplementaryModel)
      {

        set(get(value, supplementaryModel));
      }
    /// -----------------------------------------------------------------------

    template<typename ModelT, class StorableT, typename UnusedUniqueT,
        class RelatedStorableT, typename DbValT>
      void
      RelatedProperty< ModelT, StorableT, UnusedUniqueT, RelatedStorableT,
          DbValT >::set(const boost::shared_ptr< RelatedStorableT >& value)
      {
        mIsInitialised = true;

        if (mObjectValue == value)
        {
          mIsNull = (mObjectValue.get() == NULL);
          return;
        }

        assert( mStorableConfig);

        mStorableConfig->state().lockForWrite();

        // Remove old reference
        if (mRelatedToMany != NULL)
        {
          assert(mObjectValue.get());
          uintptr_t ptrVal = reinterpret_cast< uintptr_t > (mStorable);
          mRelatedToMany->removeReference(ptrVal);
        }

        // Actually do that update
        mObjectValue = value;

        // update the back references - related to many thing
        if (mObjectValue.get() != NULL && sRelatedToManyVisitFunction != NULL)
        {
          // Get new property and add new reference
          RelatedToManyVisitorT visitor(mObjectValue,
              sRelatedToManyVisitFunction);
          mRelatedToMany = visitor.relatedToManyProperty;
          mRelatedToMany->addReference(mStorable->shared_from_this());
        }
        else
        {
          mRelatedToMany = NULL;
        }

        mStorableConfig->setUpdated();
        mStorableConfig->state().unlock();

        mIsNull = mObjectValue.get() == NULL;
      }
    /// -----------------------------------------------------------------------

    template<typename ModelT, class StorableT, typename UnusedUniqueT,
        class RelatedStorableT, typename DbValT>
      bool
      RelatedProperty< ModelT, StorableT, UnusedUniqueT, RelatedStorableT,
          DbValT >::isNull() const
      {
        return mIsNull;
      }
    /// -----------------------------------------------------------------------

    template<typename ModelT, class StorableT, typename UnusedUniqueT,
        class RelatedStorableT, typename DbValT>
      void
      RelatedProperty< ModelT, StorableT, UnusedUniqueT, RelatedStorableT,
          DbValT >::setNull()
      {
        mIsNull = true;
      }
    /// -----------------------------------------------------------------------

    template<typename ModelT, class StorableT, typename UnusedUniqueT,
        class RelatedStorableT, typename DbValT>
      void
      RelatedProperty< ModelT, StorableT, UnusedUniqueT, RelatedStorableT,
          DbValT >::setNull(bool reset)
      {
        if (reset)
          set(
          RelatedStorablePtrT());
        else
          setNull();
      }
    /// -----------------------------------------------------------------------

    template<typename ModelT, class StorableT, typename UnusedUniqueT,
        class RelatedStorableT, typename DbValT>
      void
      RelatedProperty< ModelT, StorableT, UnusedUniqueT, RelatedStorableT,
          DbValT >::setDefault()
      {
        if (mAttrMetaDelegate.DefaultValue().is_initialized())
          set(boost::get< DbValT >(mAttrMetaDelegate.DefaultValue()));
        else
          setNull();
      }
    /// -----------------------------------------------------------------------

    template<typename ModelT, class StorableT, typename UnusedUniqueT,
        class RelatedStorableT, typename DbValT>
      boost::optional< boost::shared_ptr< RelatedStorableT > >
      RelatedProperty< ModelT, StorableT, UnusedUniqueT, RelatedStorableT,
          DbValT >::defaultValue() const
      {
        if (mAttrMetaDelegate.DefaultValue().is_initialized())
          return get(boost::get< DbValT >(mAttrMetaDelegate.DefaultValue()));

        return boost::optional< boost::shared_ptr< RelatedStorableT > >();
      }
    /// -----------------------------------------------------------------------

    /// ---------------
    /// AttrMetaI interface

    template<typename ModelT, class StorableT, typename UnusedUniqueT,
        class RelatedStorableT, typename DbValT>
      const std::string&
      RelatedProperty< ModelT, StorableT, UnusedUniqueT, RelatedStorableT,
          DbValT >::FieldName() const
      {
        return mAttrMetaDelegate.FieldName();
      }
    /// -----------------------------------------------------------------------

    template<typename ModelT, class StorableT, typename UnusedUniqueT,
        class RelatedStorableT, typename DbValT>
      bool
      RelatedProperty< ModelT, StorableT, UnusedUniqueT, RelatedStorableT,
          DbValT >::IsPrimaryKey() const
      {
        return mAttrMetaDelegate.IsPrimaryKey();
      }
    /// -----------------------------------------------------------------------

    template<typename ModelT, class StorableT, typename UnusedUniqueT,
        class RelatedStorableT, typename DbValT>
      bool
      RelatedProperty< ModelT, StorableT, UnusedUniqueT, RelatedStorableT,
          DbValT >::IsValidIfNull() const
      {
        return mAttrMetaDelegate.IsValidIfNull();
      }
    /// -----------------------------------------------------------------------

    template<typename ModelT, class StorableT, typename UnusedUniqueT,
        class RelatedStorableT, typename DbValT>
      const DbValT&
      RelatedProperty< ModelT, StorableT, UnusedUniqueT, RelatedStorableT,
          DbValT >::InitStepDbValue() const
      {
        return mBindGetDelegate.InitStepDbValue();
      }
    /// -----------------------------------------------------------------------

    template<typename ModelT, class StorableT, typename UnusedUniqueT,
        class RelatedStorableT, typename DbValT>
      bool
      RelatedProperty< ModelT, StorableT, UnusedUniqueT, RelatedStorableT,
          DbValT >::Bind(QSqlQuery& qry, const int& bindIndex)
      {
        assert(mIsNull || mObjectValue.get() != NULL);

        if (mIsNull && !IsPrimaryKey())
        {
          qry.bindValue(bindIndex, QVariant());
          return true;
        }

        return mBindGetDelegate.Bind(qry, bindIndex);
      }
    /// -----------------------------------------------------------------------

    template<typename ModelT, class StorableT, typename UnusedUniqueT,
        class RelatedStorableT, typename DbValT>
      bool
      RelatedProperty< ModelT, StorableT, UnusedUniqueT, RelatedStorableT,
          DbValT >::Get(QSqlQuery& qry, int fieldIndex)
      {
        QVariant value = qry.value(fieldIndex);

        mIsInitialised = true;

        if (value.isNull())
        {
          mIsNull = true;
          return true;
        }

        mIsNull = false;
        return mBindGetDelegate.Get(qry, fieldIndex, value);
      }
    /// -----------------------------------------------------------------------

    template<typename ModelT, class StorableT, typename UnusedUniqueT,
        class RelatedStorableT, typename DbValT>
      bool
      RelatedProperty< ModelT, StorableT, UnusedUniqueT, RelatedStorableT,
          DbValT >::initialiseModel(const int pass)
      {
        if (mStorableConfig->state().isExcludedFromDb())
          return true;

        if (pass != 1)
          return true;

        if (!mBindGetDelegate.initialiseModel())
          return false;

        if (mObjectValue.get() != NULL && sRelatedToManyVisitFunction != NULL)
        {
          RelatedToManyVisitorT visitor(mObjectValue,
              sRelatedToManyVisitFunction);
          mRelatedToMany = visitor.relatedToManyProperty;
          mRelatedToMany->addReference(mStorable->shared_from_this());
        }
        else
        {
          mRelatedToMany = NULL;
        }

        mIsNull = mObjectValue.get() == NULL;

        return true;
      }
    /// -----------------------------------------------------------------------

    template<typename ModelT, class StorableT, typename UnusedUniqueT,
        class RelatedStorableT, typename DbValT>
      void
      RelatedProperty< ModelT, StorableT, UnusedUniqueT, RelatedStorableT,
          DbValT >::setup()
      {
        assert( mStorable);

        // Make sure the model is initialised first.
        if (mStorable->model().lock() != NULL
            && mStorable->model().lock()->isInitialised())
        {
          // Then check if this property is initislised
          if (!mIsInitialised
              && mAttrMetaDelegate.DefaultValue().is_initialized())
          {
            // Set the default value
            set(boost::get< DbValT >(mAttrMetaDelegate.DefaultValue()));
          }
        }
      }
    /// -----------------------------------------------------------------------

    template<typename ModelT, class StorableT, typename UnusedUniqueT,
        class RelatedStorableT, typename DbValT>
      void
      RelatedProperty< ModelT, StorableT, UnusedUniqueT, RelatedStorableT,
          DbValT >::tearDown()
      {
      }
    /// -----------------------------------------------------------------------

    template<typename ModelT, class StorableT, typename UnusedUniqueT,
        class RelatedStorableT, typename DbValT>
      AttrI::PtrT
      RelatedProperty< ModelT, StorableT, UnusedUniqueT, RelatedStorableT,
          DbValT >::createCopy(DynamicStorable& storable, AttrI::WptrT attrMeta)
      {
        return CopierT(*this)(storable, attrMeta);
      }
    /// -----------------------------------------------------------------------

    template<typename ModelT, class StorableT, typename UnusedUniqueT,
        class RelatedStorableT, typename DbValT>
      bool
      RelatedProperty< ModelT, StorableT, UnusedUniqueT, RelatedStorableT,
          DbValT >::isValid() const
      {
        // Ensure that if isNull is not set then the object contains a value
        assert(mIsNull || mObjectValue.get() != NULL
            || !mBindGetDelegate.IsNull());
        return (IsValidIfNull() || !mIsNull);
      }
    /// -----------------------------------------------------------------------

    template<typename ModelT, class StorableT, typename UnusedUniqueT,
        class RelatedStorableT, typename DbValT>
      std::string
      RelatedProperty< ModelT, StorableT, UnusedUniqueT, RelatedStorableT,
          DbValT >::invalidReason() const
      {
        // Ensure that if isNull is not set then the object contains a value
        assert(mIsNull || mObjectValue.get() != NULL
            || !mBindGetDelegate.IsNull());

        if (!IsValidIfNull() && mIsNull)
          return FieldName() + " is null";

        return "";
      }
    /// -----------------------------------------------------------------------

    template<typename ModelT, class StorableT, typename UnusedUniqueT,
        class RelatedStorableT, typename DbValT>
      StorableConfigI&
      RelatedProperty< ModelT, StorableT, UnusedUniqueT, RelatedStorableT,
          DbValT >::storableConfig()
      {
        assert(mStorableConfig != NULL);
        return *mStorableConfig;
      }
    /// -----------------------------------------------------------------------

    template<typename ModelT, class StorableT, typename UnusedUniqueT,
        class RelatedStorableT, typename DbValT>
      boost::shared_ptr< MvLinkI >
      RelatedProperty< ModelT, StorableT, UnusedUniqueT, RelatedStorableT,
          DbValT >::mvLink()
      {
        boost::shared_ptr < MvLinkI > mvLink(new orm::v1::MvLinkStorable<
            ModelT, RelatedStorableT >(*this));
        return mvLink;
      }
    /// -------------------------------------------------------------------------

    template<typename ModelT, class StorableT, typename UnusedUniqueT,
        class RelatedStorableT, typename DbValT>
      boost::shared_ptr< RelatedStorableT >
      RelatedProperty< ModelT, StorableT, UnusedUniqueT, RelatedStorableT,
          DbValT >::get(const DbValT& value,
          boost::shared_ptr< ModelT > supplementaryModel) const
      {
        boost::shared_ptr < ModelT > model = mStorable->model().lock();

        if (model.get() == NULL && supplementaryModel.get() != NULL)
          model = supplementaryModel;

        if (model.get() == NULL)
        {
          boost::format info("Failed to retrieve object from model.\n"
            "This object needs to be added to the model to use this feature.\n"
            "field=%s, key=%s in %s");
          info % FieldName();
          info % boost::lexical_cast< std::string >(value);
          if (mStorableConfig == NULL)
            info % "";
          else
            info % mStorableConfig->sqlOpts()->name();
          throw ModelNotSetException(info.str());
        }

        boost::shared_ptr < RelatedStorableT > objValue //
        = ((*model).*sObjectGetter)(value);

        if (objValue.get() == NULL)
        {
          boost::format info("Failed to retrieve object from model.\n"
            "field=%s, key=%s in %s");
          info % FieldName();
          info % boost::lexical_cast< std::string >(value);
          if (mStorableConfig == NULL)
            info % "";
          else
            info % mStorableConfig->sqlOpts()->name();
          breakpoint();
          throw ObjectDoesNotExistException(info.str());
        }

        return objValue;
      }
  /// -----------------------------------------------------------------------
  }
}
