/*
 * Conn.cpp
 *
 *  Created on: Feb 7, 2011
 *      Author: Jarrod Chesney
 */

#include "Conn.h"
#include <assert.h>
#include <boost/format.hpp>
#include <boost/algorithm/string/find.hpp>

namespace orm
{

  namespace v1
  {

    /// ---------------------------------------------------------------------------
    //                   Conn: Implementation
    /// ---------------------------------------------------------------------------

    Conn::Conn() :
        Type(InvalidType), //
        Port(0)
    {
    }
    /// ---------------------------------------------------------------------------

    std::string
    Conn::toString() const
    {
      switch (Type)
      {
        case InvalidType:
        {
          return "Invalid Type";
          break;
        }

        case SQLite:
        {
          boost::format fmt("Type=SQLite, Filename=%s");
          fmt % Database;
          return fmt.str();
          break;
        }

        case MsAccess:
        {
          boost::format fmt("Type=MsAccess, Filename=%s");
          fmt % Database;
          return fmt.str();
          break;
        }

        case PostGreSQL:
        {
          boost::format fmt("Type=PostGreSQL, Host=%s, Port=%d, Database=%s, Username=%s, Password=%s");
          fmt % Host;
          fmt % Port;
          fmt % Database;
          fmt % Username;
          fmt % Password;
          return fmt.str();
          break;
        }

        case Oracle:
        {
          boost::format fmt("Type=Oracle, Host=%s, Port=%d, Database=%s, Username=%s, Password=%s");
          fmt % Host;
          fmt % Port;
          fmt % Database;
          fmt % Username;
          fmt % Password;
          return fmt.str();
          break;
        }
      }assert(false);
      return "";
    }
    /// ---------------------------------------------------------------------------

    void
    Conn::setTypeFromDatabaseName()
    {
      if (boost::ifind_first(Database, ".sqlite"))
      {
        Type = SQLite;
      }
      else if (boost::ifind_first(Database, ".mdb"))
      {
        Type = MsAccess;
      }
      else
      {
        boost::format info("database %s does not end in .sqlite or .mdb");
        info % Database;
        throw std::runtime_error(info.str());
      }

    }
  /// ---------------------------------------------------------------------------
  }

}
