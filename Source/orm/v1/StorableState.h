/*
 * StorableState.h
 *
 *  Created on: Feb 10, 2011
 *      Author: Jarrod Chesney
 */

#ifndef _ORM_V1_STORABLE_STATE_H_
#define _ORM_V1_STORABLE_STATE_H_

#include <QtCore/qreadwritelock.h>

namespace orm
{

  namespace v1
  {

    /*! Storable State
     * Stores database/data state information about the storable object.
     */
    class StorableState
    {
      public:
        StorableState();

        virtual
        ~StorableState();

        bool
        isUpdated() const;

        void
        setStoredToDb();

        bool
        isDeletedFromDb() const;

        void
        setDeletedFromDb();

        bool
        isLoadedFromDb() const;

        void
        setLoadedFromDb();

        bool
        isExcludedFromDb() const;

        void
        setExcludeFromDb(const bool exclude);

        void
        lockForRead();

        void
        lockForWrite();

        void
        unlock();

      private:
        friend class StorableChange;

        virtual void
        setUpdated();

        bool mUpdated;
        bool mExcluded;

        enum StateInDbE
        {
          NotInDbState,
          LoadedFromDbState,
          DeletedFromDbState
        };

        StateInDbE mStateInDb;

        QReadWriteLock mReadWriteLock;
    };

  }

}


#ifndef USE_ORM_V1_DLL
#include "StorableState.ini"
#endif

#endif /* _ORM_V1_STORABLE_STATE_H_ */
