/*
 * Util.h
 *
 *  Copyright NG SCADA Systems Pty Ltd 2011
 *
 *  This software is proprietary, you are not free to copy
 *  or redistribute this code in any format.
 *
 *  All rights to this software are reserved by 
 *  NG SCADA Systems Pty Ltd
 */

#ifndef _ORM_V1_UTIL_H_
#define _ORM_V1_UTIL_H_

#include <vector>

#include <boost/shared_ptr.hpp>

#include "Storable.h"

/*! orm::v1::Util Brief Description
 * Long comment about the class
 *
 */
namespace orm
{

  namespace v1
  {

    namespace util
    {

      template<typename BaseStorableT, typename DerrivedStorableT>
        void
        downcast(std::vector< boost::shared_ptr< BaseStorableT > >& baseStorables,
            std::vector< boost::shared_ptr< DerrivedStorableT > >& derrivedStorables)
        {
          typedef boost::shared_ptr< BaseStorableT > BasePtrT;
          typedef std::vector< BasePtrT > BaseListT;
          typedef boost::shared_ptr< DerrivedStorableT > DerrivedPtrT;
          typedef std::vector< DerrivedPtrT > DerrivedListT;

          for (typename BaseListT::iterator itr = baseStorables.begin(); //
          itr != baseStorables.end(); ++itr)
            derrivedStorables.push_back(DerrivedStorableT::downcast(*itr));
        }

      template<typename BackRefObjT, typename ObjT>
        boost::shared_ptr< ObjT >
        getOneFromBackRef(boost::shared_ptr< BackRefObjT >& backRefObj, void
        (BackRefObjT::*backRefFunc)(std::vector< boost::shared_ptr< ObjT > >&) const)
        {
          if (backRefObj.get() == NULL)
          {
            assert(BackRefObjT().config());
            assert(BackRefObjT().config()->sqlOpts().get());

            assert(ObjT().config());
            assert(ObjT().config()->sqlOpts().get());

            boost::format info("Failed to get exactly one back reference %s\n"
                "from %s.\n"
              "backRefObj was null.");
            info % ObjT().config()->sqlOpts()->name();
            info % BackRefObjT().config()->sqlOpts()->name();
            throw GetOneBackrefException(info.str());

          }

          std::vector< boost::shared_ptr< ObjT > > list;
          ((*backRefObj).*backRefFunc)(list);

          if (list.size() != 1)
          {
            assert(BackRefObjT().config());
            assert(BackRefObjT().config()->sqlOpts().get());

            assert(ObjT().config());
            assert(ObjT().config()->sqlOpts().get());

            boost::format info("Failed to get exactly one back reference %s\n"
                "from %s.\n"
              "Expected 1 back reference, got %d.");
            info % ObjT().config()->sqlOpts()->name();
            info % BackRefObjT().config()->sqlOpts()->name();
            info % list.size();
            throw GetOneBackrefException(info.str());

          }

          return list.front();
        }

    }

  }

}

#endif /* _ORM_V1_UTIL_H_ */
