/*
 * Storable.h
 *
 *  Created on: Dec 17, 2010
 *      Author: Jarrod Chesney
 */

#ifndef _ORM_V1_STORABLE_I_H_
#define _ORM_V1_STORABLE_I_H_

#include <vector>

#include <boost/shared_ptr.hpp>

namespace orm
{
  namespace v1
  {
    class StorableConfigI;

    /*!  StorableI
     * Interface for the storable objects
     */
    class StorableI
    {
      public:
        typedef boost::shared_ptr< StorableI > PtrT;
        typedef std::vector< PtrT > ListT;

      public:
        /// --------------
        // Flags

        virtual StorableConfigI*
        config() = 0;

        virtual void
        setUpdated(StorableConfigI& config) = 0;

      public:
        /// ---------------
        /// Methods setting up for interation with other storables

        virtual void
        clearModel() = 0;

        virtual bool
        isValid() = 0;

        virtual bool
        isPrimaryKeyValid() = 0;

        virtual std::string
        invalidReason() = 0;

        virtual bool
        initialiseModel(const int pass) = 0;

      public:

        virtual std::string
        typeName() const = 0;

      protected:

        virtual void
        setup() = 0;

        virtual void
        tearDown() = 0;

    };

  }
}

#endif /* _ORM_V1_STORABLE_I_H_ */
