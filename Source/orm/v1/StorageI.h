/*
 * StorageI.h
 *
 *  Copyright NG SCADA Systems Pty Ltd 2011
 *
 *  This software is proprietary, you are not free to copy
 *  or redistribute this code in any format.
 *
 *  All rights to this software are reserved by 
 *  NG SCADA Systems Pty Ltd
 */

#ifndef _ORM_V1_STORAGE_H_
#define _ORM_V1_STORAGE_H_

#include <string>

#include <boost/weak_ptr.hpp>

namespace orm
{
  namespace v1
  {

    template<typename ModelT>
      class Model;

    class StorableChange;

    template<class ModelT>
      class StorageI
      {
        public:
          virtual void
          save(StorableChange& change) = 0;

          virtual bool
          forceSave() = 0;

          virtual bool
          load() = 0;

          virtual bool
          isSaveRequired() const = 0;

          virtual void
          saveComplete() = 0;

          // Convieniance method so Model can initialise the data
          virtual bool
          initialiseModel(const int pass) = 0;

          // Convieniance method so Model can get the tablename
          virtual const std::string
          name() const = 0;

          virtual void
          setDebug(const bool prmDebug) = 0;

          virtual void
          setModel(boost::weak_ptr< ModelT > model,
              boost::weak_ptr< Model < ModelT > > baseModel) = 0;
      };
  }
}

#endif /* _ORM_V1_STORAGE_H_ */
