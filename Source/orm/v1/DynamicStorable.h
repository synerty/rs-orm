/*
 * DynamicStorable.h
 *
 *  Created on: Feb 8, 2011
 *      Author: Jarrod Chesney
 */

#ifndef _ORM_V1_DYNAMIC_STORABLE_H_
#define _ORM_V1_DYNAMIC_STORABLE_H_

#if defined(BUILD_ORM_V1_DLL)
#define DECL_ORM_V1_DIR __declspec(dllexport)
#elif defined(USE_ORM_V1_DLL)
#define DECL_ORM_V1_DIR __declspec(dllimport)
#else
#define DECL_ORM_V1_DIR
#endif

#include <boost/weak_ptr.hpp>

#include "Model.h"
#include "Attribute.h"
#include "Storable.h"
#include "StorableWrapper.h"
#include "StorageWrapper.h"
#include "SqlOpts.h"

/// ---------------------------------------------------------------------------
/// Forward Declarations
namespace orm
{

  namespace v1
  {
    class DynamicStorable;
    class DynamicModel;
  }
}
/// ---------------------------------------------------------------------------

/// ---------------------------------------------------------------------------
/// StorageWrapper specialisations
namespace orm
{

  namespace v1
  {

    template<>
          StorageWrapper< orm::v1::DynamicModel, orm::v1::DynamicStorable, bool >::StorageWrapper();

    template<>
          StorageWrapper< orm::v1::DynamicModel, orm::v1::DynamicStorable, bool >::~StorageWrapper();

    /* --------------- */
    /* Initialise */

    template<>
      void
          StorageWrapper< orm::v1::DynamicModel, orm::v1::DynamicStorable, bool >::setup(Model<
              orm::v1::DynamicModel >& model,
              QSqlDatabase& db);

    /* --------------- */
    /* storable accessors */

    template<>
      const std::vector< boost::shared_ptr< orm::v1::DynamicStorable > >
          &
          StorageWrapper< orm::v1::DynamicModel, orm::v1::DynamicStorable, bool >::list();

    template<>
      const std::vector< boost::shared_ptr< orm::v1::DynamicStorable > >
          &
          StorageWrapper< orm::v1::DynamicModel, orm::v1::DynamicStorable, bool >::list() const;

    template<>
      const StorageWrapper< orm::v1::DynamicModel, orm::v1::DynamicStorable,
          bool >::MapT
          &
          StorageWrapper< orm::v1::DynamicModel, orm::v1::DynamicStorable, bool >::map() const;

    template<>
      void
          StorageWrapper< orm::v1::DynamicModel, orm::v1::DynamicStorable, bool >::add(boost::shared_ptr<
              orm::v1::DynamicStorable > obj,
              boost::weak_ptr< orm::v1::DynamicModel > model,
              boost::weak_ptr< Model< orm::v1::DynamicModel > > baseModel);

    template<>
      void
          StorageWrapper< orm::v1::DynamicModel, orm::v1::DynamicStorable, bool >::addDerrived(boost::shared_ptr<
              orm::v1::DynamicStorable > obj);

    template<>
      void
          StorageWrapper< orm::v1::DynamicModel, orm::v1::DynamicStorable, bool >::remove(boost::shared_ptr<
              orm::v1::DynamicStorable > obj);

    template<>
      bool
          StorageWrapper< orm::v1::DynamicModel, orm::v1::DynamicStorable, bool >::contains(//
          const boost::shared_ptr< orm::v1::DynamicStorable >& obj) const;

    template<>
      boost::shared_ptr< orm::v1::DynamicStorable >
          StorageWrapper< orm::v1::DynamicModel, orm::v1::DynamicStorable, bool >::get(const bool& id);

    template<>
      const boost::shared_ptr< orm::v1::DynamicStorable >
          StorageWrapper< orm::v1::DynamicModel, orm::v1::DynamicStorable, bool >::get(const bool& id) const;

    /* --------------- */
    /* storage accessors */

    template<>
      void
          StorageWrapper< orm::v1::DynamicModel, orm::v1::DynamicStorable, bool >::clear();

    template<>
      void
          StorageWrapper< orm::v1::DynamicModel, orm::v1::DynamicStorable, bool >::loadCompletedEvent();

    template<>
      std::string
          StorageWrapper< orm::v1::DynamicModel, orm::v1::DynamicStorable, bool >::storableTypeName();

  }
}
/// ---------------------------------------------------------------------------

/// ---------------------------------------------------------------------------
/// Everything else
namespace orm
{

  namespace v1
  {

    template<typename ModelT, class StorableT, typename UnusedUniqueT,
        typename TypeT>
      class SimpleProperty;

    typedef SimpleProperty< DynamicModel, DynamicStorable, DynamicStorable,
        std::string > DynamicStringPropertyT;

    typedef SimpleProperty< DynamicModel, DynamicStorable, DynamicStorable,
        int16_t > DynamicInt16PropertyT;

    typedef SimpleProperty< DynamicModel, DynamicStorable, DynamicStorable,
        int32_t > DynamicInt32PropertyT;

    typedef SimpleProperty< DynamicModel, DynamicStorable, DynamicStorable,
        int64_t > DynamicInt64PropertyT;

    typedef SimpleProperty< DynamicModel, DynamicStorable, DynamicStorable,
        float > DynamicReal32PropertyT;

    typedef SimpleProperty< DynamicModel, DynamicStorable, DynamicStorable,
        bool > DynamicBoolPropertyT;

    class DECL_ORM_V1_DIR DynamicStorableConfig : public StorableConfigI
    {
      public:
        typedef boost::shared_ptr< DynamicStorableConfig > PtrT;
        typedef std::vector< PtrT > ListT;

      public:
        DynamicStorableConfig(StorableConfigI* inheritsFrom);

        virtual
        ~DynamicStorableConfig();

        PtrT
        createCopy(StorableConfigI* inheritsFrom);

        void
        InitialiseCopy(boost::shared_ptr< DynamicStorable > storable,
            PtrT& copy);

      public:

        void
        setup(StorableI& storable);

        StorableState&
        state();

        StorableI&
        storable();

        void
        setUpdated();

        StorableConfigI*
        inheritsFrom();

        bool
        isValid();

        bool
        isPrimaryKeyValid();

        std::string
        invalidReason();

        void
        clearModel();

        void
        setup();

        void
        tearDown();

      public:
        /// ---------------
        /// AttrA accessors

        const AttrsT&
        fields();

        void
        addField(AttrI* field);

      public:
        /// ---------------
        /// BackRefI accessors

        void
        addBackRef(BackRefI* backRef);

        const BackRefsT&
        backRefs() const;

      public:
        /// ---------------
        /// AttrMeta accessors

        const AttrPtrsT&
        fieldMetas() const;

        void
        addFieldMeta(AttrI::PtrT field);

      public:
        /// ---------------
        /// SqlOpt accessors

        SqlOpts::PtrT
        sqlOpts();

        const SqlOpts::PtrT&
        sqlOpts() const;

      private:
        StorableI* mStorable;

        StorableConfigI* mInheritsFrom;

        StorableState mState;

        // This class will be shared when the config is copied
        boost::shared_ptr< SqlOpts > mSqlOpts;

        // Standard list of attributes to return
        AttrsT mFieldForReturn;

        // Standard list of attributes to return
        BackRefsT mBackRefForReturn;

        // Unlike a standard storable that holdes the AttrA instances (properties)
        // on the stack, the DynamicStorable does not. So we need to keep track of
        // these and deleted them when we destruct.
        AttrPtrsT mFields;

        // This list will be shared when the config is copied
        boost::shared_ptr< AttrPtrsT > mTemplateFields;

      private:
    };
    /// -----------------------------------------------------------------------

    class DECL_ORM_V1_DIR DynamicStorable : public StorableA< DynamicModel >
    {
      public:
        typedef boost::shared_ptr< DynamicStorable > PtrT;
        typedef std::vector< PtrT > ListT;

      public:
        DynamicStorable();
        virtual
        ~DynamicStorable();

      public:
        /// --------------
        /// Actual public interface

        virtual
        void
        addToModel(boost::shared_ptr< DynamicModel > model);

        virtual
        void
        removeFromModel();

        virtual
        bool
        isInModel();

      public:

        StorableConfigI*
        config();

        std::string
        typeName() const;

        boost::shared_ptr< DynamicStorable >
        shared_from_this();

        static boost::shared_ptr< DynamicStorable >
        downcast(boost::shared_ptr< StorableI > baseClass,
            bool warnForNull = true);

      protected:

        void
        setup();

      private:
        DynamicStorableConfig::ListT mConfigs;
    };
    /// -----------------------------------------------------------------------

    class DECL_ORM_V1_DIR DynamicModel : public Model< DynamicModel >
    {
      public:
        DynamicModel(const Conn& ormConn);

        DynamicStorableConfig::PtrT
        addInherited();

      public:
        typedef boost::shared_ptr< DynamicStorable > DynamicStorablePtrT;
        typedef std::vector< DynamicStorablePtrT > DynamicStorableListT;

        /*! Add Storable
         * The model has to be initialised in a dymamic storable before its of use.
         * Thats why you need to ask the model for a new storable instead of default
         * constructing one.
         * @return new storable
         */
        DynamicStorablePtrT
        add();

        void
        remove(DynamicStorablePtrT obj);

        bool
        contains(const DynamicStorablePtrT obj) const;

        const DynamicStorableListT&
        storables();

        const DynamicStorableListT&
        storables() const;

      private:
        friend class DynamicStorable;

        void
        setupDynamicStorable(boost::shared_ptr< DynamicStorable > storable,
            DynamicStorableConfig::ListT& configs);

        typedef StorageWrapper< DynamicModel, DynamicStorable, bool >
            StorableWrapperT;
        StorableWrapperT mStorageWrapper;

        DynamicStorableConfig::ListT mConfigs;
    };
  /// -----------------------------------------------------------------------

  }

}

#endif /* _ORM_V1_DYNAMIC_STORABLE_H_ */
