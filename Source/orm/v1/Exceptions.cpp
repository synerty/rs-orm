/*
 * Exceptions.cpp
 *
 *  Copyright NG SCADA Systems Pty Ltd 2011
 *
 *  This software is proprietary, you are not free to copy
 *  or redistribute this code in any format.
 *
 *  All rights to this software are reserved by 
 *  NG SCADA Systems Pty Ltd
 */

#include "Exceptions.h"

#include "StorableConfigI.h"
#include "SqlOpts.h"
#include "Breakpoint.h"

namespace orm
{
  namespace v1
  {

    // ---------------
    // Storable exceptions

    AccessingNullValueException::AccessingNullValueException(const StorableConfigI* storableConfig,
        const std::string& fieldName) :
      std::runtime_error( //
      "Accessing Null Value for " //
          + (storableConfig == NULL //
          ? "Null Storable Config - " //
              : storableConfig->sqlOpts().get() == NULL //
              ? "Null Storable Config SqlOps - " //
                  : storableConfig->sqlOpts()->name() + " - ") //
          + fieldName //
          )
    {
      breakpoint();
    }

  }
}
