/*
 * StorableChange.h
 *
 *  Copyright NG SCADA Systems Pty Ltd 2011
 *
 *  This software is proprietary, you are not free to copy
 *  or redistribute this code in any format.
 *
 *  All rights to this software are reserved by 
 *  NG SCADA Systems Pty Ltd
 */

#ifndef _ORM_V1_STORABLE_CHANGE_H_
#define _ORM_V1_STORABLE_CHANGE_H_

#if defined(BUILD_ORM_V1_DLL)
#define DECL_ORM_V1_DIR __declspec(dllexport)
#elif defined(USE_ORM_V1_DLL)
#define DECL_ORM_V1_DIR __declspec(dllimport)
#else
#define DECL_ORM_V1_DIR
#endif

#include <deque>
#include <cassert>

#include "StorableI.h"
#include "Exceptions.h"

/*! Stotrable Change
 * This class represnets a change request from the storable to the model
 *
 */
namespace orm
{

  namespace v1
  {

    class DECL_ORM_V1_DIR StorableChange
    {
      public:
        typedef std::deque< StorableConfigI* > ConfigsT;

        enum TypeE
        {
          addChange,
          updateChange,
          removeChange,
          noChange
        };

      public:

        StorableChange(StorableI::PtrT storable) :
          mType(noChange), mStorable(storable)
        {
        }

        TypeE
        type() const
        {
          assert(mType != noChange);
          return mType;
        }

        /*! Configs
         * List of configs to update
         * @return list of configs
         */
        const ConfigsT&
        updatedConfigs() const
        {
          assert(mType == updateChange);
          return mConfigs;
        }

        /*! Storable
         * The storable the change applies to
         * @return
         */
        StorableI::PtrT
        storable() const
        {
          assert(mType != noChange);
          return mStorable;
        }

        /*! Add
         * Tells the change that this is an add
         */
        StorableChange&
        add()
        {
          assert(mType == noChange);
          mType = addChange;

          if (!mStorable->isValid())
            throw StorableDataInvalidException(mStorable->invalidReason());

          return *this;
        }

        /*! Update
         * Tells the change that this is a delete
         */
        StorableChange&
        remove()
        {
          assert(mType == noChange);
          mType = removeChange;

          if (!mStorable->isValid())
            throw StorableDataInvalidException(mStorable->invalidReason());

          return *this;
        }

        /*! Update
         * Adds thid config to configs to update
         * @param config
         */
        StorableChange&
        update(StorableConfigI& config)
        {
          assert(mType == noChange || mType == updateChange);
          assert(&config.storable() == mStorable.get());
          mConfigs.push_back(&config);
          mType = updateChange;
          config.state().setUpdated();
          return *this;
        }

        bool
        isMergable(StorableChange& other)
        {
          return mType == other.mType //
              && mStorable == other.mStorable;
        }

        void
        merge(StorableChange& other)
        {
          assert(isMergable(other));
          std::copy(other.mConfigs.begin(),
              other.mConfigs.end(),
              std::back_inserter(mConfigs));
        }

      private:
        /*! Change Type
         * The type of change to be applied
         */
        TypeE mType;

        /*! Storable these changes apply to
         * This variable keeps the storable alive while we point to the config which
         * is on the stack (or not in the case of DynamicStorble
         */
        StorableI::PtrT mStorable;

        /*! Config
         * The configs the changes apply to
         */
        ConfigsT mConfigs;
    };

  }

}

#endif /* _ORM_V1_STORABLE_CHANGE_H_ */
