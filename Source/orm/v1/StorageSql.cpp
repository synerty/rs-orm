#include "StorageSql.h"

#include <boost/algorithm/string/find.hpp>

namespace orm
{
  namespace v1
  {

    //----------------------------------------------------------------------------
    //                            StorageSql<StorableT>
    //----------------------------------------------------------------------------

    StorageSql::StorageSql(QSqlDatabase& db) :
      // Members
          mDb(db), //
          mConfig(NULL)
    {
    }

    StorageSql::~StorageSql()
    {
    }

    void
    StorageSql::setConfig(StorableConfigI* config)
    {
      mConfig = config;
    }

    std::string
    StorageSql::sqlSelect() const
    {
      return mConfig->sqlOpts()->withClause() //
          + sqlSelectFields() //
          + sqlSelectFrom() //
          + sqlWhereCondition() //
          + sqlOrderBy();
    }

    std::string
    StorageSql::sqlInsert(StorableConfigI& config) const
    {
      const AttrPtrsT& attrMetas = config.fieldMetas();
      assert(!attrMetas.empty());

      std::string lFieldNames;
      std::string lBindIndexs;
      bool lFirstAdded = false;
      for (AttrPtrsT::const_iterator itr = attrMetas.begin(); //
      itr != attrMetas.end(); ++itr)
      {
        const AttrI::PtrT& attrMeta = *itr;
        if (lFirstAdded)
        {
          lFieldNames += "\t, ";
          lBindIndexs += "\t, ";
        }
        lFieldNames += fQ(attrMeta->FieldName());
        lBindIndexs += "?";
        lFirstAdded = true;
      }

      std::string sql = "INSERT INTO " + fQ(config.sqlOpts()->tableName())
          + "\n" //
          + " (" + lFieldNames + ") \n" //
          + " VALUES (" + lBindIndexs + ")\n";

      return sql;
    }

    std::string
    StorageSql::sqlUpdate(StorableConfigI& config, int& pkStartBindIndex) const
    {
      bool lFirstAdded = false;
      std::string lAssigns;

      // get fields
      const AttrPtrsT& attrMetas = config.fieldMetas();
      assert(!attrMetas.empty());

      for (AttrPtrsT::const_iterator itr = attrMetas.begin(); //
      itr != attrMetas.end(); ++itr)
      {
        const AttrI::PtrT& attrMeta = *itr;

        if (lFirstAdded)
          lAssigns += "\t, ";

        lAssigns += fQ(attrMeta->FieldName()) + " = ?";
        lFirstAdded = true;

        pkStartBindIndex++;
      }

      std::string sql = "UPDATE " //
          + fQ(config.sqlOpts()->tableName()) + "\n" //
          + " SET " + lAssigns + " \n";

      sql += sqlWherePrimaryKey(config);

      return sql;
    }

    std::string
    StorageSql::sqlDelete(StorableConfigI& config) const
    {
      // We delete from the base table, the others should cascade
      std::string sql;
      sql += "DELETE\n";
      sql += "FROM " + fQ(config.sqlOpts()->tableName()) + "\n";
      sql += sqlWherePrimaryKey(config);
      return sql;
    }

    std::string
    StorageSql::sqlSelectFields() const
    {
      bool lFirstAdded = false;
      std::string sql = "SELECT ";
      std::string tblAlias = mConfig->sqlOpts()->sqlAlias();

      for (StorableConfigI* cfg = mConfig; //
      cfg != NULL; cfg = cfg->inheritsFrom())
      {
        // Get attributes / field names.
        const AttrPtrsT& attrMetas = cfg->fieldMetas();
        SqlOpts::PtrT sqlOpt = cfg->sqlOpts();
        assert(!attrMetas.empty());
        assert(sqlOpt.get());

        // Update table alias if not concrete inheritance
        if (mConfig->sqlOpts()->inheritanceType()
            != SqlOpts::ConcreteInheritance)
          tblAlias = sqlOpt->sqlAlias();

        // Add distinct
        if (!lFirstAdded && cfg->sqlOpts()->isSelectDistinct())
          sql += "DISTINCT ";

        // Add the field names
        for (AttrPtrsT::const_iterator itr = attrMetas.begin(); //
        itr != attrMetas.end(); ++itr)
        {
          const AttrI::PtrT& attrMeta = *itr;

          if (lFirstAdded)
            sql += "\t, ";

          sql += tblAlias + "." + fQ(attrMeta->FieldName()) + "\n";
          lFirstAdded = true;
        }
      }

      return sql;
    }

    std::string
    StorageSql::sqlSelectFrom() const
    {
      std::string sql;

      if (mConfig->sqlOpts()->inheritanceType() == SqlOpts::ConcreteInheritance)
      {
        sql = "FROM " + tableOrQuerySql(*mConfig->sqlOpts()) //
            + as() + mConfig->sqlOpts()->sqlAlias() + "\n";
        return sql;
      }

      // Create a vector of the configs so we can iterate through them in
      // reverse
      std::vector< StorableConfigI* > configs;
      for (StorableConfigI* cfg = mConfig; //
      cfg != NULL; cfg = cfg->inheritsFrom())
        configs.push_back(cfg);

      for (std::vector< StorableConfigI* >::reverse_iterator
          cfgItr = configs.rbegin(); //
      cfgItr != configs.rend(); ++cfgItr)
      {
        StorableConfigI* config = *cfgItr;
        SqlOpts::PtrT sqlOpt = config->sqlOpts();

        if (sql.empty())
        {
          // In postgres, we only ever want to qry the table we're using.
          // If this is not the case then use qry sql, not a table name
          sql = tableOrQuerySql(*sqlOpt) //
              + as() + sqlOpt->sqlAlias() + "\n";

          if (!sqlOpt->joins().empty())
            throw InvalidJoinException();

          continue;
        }

        SqlJoin::ListT joins = sqlOpt->joins();

        if (sqlOpt->inheritanceType() == SqlOpts::JoinedInheritance)
        {
          // Get the base table sql opts
          StorableConfigI* inheritedConfig = (*cfgItr)->inheritsFrom();
          assert(inheritedConfig != NULL);
          SqlOpts::PtrT inheritedSqlOpt = inheritedConfig->sqlOpts();

          // Get the pk names - assumed the same for both
          std::vector < std::string > pkNames;
          sqlPrimaryKeyFieldNames(*config, pkNames);

          // Add a join for each pk field
          for (std::vector< std::string >::const_iterator
              pkNameItr = pkNames.begin(); //
          pkNameItr != pkNames.end(); ++pkNameItr)
          {
            joins.push_back(SqlJoin(*pkNameItr, inheritedSqlOpt, *pkNameItr));
          }
        }

        if (joins.empty())
        {
          sql += "\t, " + fQ(sqlOpt->tableName()) //
              + as() + sqlOpt->sqlAlias() + "\n";
        }
        else
        {

          std::string joinType;
          assert(!joins.empty());
          switch (joins.front().joinType)
          {
            case SqlJoin::InnerJoin:
              joinType = "INNER JOIN ";
              break;

            case SqlJoin::LeftJoin:
              joinType = "LEFT OUTER JOIN ";
              break;

            case SqlJoin::RightJoin:
              joinType = "RIGHT OUTER JOIN ";
              break;

            case SqlJoin::OuterJoin:
              joinType = "FULL OUTER JOIN ";
              break;

            default:
              assert(false);
              break;
          }

          if (mDb.driverName() == "QODBC")
          {
            std::string databaseName = mDb.databaseName().toStdString();

            using namespace boost;
            const bool isMdb = ifind_first(databaseName, std::string(".mdb"));

            if (isMdb)
              sql = " (" + sql + ") ";
          }

          sql += "\t" + joinType + tableOrQuerySql(*sqlOpt) //
              + as() + sqlOpt->sqlAlias() + "\n";

          std::string joinSql;

          for (SqlJoin::ListT::const_iterator joinItr = joins.begin(); //
          joinItr != joins.end(); joinItr++)
          {
            const SqlJoin& join = *joinItr;
            SqlOpts::PtrT toSqlOpt = join.toSqlOpt.lock();
            assert(toSqlOpt.get());

            if (joinSql.empty())
              joinSql += "\t\tON ";
            else
              joinSql += "\t\t\tAND ";

            joinSql += toSqlOpt->sqlAlias() + "." + fQ(join.toField) //
                + " = " + sqlOpt->sqlAlias() + "." + fQ(join.fromField) + "\n";
          }

          sql += joinSql;
        }
      }

      return "FROM " + sql;
    }

    std::string
    StorageSql::sqlWhereCondition() const
    {
      SqlOpts::ConditionListT conds;

      for (StorableConfigI* cfg = mConfig; //
      cfg != NULL; cfg = cfg->inheritsFrom())
        conds.insert(conds.end(),
            cfg->sqlOpts()->conditions().begin(),
            cfg->sqlOpts()->conditions().end());

      if (conds.empty())
      {
        return "";
      }

      std::string sql = "WHERE ";
      for (SqlOpts::ConditionListT::const_iterator itr = conds.begin(); itr
          != conds.end(); ++itr)
      {
        if (itr != conds.begin())
        {
          sql += "\tAND ";
        }

        sql += (*itr) + "\n";
      }
      return sql;
    }

    std::string
    StorageSql::sqlWherePrimaryKey(StorableConfigI& config) const
    {
      std::vector < std::string > pkNames;
      sqlPrimaryKeyFieldNames(config, pkNames);

      std::string sql;

      for (std::vector< std::string >::const_iterator itr = pkNames.begin(); //
      itr != pkNames.end(); ++itr)
      {
        if (!sql.empty())
          sql += "\tAND ";

        sql += fQ(*itr) + " = ?";
      }

      // If we don't have any keys we might run an "unrestrained update" on the
      // database AND THAT IS BAD.
      assert(!sql.empty());

      return "WHERE " + sql;
    }

    std::string
    StorageSql::sqlOrderBy() const
    {
      //    if (m_OrderBys.empty())
      //    {
      //      return "";
      //    }
      //
      //    std::string sql = "ORDER BY ";
      //    for (typename std::vector<TcField*>::const_iterator
      //        itr = m_OrderBys.begin(); itr != m_OrderBys.end(); ++itr)
      //    {
      //      if (itr != m_OrderBys.begin())
      //      {
      //        sql += "\n, ";
      //      }
      //
      //      sql += fQ(itr->FieldName());
      //    }
      //    sql += "\n";
      //    return sql;
      return "";
    }

    std::string
    StorageSql::fQ(const std::string prmFieldName) const
    {
      if (mDb.driverName() == "QODBC")
      {
        std::string databaseName = mDb.databaseName().toStdString();

        using namespace boost;
        const bool isMdb = ifind_first(databaseName, std::string(".mdb"));

        if (isMdb)
          return "[" + prmFieldName + "]";
      }

      return mDb.driver()->escapeIdentifier(prmFieldName.c_str(),
          QSqlDriver::FieldName).toStdString();
    }

    std::string
    StorageSql::as() const
    {
      return (mDb.driverName() == "QOCI" ? " " : " AS ");
    }

    std::string
    StorageSql::only() const
    {
      return (mDb.driverName() == "QPSQL" ? " ONLY " : " ");
    }

    std::string
    StorageSql::tableOrQuerySql(const SqlOpts& sqlOpt) const
    {
      if (!sqlOpt.tableName().empty())
        return only() + fQ(sqlOpt.tableName());

      if (!sqlOpt.querySql().empty())
        return " (\n\t" + sqlOpt.querySql() + "\n)\n";

      throw EmptyTableNameAndQuerySqlException();
    }

    void
    StorageSql::sqlPrimaryKeyFieldNames(StorableConfigI& config,
        std::vector< std::string >& names) const
    {
      const AttrPtrsT& attrMetas = mConfig->fieldMetas();

      for (AttrPtrsT::const_iterator itr = attrMetas.begin(); //
      itr != attrMetas.end(); ++itr)
      {
        const AttrI::PtrT& attrMeta = *itr;

        if (!attrMeta->IsPrimaryKey())
          continue;

        names.push_back(attrMeta->FieldName());
      }
    }
  }
}
