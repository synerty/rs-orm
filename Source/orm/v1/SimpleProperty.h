/*
 * Attribute.h
 *
 *  Created on: Dec 17, 2010
 *      Author: Jarrod Chesney
 */

#ifndef _ORM_V1_SIMPLE_PROPERTY_H_
#define _ORM_V1_SIMPLE_PROPERTY_H_

#include <string>
#include <assert.h>
#include <stdint.h>

#include <QtCore/qvariant.h>

#include "PropertyBase.h"
#include "PropertyPrivate.h"
#include "Exceptions.h"

namespace orm
{
  namespace v1
  {
    template<typename ModelT, class StorableT, typename UnusedUniqueT,
        typename TypeT>
      class SimpleProperty;

    template<typename ModelT, class StorableT, typename UnusedUniqueT,
        typename TypeT>
      class SimpleProperty : public PropertyBase< ModelT, StorableT,
          UnusedUniqueT, TypeT, SimpleProperty< ModelT, StorableT,
              UnusedUniqueT, TypeT > >
      {
        public:
          SimpleProperty();

          virtual
          ~SimpleProperty();

        public:

          /// ---------------
          /// AttributeI interface

          virtual bool
          Bind(QSqlQuery& qry, const int& bindIndex);

          virtual bool
          Get(QSqlQuery& qry, int fieldIndex);

        protected:
          typedef PropertyBase< ModelT, StorableT, UnusedUniqueT, TypeT,
              SimpleProperty< ModelT, StorableT, UnusedUniqueT, TypeT > >
              BasePropT;

          typedef __property_private ::BindGetDelegate< TypeT >
              BindGetDelegateT;
      };
  }
}

  //! Adds the orm property to the class
#define ORM_V1_DECLARE_SIMPLE_PROPERTY(_get_name_, _name_, _type_) \
      /* BEGIN _name_ inherited PROPERTY */ \
      private: \
         class __ ## _name_ ## UniqueProp; \
        \
      public: \
         typedef orm::v1::SimpleProperty< \
             ModelT \
             , ThisStorableT \
             , __ ## _name_ ## UniqueProp \
             , _type_ \
             > _name_ ## PropT; \
        \
        virtual _type_ _get_name_() const; \
        \
        virtual void set ## _name_(const _type_& value); \
        \
        virtual bool is ## _name_ ## Null() const; \
        \
        virtual void set ## _name_ ## Null(); \
      private: \
        _name_ ## PropT mProp ## _name_; \
      /* END _name_ inherited PROPERTY */

#define ORM_V1_DECLARE_SIMPLE_PROPERTY_INHERITED(_get_name_, _name_, _type_) \
      /* BEGIN _name_ PROPERTY */ \
      ORM_V1_DECLARE_SIMPLE_PROPERTY(_get_name_, _name_, _type_) \
      /* END _name_ PROPERTY */


//! Adds the orm property to the class
#define ORM_V1_DEFINE_SIMPLE_PROPERTY(_storable_t_, _get_name_, _name_, _type_) \
      /* BEGIN _name_ PROPERTY */ \
        \
        void \
        _storable_t_::set ## _name_(const _type_& value) \
        { \
          mProp ## _name_.set(value); \
        } \
        \
        _type_ \
        _storable_t_::_get_name_() const \
        { \
          return mProp ## _name_.get(); \
        } \
        \
        void \
        _storable_t_::set ## _name_ ## Null() \
        { \
          mProp ## _name_.setNull(); \
        } \
        \
        bool \
        _storable_t_::is ## _name_ ## Null() const \
        { \
          return mProp ## _name_.isNull(); \
        } \
      /* END _name_ PROPERTY */

  //! Adds the orm property to the class
#define ORM_V1_DEFINE_SIMPLE_PROPERTY_INHERITED(_storable_t_,  _get_name_, _name_, _type_) \
      /* BEGIN _name_ inherited PROPERTY */ \
        \
        _type_ \
        _storable_t_::_get_name_() const \
        { \
          return mProp ## _name_.get(); \
        } \
        \
        void \
        _storable_t_::set ## _name_(const _type_& value) \
        { \
          BaseStorableT::set ## _name_(value); \
          mProp ## _name_.set(value); \
        } \
        \
        bool \
        _storable_t_::is ## _name_ ## Null() const \
        { \
          return mProp ## _name_.isNull(); \
        } \
        \
        void \
        _storable_t_::set ## _name_ ## Null()\
        { \
          BaseStorableT::set ## _name_ ## Null(); \
          mProp ## _name_.setNull(); \
        } \
      /* END _name_ inherited PROPERTY */


#ifndef USE_ORM_V1_DLL
#include "SimpleProperty.ini"
#endif

#endif /* _ORM_V1_SIMPLE_PROPERTY_H_ */
