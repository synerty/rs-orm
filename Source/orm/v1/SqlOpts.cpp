/*
 * SqlOpts.cpp
 *
 *  Created on: Feb 3, 2011
 *      Author: Jarrod Chesney
 */

#include "SqlOpts.h"

#include <assert.h>

#include <boost/format.hpp>

namespace orm
{
  namespace v1
  {

    /// -----------------------------------------------------------------------
    ///                         SqlJoin implemetation
    /// -----------------------------------------------------------------------

    SqlJoin::SqlJoin(const std::string fromField_,
        boost::weak_ptr< SqlOpts > toSqlOpt_,
        const std::string toField_,
        const JoinTypeE joinType_) :
      fromField(fromField_), //
          toSqlOpt(toSqlOpt_), //
          toField(toField_), //
          joinType(joinType_)
    {
    }

    /// -----------------------------------------------------------------------
    ///                         SqlOpts implemetation
    /// -----------------------------------------------------------------------

    SqlOpts::SqlOpts() :
      mSelectDistinct(false), //
          mInheritanceType(NoInheritance)
    {
      boost::format fmt("z%xz");
      fmt % reinterpret_cast< uintptr_t > (this);
      mSqlAlias = fmt.str();
    }
    /// -----------------------------------------------------------------------

    SqlOpts::~SqlOpts()
    {
    }
    /// -----------------------------------------------------------------------

    std::string
    SqlOpts::name() const
    {
      return mName;
    }
    /// -----------------------------------------------------------------------

    void
    SqlOpts::setName(const std::string& name)
    {
      mName = name;
    }
    /// -----------------------------------------------------------------------

    std::string
    SqlOpts::tableName() const
    {
      return mTableName;
    }
    /// -----------------------------------------------------------------------

    void
    SqlOpts::setTableName(const std::string& tableName)
    {
      mTableName = tableName;
      if (mName.empty())
        mName = tableName;
    }
    /// -----------------------------------------------------------------------

    std::string
    SqlOpts::querySql() const
    {
      return mQuerySql;
    }
    /// -----------------------------------------------------------------------

    void
    SqlOpts::setQuerySql(const std::string& querySql)
    {
      mQuerySql = querySql;
    }
    /// -----------------------------------------------------------------------

    std::string
    SqlOpts::sqlAlias() const
    {
      return mSqlAlias;
    }
    /// -----------------------------------------------------------------------

    void
    SqlOpts::setSelectDistinct(const bool isDistinct)
    {
      mSelectDistinct = isDistinct;
    }
    /// -----------------------------------------------------------------------

    bool
    SqlOpts::isSelectDistinct() const
    {
      return mSelectDistinct;
    }
    /// -----------------------------------------------------------------------


    void
    SqlOpts::addCondition(std::string condition)
    {
      mConditions.push_back(condition);
    }
    /// -----------------------------------------------------------------------


    void
    SqlOpts::setWithClause(std::string with)
    {
      mSqlWith = with;
    }
    /// -----------------------------------------------------------------------


    std::string
    SqlOpts::withClause() const
    {
      return mSqlWith;
    }
    /// -----------------------------------------------------------------------

    void
    SqlOpts::clearConditions()
    {
      mConditions.clear();
    }
    /// -----------------------------------------------------------------------

    const SqlOpts::ConditionListT&
    SqlOpts::conditions() const
    {
      return mConditions;
    }
    /// -----------------------------------------------------------------------

    void
    SqlOpts::addJoin(const SqlJoin join)
    {
      mJoins.push_back(join);
    }
    /// -----------------------------------------------------------------------

    const SqlJoin::ListT&
    SqlOpts::joins() const
    {
      return mJoins;
    }
    /// -----------------------------------------------------------------------

    void
    SqlOpts::setInheritanceType(const InheritanceTypeE type)
    {
      mInheritanceType = type;
    }
    /// -----------------------------------------------------------------------

    SqlOpts::InheritanceTypeE
    SqlOpts::inheritanceType() const
    {
      return mInheritanceType;
    }
  /// -----------------------------------------------------------------------
  }
}
