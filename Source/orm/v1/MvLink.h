/*
 * MvLink.h
 *
 *  Created on: Feb 7, 2011
 *      Author: Jarrod Chesney
 */

#ifndef _ORM_V1_MV_LINK_H_
#define _ORM_V1_MV_LINK_H_

#include "Attribute.h"

#include <string.h>
#include <stdint.h>

#include <boost/variant.hpp>
#include <boost/shared_ptr.hpp>

class QObject;
class QWidget;
#include <QtCore/qvariant.h>
#include <QtGui/qlabel.h>

/*! orm::v1::MvLink Brief Description
 * Long comment about the class
 *
 */
namespace orm
{

  namespace v1
  {
    class StorableI;

    class MvLinkI
    {
      public:
        struct Invalid
        {
            bool
            operator<(const Invalid& o)
            {
              return this < &o;
            }
        };

        typedef Invalid InvalidT;

        typedef boost::shared_ptr< MvLinkI > PtrT;
        typedef boost::variant< InvalidT, bool, float, double, int16_t,
            int32_t, int64_t, std::string, boost::shared_ptr< StorableI > >
            StandardValueT;

      public:
        /// ---------------
        /// Provide a way of getting standard values

        virtual bool
        value(StandardValueT& value) = 0;

        virtual bool
        setValue(const StandardValueT& value) = 0;

        virtual bool
        isNull() const = 0;

        virtual void
        setNull() = 0;

        virtual void
        setDefault() = 0;

      public:
        /// ---------------
        /// Interact with the view

        virtual QWidget*
        widget(QWidget* parent) = 0;
    };

    template<typename ValueTypeT>
      class MvLink : public MvLinkI
      {
        public:
          /// ---------------
          /// Constructor, Destructor
          MvLink(PropertyI< ValueTypeT >& property);

          virtual
          ~MvLink();

        public:
          /// ---------------
          /// Interact with the model


        public:
          /// ---------------
          /// Provide a way of getting standard values

          bool
          value(StandardValueT& value);

          bool
          setValue(const StandardValueT& value);

          bool
          isNull() const;

          void
          setNull();

          void
          setDefault();

        public:
          /// ---------------
          /// Interact with the view

          QWidget*
          widget(QWidget* parent);

        private:
          PropertyI< ValueTypeT >& mProperty;
      };

    template<typename ModelT, typename StorableT>
      class MvLinkStorable : public MvLinkI
      {
        public:
          /// ---------------
          /// Constructor, Destructor
          MvLinkStorable(PropertyI< boost::shared_ptr< StorableT > >& property);

          virtual
          ~MvLinkStorable();

        public:
          /// ---------------
          /// Interact with the model


        public:
          /// ---------------
          /// Provide a way of getting standard values

          bool
          value(StandardValueT& value);

          bool
          setValue(const StandardValueT& value);

          bool
          isNull() const;

          void
          setNull();

          void
          setDefault();

        public:
          /// ---------------
          /// Interact with the view

          QWidget*
          widget(QWidget* parent);

        private:
          PropertyI< boost::shared_ptr< StorableT > >& mProperty;
      };

  }

}

#ifndef USE_ORM_V1_DLL
#include "MvLink.ini"
#endif

#endif /* _ORM_V1_MV_LINK_H_ */
