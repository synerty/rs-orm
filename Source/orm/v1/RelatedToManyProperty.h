/*
 * IsAttribute.h
 *
 *  Created on: Dec 20, 2010
 *      Author: Jarrod Chesney
 */

#ifndef _ORM_V1_RELATED_TO_MANY_PROPERTY_H_
#define _ORM_V1_RELATED_TO_MANY_PROPERTY_H_

#include <vector>

#include <boost/weak_ptr.hpp>
#include <boost/shared_ptr.hpp>
#include "RelatedPropertyPrivate.h"
#include "StorableConfigI.h"
#include "Attribute.h"

namespace orm
{
  namespace v1
  {

    template<typename ModelT>
      class Model;

    class StorableI;

    namespace __related_property_private
    {
      template<typename ModelT, class StorableT, typename RelatingStorableT>
        struct RelatedToManyVisitor;

    }

    /*!  Related From Many Propery
     * This class handels the relationships between its owning storable has
     * with mutlitple objects. This is usually done with an indepent many
     * to many linking table.
     *
     * @tparam ModelT The model type this properties storable belongs to.
     * @tparam StorableT The storable type this propert belongs to.
     * @tparam RelatingStorableT type of the object relating this object to another
     *
     */
    template<typename ModelT, typename StorableT, typename RelatingStorableT>
      class RelatedToManyProperty : public BackRefI
      {
          typedef boost::weak_ptr< RelatingStorableT > RelatingStorableWptrT;
          typedef boost::shared_ptr< RelatingStorableT > RelatingStorablePtrT;
          typedef std::map< uintptr_t, RelatingStorableWptrT > WMapT;

        public:
          typedef std::vector< RelatingStorablePtrT > ListT;

        public:
          RelatedToManyProperty();

          bool
          isEmpty() const;

          void
          get(ListT& sharedPtrs) const;

          void
          addReference(RelatingStorableWptrT value);

          void
          removeReference(const uintptr_t relatingObjectPtrVal);

          void
          setup(StorableT* storable);

        public:
          /// ---------------
          /// BackRefI interface

          void
          get(std::vector< boost::shared_ptr< StorableI > >& storable) const;

        private:
          WMapT mRelatingObjects;

          StorableConfigI* mStorableConfig;
      };
  }
}

//! Adds the orm property to the class using a custom type
#define ORM_V1_DECLARE_RELATED_TO_MANY_PROPERTY(_get_name_, _name_, _relating_t_) \
     /* BEGIN _name_ RELATED TO MANY PROPERTY */ \
     private: \
       typedef orm::v1::RelatedToManyProperty<ModelT, ThisStorableT, _relating_t_> \
           _name_ ## PropT; \
       \
       typedef orm::v1::__related_property_private::RelatedToManyVisitor \
           < ModelT, _relating_t_, ThisStorableT> _name_ ## RelatedPropertyVisitorT; \
       \
       friend struct orm::v1::__related_property_private::RelatedToManyVisitor \
           < ModelT, _relating_t_, ThisStorableT>; \
       \
       friend class _relating_t_; \
       \
       _name_ ## PropT mProp ## _name_; \
       \
       void \
       _name_(_name_ ## RelatedPropertyVisitorT& visitor); \
       \
     public : \
       typedef std::vector< boost::shared_ptr< _relating_t_  > > _name_ ## ListT ; \
       \
       virtual void \
       _get_name_(_name_ ## ListT& sharedPtrs) const; \
       \
       virtual bool \
       is ## _name_ ## Empty() const; \
       \
     private: \
     /* END _name_ RELATED TO MANY PROPERTY */

//! Adds the orm property to the class using a custom type
#define ORM_V1_DEFINE_RELATED_TO_MANY_PROPERTY(_storable_t_, _get_name_, _name_, _relating_t_) \
   /* BEGIN _name_ RELATED TO MANY PROPERTY */ \
     \
     void \
     _storable_t_::_name_(_name_ ## RelatedPropertyVisitorT& visitor) \
     { \
       visitor(mProp ## _name_); \
     } \
     \
     void \
     _storable_t_::_get_name_(_name_ ## ListT& sharedPtrs) const \
     { \
       mProp ## _name_.get(sharedPtrs); \
     } \
     \
     bool \
     _storable_t_::is ## _name_ ## Empty() const \
     { \
       return mProp ## _name_.isEmpty(); \
     } \
     \
   /* END _name_ RELATED TO MANY PROPERTY */

#ifndef USE_ORM_V1_DLL
#include "RelatedToManyProperty.ini"
#endif

#endif /* _ORM_V1_RELATED_TO_MANY_PROPERTY_H_ */
