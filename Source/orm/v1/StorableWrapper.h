/*
 * StorableKey.h
 *
 *  Copyright NG SCADA Systems Pty Ltd 2011
 *
 *  This software is proprietary, you are not free to copy
 *  or redistribute this code in any format.
 *
 *  All rights to this software are reserved by 
 *  NG SCADA Systems Pty Ltd
 */

#ifndef _ORM_V1_STORABLE_WRAPPER_H_
#define _ORM_V1_STORABLE_WRAPPER_H_

#include <string>

#include <boost/shared_ptr.hpp>
#include <boost/weak_ptr.hpp>

/*! Storable Key Functor
 * This class is intended to hide the implementation of the storable
 * from the model and still allow the model to retrieve a the primary
 * key from the storable.
 *
 * Specialise this class
 *
 */
namespace orm
{

  namespace v1
  {
    class StorableI;
    class StorableConfigI;

    template<typename ModelT>
      class Model;

    template<typename StorableT, typename KeyT>
      class StorableKeyWrapper
      {
        public:
          KeyT
          key(const boost::shared_ptr< StorableT >& storable) const;

      };

    template<typename ModelT, typename StorableT>
      class StorableWrapper
      {
        public:
              StorableWrapper(boost::shared_ptr< StorableT > storable = boost::shared_ptr<
                  StorableT >());

          StorableWrapper(boost::shared_ptr< StorableI > storable, bool doDowncast);

          void
          nullPointerCheck() const;

          std::string
          name() const;

          boost::shared_ptr< StorableI >
          storableIPtr();

          /*! Storable
           * Used to create a new storable when default constructed
           * @return
           */
          boost::shared_ptr< StorableT >
          storable();

          StorableConfigI*
          config();

          void
          setModel(boost::weak_ptr< ModelT > model,
              boost::weak_ptr< Model< ModelT > > baseModel);

          bool
          initialiseModel(const int pass);

          std::string
          typeName() const;

        protected:
          boost::shared_ptr< StorableT > mStorable;
      };

  }

}

#define ORM_V1_DECLARE_SPECIALISED_STORABLE_KEY_WRAPPER( _storable_type_ , _key_type_ ) \
    namespace orm \
    { \
    \
      namespace v1 \
      { \
      \
        template <> \
          _key_type_ \
          StorableKeyWrapper< _storable_type_, _key_type_ >::key(const boost::shared_ptr< _storable_type_ >& storable) const; \
          \
      } \
    \
    } \


#define ORM_V1_DEFINE_SPECIALISED_STORABLE_KEY_WRAPPER( _storable_type_ , _key_type_, _key_func_name_ ) \
  template <> \
     _key_type_ \
    orm::v1::StorableKeyWrapper< _storable_type_, _key_type_ >::key(const boost::shared_ptr< _storable_type_ >& storable) const \
    { \
      assert(storable.get() != NULL); \
      return storable->_key_func_name_(); \
    } \

#endif /* _ORM_V1_STORABLE_WRAPPER_H_ */
