#include "MvLinkStandardValue.h"

#include "StorableI.h"
#include "StorableConfigI.h"
#include "SqlOpts.h"

namespace orm
{

  namespace v1
  {

    /// -----------------------------------------------------------------------
    /// MvLinkStandardValueVisitor implimentation
    /// -----------------------------------------------------------------------

    MvLinkI::StandardValueT
    MvLinkStandardValueVisitor::fromQVariant(const QVariant& qVar)
    {
      MvLinkI::StandardValueT stdVar;
      switch (ValueType)
      {
        case Bool:
        {
          bool ok = false;
          stdVar = (qVar.toInt(&ok) == Qt::Checked);
          assert(ok);
          break;
        }

        case Float:
        {
          bool ok = false;
          stdVar = qVar.toFloat(&ok);
          assert(ok);
          break;
        }

        case Double:
        {
          bool ok = false;
          stdVar = qVar.toDouble(&ok);
          assert(ok);
          break;
        }

        case Int16:
        {
          bool ok = false;
          stdVar = static_cast< int16_t > (qVar.toInt(&ok));
          assert(ok);
          break;
        }

        case Int32:
        {
          bool ok = false;
          stdVar = static_cast< int32_t > (qVar.toInt(&ok));
          assert(ok);
          break;
        }

        case Int64:
        {
          bool ok = false;
          stdVar = static_cast< int64_t > (qVar.toLongLong(&ok));
          assert(ok);
          break;
        }

        case String:
          stdVar = qVar.toString().toStdString();
          break;

        case Storable:
          assert(false);
          break;

        case InvalidValue:
          assert(false);
          break;

      }
      return stdVar;
    }

    void
    MvLinkStandardValueVisitor::operator()(MvLinkI::InvalidT& val)
    {
      ValueType = InvalidValue;
      Data = QVariant();
    }

    void
    MvLinkStandardValueVisitor::operator()(bool& val)
    {
      ValueType = Bool;
      Data = (val ? Qt::Checked : Qt::Unchecked);
    }

    void
    MvLinkStandardValueVisitor::operator()(float& val)
    {
      ValueType = Float;
      Data = val;
    }

    void
    MvLinkStandardValueVisitor::operator()(double& val)
    {
      ValueType = Double;
      Data = val;
    }

    void
    MvLinkStandardValueVisitor::operator()(int16_t& val)
    {
      ValueType = Int16;
      Data = val;
    }

    void
    MvLinkStandardValueVisitor::operator()(int32_t& val)
    {
      ValueType = Int32;
      Data = val;
    }

    void
    MvLinkStandardValueVisitor::operator()(int64_t& val)
    {
      ValueType = Int64;
      Data = val;
    }

    void
    MvLinkStandardValueVisitor::operator()(std::string& val)
    {
      ValueType = String;
      Data = QString(val.c_str());
    }

    void
    MvLinkStandardValueVisitor::operator()(boost::shared_ptr< StorableI >& val)
    {
      assert(val.get());
      assert(val->config());
      assert(val->config()->sqlOpts().get());
      ValueType = Storable;
      Data = QString(val->config()->sqlOpts()->name().c_str());
    }

  }
}
