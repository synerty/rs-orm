/*
 * RelatedPropertyPrivate.h
 *
 *  Created on: Feb 4, 2011
 *      Author: Jarrod Chesney
 */

#ifndef _ORM_V1_RELATED_PROPERTY_PRIVATE_H_
#define _ORM_V1_RELATED_PROPERTY_PRIVATE_H_

#include <sstream>

#include <QtSql/qsqlquery.h>
#include <QtSql/qsqlrecord.h>

#include <boost/optional.hpp>
#include <boost/format.hpp>

#include "Attribute.h"
#include "Exceptions.h"

/*! orm::v1::__private::RelatedPropertyPrivate Brief Description
 * Long comment about the class
 *
 */
namespace orm
{

  namespace v1
  {

    template<typename ModelT, typename StorableT, typename UnusedUniqueT,
        typename RelatedStorableT, typename DbValT>
      class RelatedProperty;

    template<typename ModelT, typename StorableT, typename RelatingObjectT>
      class RelatedToManyProperty;

    namespace __related_property_private
    {

      /// ---------------------------------------------------------------------
      /// RelatedPropertyRelatedToManyVisitor class
      /// ---------------------------------------------------------------------

      /*! Related Property - Related From Many - Visitor
       * This object is used to retrieve a reference to the related to many
       * property in the place of the RelatedProperty.
       * This is done because it requires less template arguments than
       * RelatedProperty, these arguments are arguments that RelatedToMany
       * would otherwise not need the user to supply.
       * To be clear, It asks the related storable for a reference to the
       * related to many property for this related propert.
       */
      template<typename ModelT, typename StorableT, typename RelatedStorableT>
        struct RelatedToManyVisitor
        {
            typedef RelatedToManyProperty< ModelT, RelatedStorableT, StorableT >
                ToManyPropertyT;

            typedef RelatedToManyVisitor< ModelT, StorableT, RelatedStorableT >
                ThisT;

            typedef void
            (RelatedStorableT::*VisitFunctionT)(ThisT&);

                RelatedToManyVisitor(boost::shared_ptr< RelatedStorableT >& relatedObject,
                    VisitFunctionT& visitFunc);

            void
            operator()(ToManyPropertyT& fromManyProperty);

            ToManyPropertyT* relatedToManyProperty;
        };

      /// ---------------------------------------------------------------------
      /// CopyDelegate class
      /// ---------------------------------------------------------------------

      template<typename ModelT, typename StorableT, typename UnusedUniqueT,
          typename RelatedStorableT, typename DbValT>
        struct CopyDelegate
        {
            CopyDelegate(RelatedProperty< ModelT, StorableT, UnusedUniqueT,
                RelatedStorableT, DbValT >& srcProp_);

            AttrI::PtrT
            operator()(DynamicStorable& dstStorable, AttrI::WptrT attrMeta);
        };
      /// ---------------------------------------------------------------------

      /// ---------------------------------------------------------------------
      /// CopyDelegate class - DynamicModel specialisation
      /// ---------------------------------------------------------------------

      template<typename UnusedUniqueT, typename RelatedStorableT,
          typename DbValT>
        struct CopyDelegate< DynamicModel, DynamicStorable, UnusedUniqueT,
            RelatedStorableT, DbValT >
        {
            typedef RelatedProperty< DynamicModel, DynamicStorable,
                UnusedUniqueT, RelatedStorableT, DbValT > PropT;

            PropT& srcProp;
            CopyDelegate(PropT& srcProp_);

            AttrI::PtrT
            operator()(DynamicStorable& dstStorable, AttrI::WptrT attrMeta);

        };

      /// ---------------------------------------------------------------------
      /// AttrMetaDelegate class
      /// ---------------------------------------------------------------------


      template<typename UnusedUniqueT, typename DbValT>
        struct AttrMetaDelegate : public AttrMetaI
        {
            /// ---------------
            /// AttrMetaI interface

            const std::string&
            FieldName() const;

            bool
            IsPrimaryKey() const;

            bool
            IsValidIfNull() const;

            boost::optional< DbValT >
            DefaultValue() const;

            /// ---------------
            /// Setters

            void
            setup(const std::string fieldName,
                const bool isPrimaryKey,
                const bool isValidIfNull,
                const boost::optional< DbValT > defaultValue);

          private:
            static std::string sFieldName;
            static bool sIsPrimaryKey;
            static bool sIsValidIfNull;
            static boost::optional< DbValT > sDefaultValue;
        };

      /// ---------------------------------------------------------------------
      /// AttrMetaDelegate class (DynamicModal specilisation)
      /// ---------------------------------------------------------------------

      template<typename DbValT>
        struct AttrMetaDelegate< DynamicStorable, DbValT > : public AttrMetaI
        {
            AttrMetaDelegate() :
              mIsPrimaryKey(false), mIsValidIfNull(false)
            {
            }

            /// ---------------
            /// Special setter for Dynamic specialisation

            void
            setAttrMeta(AttrI::WptrT attrMeta)
            {
              mAttrMeta = attrMeta;
            }

            /// ---------------
            /// AttrMetaI interface

            const std::string&
            FieldName() const
            {
              AttrI::PtrT attr = mAttrMeta.lock();
              if (attr.get() != NULL)
                return attr->FieldName();

              return mFieldName;
            }

            bool
            IsPrimaryKey() const
            {
              AttrI::PtrT attr = mAttrMeta.lock();
              if (attr.get() != NULL)
                return attr->IsPrimaryKey();

              return mIsPrimaryKey;
            }

            bool
            IsValidIfNull() const
            {
              AttrI::PtrT attr = mAttrMeta.lock();
              if (attr.get() != NULL)
                return attr->IsValidIfNull();

              return mIsValidIfNull;
            }

            /// ---------------
            /// Setters

            void
            setup(const std::string fieldName,
                const bool isPrimaryKey,
                const bool isValidIfNull,
                boost::optional< DbValT > defaultValue)
            {
              assert(mAttrMeta.lock().get() == NULL);
              mFieldName = fieldName;
              mIsPrimaryKey = isPrimaryKey;
              mIsValidIfNull = isValidIfNull;
              mDefaultValue = defaultValue;
            }

          private:
            // Initial/Template values
            std::string mFieldName;
            bool mIsPrimaryKey;
            bool mIsValidIfNull;
            boost::optional< DbValT > mDefaultValue;

          private:
            // values to use when copied
            AttrI::WptrT mAttrMeta;
        };

      /// ---------------------------------------------------------------------
      /// RelatedPropertyBindGet classes
      /// ---------------------------------------------------------------------

      template<typename ModelT, typename StorableT, typename UnusedUniqueT,
          typename RelatedStorableT, typename DbValT>
        class BindGetDelegate
        {
          public:
            BindGetDelegate(RelatedProperty< ModelT, StorableT, UnusedUniqueT,
                RelatedStorableT, DbValT >& prop);

            bool
            Bind(QSqlQuery& qry, const int& bindIndex);

            bool
            Get(QSqlQuery& qry, int fieldIndex, QVariant& value);

            const DbValT&
            InitStepDbValue() const;

            bool
            IsNull() const;

            bool
            initialiseModel();
        };
      /// ---------------------------------------------------------------------

      // Partial specialisation
      template<typename ModelT, typename StorableT, typename UnusedUniqueT,
          typename RelatedStorableT>
        class BindGetDelegate< ModelT, StorableT, UnusedUniqueT,
            RelatedStorableT, std::string >
        {
          public:
            BindGetDelegate(RelatedProperty< ModelT, StorableT, UnusedUniqueT,
                RelatedStorableT, std::string >& prop);

            inline bool
            Bind(QSqlQuery& qry, const int& bindIndex);

            inline bool
            Get(QSqlQuery& qry, int fieldIndex, QVariant& value);

            const std::string&
            InitStepDbValue() const;

            bool
            IsNull() const;

            bool
            initialiseModel();
          private:

            RelatedProperty< ModelT, StorableT, UnusedUniqueT,
                RelatedStorableT, std::string > & mProp;

            typename std::auto_ptr< std::string > mDbValue;
            bool mIsNull;

            static std::string sEmptyValue;
        };
      /// ---------------------------------------------------------------------


      template<typename ModelT, typename StorableT, typename UnusedUniqueT,
          typename RelatedStorableT>
        std::string BindGetDelegate< ModelT, StorableT, UnusedUniqueT,
            RelatedStorableT, std::string >::sEmptyValue;
      /// ---------------------------------------------------------------------

      // Partial specialisation
      template<typename ModelT, typename StorableT, typename UnusedUniqueT,
          typename RelatedStorableT>
        class BindGetDelegate< ModelT, StorableT, UnusedUniqueT,
            RelatedStorableT, uint64_t >
        {
          public:
            BindGetDelegate(RelatedProperty< ModelT, StorableT, UnusedUniqueT,
                RelatedStorableT, uint64_t >& prop);

            inline bool
            Bind(QSqlQuery& qry, const int& bindIndex);

            inline bool
            Get(QSqlQuery& qry, int fieldIndex, QVariant& value);

            const uint64_t&
            InitStepDbValue() const;

            bool
            IsNull() const;

            bool
            initialiseModel();

          private:
            RelatedProperty< ModelT, StorableT, UnusedUniqueT,
                RelatedStorableT, uint64_t > & mProp;
            uint64_t mDbValue;
            bool mIsNull;
        };
      /// ---------------------------------------------------------------------

      // Partial specialisation
      template<typename ModelT, typename StorableT, typename UnusedUniqueT,
          typename RelatedStorableT>
        class BindGetDelegate< ModelT, StorableT, UnusedUniqueT,
            RelatedStorableT, int64_t >
        {
          public:
            BindGetDelegate(RelatedProperty< ModelT, StorableT, UnusedUniqueT,
                RelatedStorableT, int64_t >& prop);

            inline bool
            Bind(QSqlQuery& qry, const int& bindIndex);

            inline bool
            Get(QSqlQuery& qry, int fieldIndex, QVariant& value);

            const int64_t&
            InitStepDbValue() const;

            bool
            IsNull() const;

            bool
            initialiseModel();

          private:
            RelatedProperty< ModelT, StorableT, UnusedUniqueT,
                RelatedStorableT, int64_t > & mProp;
            int64_t mDbValue;
            bool mIsNull;
        };
      /// ---------------------------------------------------------------------

      // Partial specialisation
      template<typename ModelT, typename StorableT, typename UnusedUniqueT,
          typename RelatedStorableT>
        class BindGetDelegate< ModelT, StorableT, UnusedUniqueT,
            RelatedStorableT, int32_t >
        {
          public:
            BindGetDelegate(RelatedProperty< ModelT, StorableT, UnusedUniqueT,
                RelatedStorableT, int32_t >& prop);

            inline bool
            Bind(QSqlQuery& qry, const int& bindIndex);

            inline bool
            Get(QSqlQuery& qry, int fieldIndex, QVariant& value);

            const int32_t&
            InitStepDbValue() const;

            bool
            IsNull() const;

            bool
            initialiseModel();

          private:
            RelatedProperty< ModelT, StorableT, UnusedUniqueT,
                RelatedStorableT, int32_t > & mProp;
            int32_t mDbValue;
            bool mIsNull;
        };
      /// ---------------------------------------------------------------------

      // Partial specialisation
      template<typename ModelT, typename StorableT, typename UnusedUniqueT,
          typename RelatedStorableT>
        class BindGetDelegate< ModelT, StorableT, UnusedUniqueT,
            RelatedStorableT, int16_t >
        {
          public:
            BindGetDelegate(RelatedProperty< ModelT, StorableT, UnusedUniqueT,
                RelatedStorableT, int16_t >& prop);

            inline bool
            Bind(QSqlQuery& qry, const int& bindIndex);

            inline bool
            Get(QSqlQuery& qry, int fieldIndex, QVariant& value);

            const int16_t&
            InitStepDbValue() const;

            bool
            IsNull() const;

            bool
            initialiseModel();

          private:
            RelatedProperty< ModelT, StorableT, UnusedUniqueT,
                RelatedStorableT, int16_t > & mProp;
            int16_t mDbValue;
            bool mIsNull;
        };
    /// ---------------------------------------------------------------------

    }

  }

}


#ifndef USE_ORM_V1_DLL
#include "RelatedPropertyPrivate.ini"
#endif

#endif /* _ORM_V1_RELATED_PROPERTY_PRIVATE_H_ */
