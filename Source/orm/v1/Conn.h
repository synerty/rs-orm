/*
 * Conn.h
 *
 *  Created on: Feb 7, 2011
 *      Author: Jarrod Chesney
 */

#ifndef _ORM_V1_CONN_H_
#define _ORM_V1_CONN_H_

#if defined(BUILD_ORM_V1_DLL)
#define DECL_ORM_V1_DIR __declspec(dllexport)
#elif defined(USE_ORM_V1_DLL)
#define DECL_ORM_V1_DIR __declspec(dllimport)
#else
#define DECL_ORM_V1_DIR
#endif

#include <string>

/*! orm::v1::Conn1 Brief Description
 * Long comment about the class
 *
 */
namespace orm
{
  namespace v1
  {
    struct Conn
    {
        enum TypeE
        {
          InvalidType,
          PostGreSQL,
          Oracle,
          SQLite,
          MsAccess
        };

        TypeE Type;
        //! Database or File Name
        std::string Database;

        std::string Host;
        int Port;
        std::string Username;
        std::string Password;

        Conn();

        std::string
        toString() const;

        void setTypeFromDatabaseName();
    };

  }
}

#endif /* _ORM_V1_CONN_H_ */
