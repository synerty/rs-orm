/*
 * StorageWrapper.h
 *
 *  Copyright NG SCADA Systems Pty Ltd 2011
 *
 *  This software is proprietary, you are not free to copy
 *  or redistribute this code in any format.
 *
 *  All rights to this software are reserved by 
 *  NG SCADA Systems Pty Ltd
 */

#ifndef _ORM_V1_STORAGE_WRAPPER_H_
#define _ORM_V1_STORAGE_WRAPPER_H_

#include <map>
#include <vector>
#include <string>
#include <sstream>

#include <boost/shared_ptr.hpp>
#include <boost/weak_ptr.hpp>
#include <boost/optional.hpp>

class QSqlDatabase;

#include "Storage.h"
#include "StorageI.h"
#include "StorableWrapper.h"

/*! orm::v1::StorageWrapper Brief Description
 * Long comment about the class
 *
 */
namespace orm
{

  namespace v1
  {

    namespace _private_storable_wrapper
    {

      template<class StorableT>
        bool
        removeFromVector(std::vector< boost::shared_ptr< StorableT > >& list,
            boost::shared_ptr< StorableT >& obj);

      template<typename ModelT, typename StorableT, typename KeyT>
        void
        removeStorable(//
        Model< ModelT >& model,
            std::vector< boost::shared_ptr< StorableT > >& allList, //
            std::vector< boost::shared_ptr< StorableT > >& managedList, //
            std::map< const KeyT, boost::shared_ptr< StorableT > >& map,
            boost::shared_ptr< StorableT >& obj);

      template<typename ModelT, typename StorableT, typename KeyT>
        void
        addStorable(//
        Model< ModelT >& model,
            std::vector< boost::shared_ptr< StorableT > >& allList, //
            std::map< const KeyT, boost::shared_ptr< StorableT > >& map,
            boost::shared_ptr< StorableT >& obj,
            std::vector< boost::shared_ptr< StorableT > >* managedList = NULL);

      template<typename StorableT, typename KeyT>
        boost::shared_ptr< StorableT >
            getStorable(std::map< const KeyT, boost::shared_ptr< StorableT > >& map,
                const KeyT& key);

      template<typename StorableT, typename KeyT>
        const boost::shared_ptr< StorableT >
            getStorable(const std::map< const KeyT, boost::shared_ptr<
                StorableT > >& map,
                const KeyT& key);

    }
    /// -----------------------------------------------------------------------


    template<typename ModelT>
      class StorageWrapperI
      {
        public:

          virtual StorageI< ModelT >&
          storage() = 0;

          virtual void
          clear() = 0;

          virtual void
          loadCompletedEvent() = 0;

          virtual std::string
          storableTypeName() = 0;

          virtual void
          setup(Model< ModelT >& model, QSqlDatabase& db) = 0;

      };
    /// -----------------------------------------------------------------------

    template<typename ModelT, typename StorableT, typename KeyT>
      class StorageWrapper : public StorageWrapperI< ModelT >
      {
        public:
          typedef boost::shared_ptr< StorableT > StorablePtrT;
          typedef std::map< const KeyT, StorablePtrT > MapT;
          typedef std::vector< StorablePtrT > ListT;

        public:
          StorageWrapper();

          virtual
          ~StorageWrapper();

        public:
          /// ---------------
          /// Initialise

          void
          setup(Model< ModelT >& model, QSqlDatabase& db);

        public:
          /// ---------------
          /// Public storable accessors

          const ListT&
          list();

          const ListT&
          list() const;

          const MapT&
          map() const;

          void
          add(StorablePtrT obj,
              boost::weak_ptr< ModelT > model,
              boost::weak_ptr< Model< ModelT > > baseModel);

          void
          remove(StorablePtrT obj);

          bool
          contains(const StorablePtrT& obj) const;

          StorablePtrT
          get(const KeyT& id);

          const StorablePtrT
          get(const KeyT& id) const;

        public:
          /// ---------------
          /// Private storable accessors
          // For use by the model

          // Specialised only
          void
          addDerrived(StorablePtrT obj);

        private:
          /// ---------------
          /// storage accessors

          StorageI< ModelT >&
          storage();

          void
          clear();

          // Specialised only
          void
          loadCompletedEvent();

          // Specialised only
          std::string
          storableTypeName();

        private:
          friend class Model< ModelT > ;

          boost::optional< Model< ModelT >& > mModel;

          boost::shared_ptr< StorageI< ModelT > > mStorage;

          ListT mManagedList;
          /* List containing derrived objects as well */
          ListT mAllList;
          boost::shared_ptr< MapT > mMap;

      };
  /// -------------------------------------------------------------------------

  }

}

#endif /* _ORM_V1_STORAGE_WRAPPER_H_ */
