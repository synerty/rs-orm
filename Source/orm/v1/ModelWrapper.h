/*
 * ModelWrapper.h
 *
 *  Copyright NG SCADA Systems Pty Ltd 2011
 *
 *  This software is proprietary, you are not free to copy
 *  or redistribute this code in any format.
 *
 *  All rights to this software are reserved by 
 *  NG SCADA Systems Pty Ltd
 */

#ifndef _ORM_V1_MODEL_WRAPPER_H_
#define _ORM_V1_MODEL_WRAPPER_H_

#include <boost/shared_ptr.hpp>

/*! orm::v1::ModelWrapper Brief Description
 * Long comment about the class
 *
 */
namespace orm
{

  namespace v1
  {

    template<typename ModelT, typename StorableT>
      struct ModelWrapper
      {
          static void
          add(boost::shared_ptr< ModelT > model,
              boost::shared_ptr< StorableT > storable);

          static void
          remove(boost::shared_ptr< ModelT > model,
              boost::shared_ptr< StorableT > storable);

          static bool
          contains(boost::shared_ptr< ModelT > model,
              boost::shared_ptr< StorableT > storable);

      };

  }

}

#endif /* _ORM_V1_MODEL_WRAPPER_H_ */
