/*
 * StorableConfig.h
 *
 *  Created on: Feb 10, 2011
 *      Author: Jarrod Chesney
 */

#ifndef _ORM_V1_STORABLE_CONFIG_H_
#define _ORM_V1_STORABLE_CONFIG_H_

#if defined(BUILD_ORM_V1_DLL)
#define DECL_ORM_V1_DIR __declspec(dllexport)
#elif defined(USE_ORM_V1_DLL)
#define DECL_ORM_V1_DIR __declspec(dllimport)
#else
#define DECL_ORM_V1_DIR
#endif

#include <boost/shared_ptr.hpp>
#include <boost/format.hpp>

#include "StorableConfigI.h"
#include "SqlOpts.h"
#include "Attribute.h"
#include "StorableState.h"
#include "StorableI.h"

namespace orm
{

  namespace v1
  {

    template<class StorableT>
      class DECL_ORM_V1_DIR StorableConfig : public StorableConfigI
      {
        public:
          StorableConfig(StorableConfigI* fields);

          virtual
          ~StorableConfig();

          void
          setup(StorableI& storable);

          StorableState&
          state();

          StorableI&
          storable();

          void
          setUpdated();

          StorableConfigI*
          inheritsFrom();

          bool
          isValid();

          bool
          isPrimaryKeyValid();

          std::string
          invalidReason();

          void
          clearModel();

          void
          setup();

          void
          tearDown();

        public:
          /// ---------------
          /// Setup
          /*! Add Attribute
           * Adds a field to this storable object.
           * @param field the attribute to use as the field
           * Ownership of field is not transferred to this class
           */
          void
          addField(AttrI* field);

          AttrsT&
          fields();

          void
          addBackRef(BackRefI* backRef);

          const BackRefsT&
          backRefs() const ;

        public:
          /// ---------------
          /// Instance accesors for static data

          SqlOpts::PtrT
          sqlOpts();

          const SqlOpts::PtrT&
          sqlOpts() const;

        public:
          /// ---------------
          /// Setup - Static members

          static SqlOpts::PtrT
          setSqlOpts();

          static bool
          addFieldMeta(AttrI::PtrT field);

        protected:
          friend class StorageSql;

          const AttrPtrsT&
          fieldMetas() const;

        private:
          // Pointer to storable on the stack (owner of this config)
          StorableI* mStorable;

          // Pointer to Config on the stack
          StorableConfigI* mInheritsFrom;

          StorableState mState;

          static SqlOpts::PtrT sSqlOpts;

          // Pointers to propeties stored on the stack
          AttrsT mFields;

          BackRefsT mBackRefs;

          static AttrPtrsT sFieldMetas;
      };

  }

}

#ifndef USE_ORM_V1_DLL
#include "StorableConfig.ini"
#endif

#endif /* _ORM_V1_STORABLE_CONFIG_H_ */
