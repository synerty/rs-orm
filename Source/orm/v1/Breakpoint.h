/*
 * Breakpoint.h
 *
 *  Copyright NG SCADA Systems Pty Ltd 2011
 *
 *  This software is proprietary, you are not free to copy
 *  or redistribute this code in any format.
 *
 *  All rights to this software are reserved by 
 *  NG SCADA Systems Pty Ltd
 */

#ifndef BREAKPOINT_H_
#define BREAKPOINT_H_

/*! orm::v1::Breakpoint Brief Description
 * Long comment about the class
 *
 */
namespace orm
{

  namespace v1
  {

    void
    breakpoint();

  }

}

#endif /* BREAKPOINT_H_ */
