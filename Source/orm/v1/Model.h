/*
 * Model.h
 *
 *  Created on: Dec 18, 2010
 *      Author: Administrator
 */

#ifndef _ORM_V1_MODEL_H_
#define _ORM_V1_MODEL_H_

#if defined(BUILD_ORM_V1_DLL)
#define DECL_ORM_V1_DIR __declspec(dllexport)
#elif defined(USE_ORM_V1_DLL)
#define DECL_ORM_V1_DIR __declspec(dllimport)
#else
#define DECL_ORM_V1_DIR
#endif

#include <string>
#include <vector>
#include <deque>
#include <map>
#include <stdint.h>

#include <boost/function.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/enable_shared_from_this.hpp>
#include <boost/lexical_cast.hpp>

#include <QtSql/qsqldatabase.h>
#include <QtCore/qthread.h>
#include <QtCore/qmutex.h>

#include "Storage.h"
#include "Conn.h"
#include "Exceptions.h"
#include "StorableChange.h"
#include "StorableWrapper.h"
#include "StorageWrapper.h"
namespace orm
{
  namespace v1
  {
    struct KeyMapInserterI;

    template<typename ModelT>
      class Model;

    template<typename ModelT>
      class StorableA;

    //    // Thread class
    //    class ModelSaver : public QObject //QThread
    //    {
    //      Q_OBJECT
    //
    //      public:
    //        ModelSaver(Model& model);
    //
    //      protected:
    //        void
    //        run();
    //
    //      signals:
    //        void
    //        SaveError();
    //
    //      private:
    //        Model& mModel;
    //    };

    class BoolFlagKeeper
    {
      public:
        BoolFlagKeeper(bool& flag) :
          mFlag(flag)
        {
          mFlag = true;
        }

        ~BoolFlagKeeper()
        {
          mFlag = false;
        }
      private:
        bool& mFlag;
    };

    /*! Model Brief Description
     * Long comment about the class
     *
     */
    template<typename ModelT>
      class DECL_ORM_V1_DIR Model : public boost::enable_shared_from_this<
          ModelT >
      {

        public:
          typedef boost::weak_ptr< ModelT > WptrT;
          typedef boost::shared_ptr< ModelT > PtrT;
          typedef Model< ModelT > BaseModel;
          typedef ModelT ThisModel;

        public:
          Model(const Conn& ormConn);

          virtual
          ~Model();

        public:

          bool
          load();

          bool
          save();

          bool
          forceSave();

          void
          beginTransaction();

          void
          commitTransaction();

          void
          rollbackTransaction();

          void
          setAutoSave(bool enabled);

          bool
          openDb();

          bool
          closeDb();

          QSqlDatabase&
          database();

          bool
          isInitialised() const;

          void
          setDebug(const bool enabled);

          bool
          isReadOnly() const;

          void
          setReadOnly(const bool enabled = true);

        public:
          /// ---------------
          /// Interface for storables

          void
          queChange(StorableChange& change);

        protected:
          void
          addStorageWrapper(StorageWrapperI< ModelT >& wrapper);

          virtual bool
          initialiseModel();

          virtual bool
          initialiseModel(const int pass);

        protected:
          QSqlDatabase mDb;
          Conn mConn;

          typedef boost::shared_ptr< StorageI< ModelT > > StoragePtrT;

          typedef std::map< std::string, StorageWrapperI< ModelT >* >
              StorageWrapperMapT;
          StorageWrapperMapT mStorageWrappers;

        private:

          bool
          isSaveRequired() const;

          void
          setupStorages() ;

          bool mIsInTransaction;
          bool mLoadInProgres;
          bool mInitialised;
          bool mStoragesSetup;
          bool mIsReadOnly;

          std::deque< StorableChange > mStorableChangeQue;

          const std::string mDbConnName;

          QMutex mAutoSaveMutex;
          bool mAutoSave;

          mutable QMutex mSorageMutex;
          mutable QMutex mQueMutex;

          //          ModelSaver mModelSaver;
          //          friend class ModelSaver;

      };
  /// ---------------------------------------------------------------------------
  }
}

#define ORM_V1_MODEL_DECLARE_STORABLE(_object_type_, _name_, _key_type_) \
  public: \
    typedef _key_type_ _name_ ## KeyT; \
    typedef boost::shared_ptr< _object_type_ > \
         _name_ ## PtrT; \
    typedef std::map< const _name_ ## KeyT \
        ,  _name_ ## PtrT > \
          _name_ ## MapT; \
    typedef std::vector< _name_ ## PtrT > \
         _name_ ## ListT; \
    \
    void \
    add ## _name_ (_name_ ## PtrT obj); \
    \
    void \
    remove ## _name_ (_name_ ## PtrT obj); \
    \
    const _name_ ## ListT& \
    _name_ ## s(); \
    \
    const _name_ ## ListT& \
    _name_ ## s() const; \
    \
    const _name_ ## MapT& \
    _name_ ## sMap() const; \
    \
    _name_ ## PtrT \
    _name_(const _name_ ## KeyT& id); \
    \
    const _name_ ## PtrT \
    _name_(const _name_ ## KeyT& id) const; \
    \
  private: \
    friend class _object_type_; \
    \
    friend class orm::v1::ModelWrapper<ThisModel, _object_type_>; \
    \
    void \
    addStorable(boost::shared_ptr< _object_type_ >& obj); \
    \
    void \
    removeStorable(boost::shared_ptr< _object_type_ >& obj); \
    \
    bool \
    containsStorable(const boost::shared_ptr< _object_type_ >& obj) const; \
    \
    void \
    addDerrived ## _name_ (_name_ ## PtrT obj); \
    \
    orm::v1::StorageWrapper< ThisModel, _object_type_, _key_type_ > \
      m ## _name_ ## StorageWrapper; \


#define ORM_V1_MODEL_DEFINE_STORABLE(_model_type_, _object_type_, _name_, _key_type_) \
    \
    void \
    _model_type_::addStorable(boost::shared_ptr< _object_type_ >& obj) \
    { \
      m ## _name_ ## StorageWrapper.add( \
          obj, \
          shared_from_this(), \
          shared_from_this()); \
    } \
    \
    void \
    _model_type_::removeStorable(boost::shared_ptr< _object_type_ >& obj) \
    { \
      m ## _name_ ## StorageWrapper.remove(obj); \
    } \
    \
    bool \
    _model_type_::containsStorable(const boost::shared_ptr< _object_type_ >& obj) const \
    { \
      return m ## _name_ ## StorageWrapper.contains(obj); \
    } \
    \
    void \
    _model_type_::add ## _name_ (_name_ ## PtrT obj) \
    { \
      m ## _name_ ## StorageWrapper.add( \
          obj, \
          shared_from_this(), \
          shared_from_this()); \
    } \
    \
    void \
    _model_type_::remove ## _name_ (_name_ ## PtrT obj) \
    { \
      m ## _name_ ## StorageWrapper.remove(obj); \
    } \
    \
    void \
    _model_type_::addDerrived ## _name_ (_name_ ## PtrT obj) \
    { \
      m ## _name_ ## StorageWrapper.addDerrived(obj); \
    } \
    \
    \
    const _model_type_::_name_ ## ListT& \
    _model_type_::_name_ ## s() \
    { \
      return m ## _name_ ## StorageWrapper.list(); \
    } \
    \
    const _model_type_::_name_ ## ListT& \
    _model_type_::_name_ ## s() const \
    { \
      return m ## _name_ ## StorageWrapper.list(); \
    } \
    \
    const _model_type_::_name_ ## MapT& \
    _model_type_::_name_ ## sMap() const \
    { \
      return m ## _name_ ## StorageWrapper.map(); \
    } \
    \
    _model_type_::_name_ ## PtrT \
    _model_type_::_name_(const _name_ ## KeyT& id) \
    { \
      return m ## _name_ ## StorageWrapper.get(id); \
    } \
    \
    const _model_type_::_name_ ## PtrT \
    _model_type_::_name_(const _name_ ## KeyT& id) const \
    { \
      return m ## _name_ ## StorageWrapper.get(id); \
    } \

#ifndef USE_ORM_V1_DLL
#include "Model.ini"
#endif

#endif /* _ORM_V1_MODEL_H_ */
