/*
 * PropertyPrivate.h
 *
 *  Created on: Feb 7, 2011
 *      Author: Jarrod Chesney
 */

#ifndef _ORM_V1_SIMPLE_PROPERTY_PRIVATE_H_
#define _ORM_V1_SIMPLE_PROPERTY_PRIVATE_H_

#include <stdint.h>
#include <string>

#include <QtCore/qvariant.h>
#include <QtSql/qsqlquery.h>

#include <boost/optional.hpp>

#include "Attribute.h"

namespace orm
{
  namespace v1
  {
    template<typename ModelT, class StorableT, typename UnusedUniqueT,
        typename TypeT>
      class SimpleProperty;

    class DynamicModel;
    class DynamicStorable;
    class DynamicStorableConfig;

    namespace __property_private
    {

      /// ---------------------------------------------------------------------
      /// Copy class
      /// ---------------------------------------------------------------------


      template<typename ModelT, class StorableT, typename UnusedUniqueT,
          typename TypeT, typename PropT>
        struct CopyDelegate
        {
            AttrI::PtrT
            operator()(DynamicStorable& dstStorable, AttrI::WptrT attrMeta)
            {
              /// Only dynamic objects are copyable
              assert(false);
              return AttrI::PtrT();
            }
        };

      template<typename UnusedUniqueT, typename TypeT, typename PropT>
        struct CopyDelegate< DynamicModel, DynamicStorable, UnusedUniqueT,
            TypeT, PropT >
        {
            AttrI::PtrT
            operator()(DynamicStorable& dstStorable, AttrI::WptrT attrMeta)
            {
              std::auto_ptr< PropT > propPtr(new PropT());
              propPtr->setup(&dstStorable);
              propPtr->mAttrMetaDelegate.setAttrMeta(attrMeta);
              AttrI::PtrT ptr(propPtr.release());
              return ptr;
            }

        };

      /// ---------------------------------------------------------------------
      /// AttrMetaDelegate class
      /// ---------------------------------------------------------------------


      template<typename UnusedUniqueT, typename TypeT>
        struct AttrMetaDelegate : public AttrMetaI
        {
            /// ---------------
            /// AttrMetaI interface

            const std::string&
            FieldName() const
            {
              return sFieldName;
            }

            bool
            IsPrimaryKey() const
            {
              return sIsPrimaryKey;
            }

            bool
            IsValidIfNull() const
            {
              return sIsValidIfNull;
            }

            boost::optional< TypeT >
            DefaultValue() const
            {
              return sDefaultValue;
            }

            /// ---------------
            /// Setters

            void
            setup(const std::string fieldName,
                const bool isPrimaryKey,
                const bool isValidIfNull,
                const boost::optional< TypeT > defaultValue)
            {
              sFieldName = fieldName;
              sIsPrimaryKey = isPrimaryKey;
              sIsValidIfNull = isValidIfNull;
              sDefaultValue = defaultValue;
            }

          private:
            static std::string sFieldName;
            static bool sIsPrimaryKey;
            static bool sIsValidIfNull;
            static boost::optional< TypeT > sDefaultValue;
        };

      /// ---------------
      /// Initialisers

      template<typename UnusedUniqueT, typename TypeT>
        std::string AttrMetaDelegate< UnusedUniqueT, TypeT >::sFieldName
        __attribute__((init_priority(65000)));
      /// ---------------------------------------------------------------------

      template<typename UnusedUniqueT, typename TypeT>
        bool AttrMetaDelegate< UnusedUniqueT, TypeT >::sIsPrimaryKey = false;
      /// ---------------------------------------------------------------------

      template<typename UnusedUniqueT, typename TypeT>
        bool AttrMetaDelegate< UnusedUniqueT, TypeT >::sIsValidIfNull = true;
      /// ---------------------------------------------------------------------

      template<typename UnusedUniqueT, typename TypeT>
        boost::optional< TypeT >
            AttrMetaDelegate< UnusedUniqueT, TypeT >::sDefaultValue
        __attribute__((init_priority(65000)));
      /// ---------------------------------------------------------------------

      /// ---------------------------------------------------------------------
      /// AttrMetaDelegate class (DynamicModal specilisation)
      /// ---------------------------------------------------------------------

      template<typename TypeT>
        struct AttrMetaDelegate< DynamicStorable, TypeT > : public AttrMetaI
        {
            AttrMetaDelegate() :
              mIsPrimaryKey(false), mIsValidIfNull(false)
            {
            }

            /// ---------------
            /// Special setter for Dynamic specialisation

            void
            setAttrMeta(AttrI::WptrT attrMeta)
            {
              mAttrMeta = attrMeta;

              // Get the default value from the property.
              AttrI::PtrT locked = attrMeta.lock();
              assert(locked.get() != NULL);

              PropertyI< TypeT >
                  * prop = dynamic_cast< PropertyI< TypeT >* > (locked.get());
              assert(prop != NULL);
              mDefaultValue = prop->defaultValue();
            }

            /// ---------------
            /// AttrMetaI interface

            const std::string&
            FieldName() const
            {
              AttrI::PtrT attr = mAttrMeta.lock();
              if (attr.get() != NULL)
                return attr->FieldName();

              return mFieldName;
            }

            bool
            IsPrimaryKey() const
            {
              AttrI::PtrT attr = mAttrMeta.lock();
              if (attr.get() != NULL)
                return attr->IsPrimaryKey();

              return mIsPrimaryKey;
            }

            bool
            IsValidIfNull() const
            {
              AttrI::PtrT attr = mAttrMeta.lock();
              if (attr.get() != NULL)
                return attr->IsValidIfNull();

              return mIsValidIfNull;
            }

            boost::optional< TypeT >
            DefaultValue() const
            {
              // Default value is handled differently due to being type dependent.
              return mDefaultValue;
            }

            /// ---------------
            /// Setters

            void
            setup(const std::string fieldName,
                const bool isPrimaryKey,
                const bool isValidIfNull,
                const boost::optional< TypeT > defaultValue)
            {
              assert(mAttrMeta.lock().get() == NULL);
              mFieldName = fieldName;
              mIsPrimaryKey = isPrimaryKey;
              mIsValidIfNull = isValidIfNull;
              mDefaultValue = defaultValue;
            }

          private:
            /// Initial/Template values
            std::string mFieldName;
            bool mIsPrimaryKey;
            bool mIsValidIfNull;
            boost::optional< TypeT > mDefaultValue;

          private:
            /// values to use when copied
            AttrI::WptrT mAttrMeta;
        };

      /// ---------------------------------------------------------------------
      /// BindGetDelegate class
      /// ---------------------------------------------------------------------

      template<typename TypeT>
        class BindGetDelegate
        {
            /// ---------------
            /// AttributeI interface
          public:
            void
            InitialiseValue(TypeT& propValue);

            virtual bool
            Bind(const TypeT& propValue, QSqlQuery& qry, const int& bindIndex)
            {
              qry.bindValue(bindIndex, propValue);
              return true;
            }

            virtual bool
            Get(TypeT& propValue,
                QSqlQuery& qry,
                int fieldIndex,
                QVariant& qryValue);
        };

      /// -----------------------------------
      /// std::string specialisation

      template<>
        void
        BindGetDelegate< std::string >::InitialiseValue(std::string& propValue);

      template<>
        bool
        BindGetDelegate< std::string >::Bind(const std::string& propValue,
            QSqlQuery& qry,
            const int& bindIndex);

      template<>
        bool
        BindGetDelegate< std::string >::Get(std::string& propValue,
            QSqlQuery& qry,
            int fieldIndex,
            QVariant& qryValue);

      /// -----------------------------------
      /// int specialisation

      template<>
        void
        BindGetDelegate< int >::InitialiseValue(int& propValue);

      template<>
        bool
        BindGetDelegate< int >::Get(int& propValue,
            QSqlQuery& qry,
            int fieldIndex,
            QVariant& qryValue);

      /// -----------------------------------
      /// int16_t specialisation

      template<>
        void
        BindGetDelegate< int16_t >::InitialiseValue(int16_t& propValue);

      template<>
        bool
        BindGetDelegate< int16_t >::Get(int16_t& propValue,
            QSqlQuery& qry,
            int fieldIndex,
            QVariant& qryValue);

      /// -----------------------------------
      /// unsigned int specialisation

      template<>
        void
            BindGetDelegate< unsigned int >::InitialiseValue(unsigned int& propValue);

      template<>
        bool
        BindGetDelegate< unsigned int >::Get(unsigned int& propValue,
            QSqlQuery& qry,
            int fieldIndex,
            QVariant& qryValue);

      /// -----------------------------------
      /// 64bit unsigned int specialisation

      template<>
        void
        BindGetDelegate< uint64_t >::InitialiseValue(uint64_t& propValue);

      template<>
        bool
        BindGetDelegate< uint64_t >::Get(uint64_t& propValue,
            QSqlQuery& qry,
            int fieldIndex,
            QVariant& qryValue);

      /// -----------------------------------
      /// 64bit int specialisation

      template<>
        void
        BindGetDelegate< int64_t >::InitialiseValue(int64_t& propValue);

      template<>
        bool
        BindGetDelegate< int64_t >::Get(int64_t& propValue,
            QSqlQuery& qry,
            int fieldIndex,
            QVariant& qryValue);

      /// -----------------------------------

      template<>
        void
        BindGetDelegate< bool >::InitialiseValue(bool& propValue);

      template<>
        bool
        BindGetDelegate< bool >::Get(bool& propValue,
            QSqlQuery& qry,
            int fieldIndex,
            QVariant& qryValue);

      /// -----------------------------------
      /// double specialisation

      template<>
        void
        BindGetDelegate< double >::InitialiseValue(double& propValue);

      template<>
        bool
        BindGetDelegate< double >::Get(double& propValue,
            QSqlQuery& qry,
            int fieldIndex,
            QVariant& qryValue);

      /// -----------------------------------
      /// double specialisation

      template<>
        void
        BindGetDelegate< float >::InitialiseValue(float& propValue);

      template<>
        bool
        BindGetDelegate< float >::Get(float& propValue,
            QSqlQuery& qry,
            int fieldIndex,
            QVariant& qryValue);
    }
  }
}

#endif /* _ORM_V1_SIMPLE_PROPERTY_PRIVATE_H_ */
