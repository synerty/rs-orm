/*
 * Exceptions.h
 *
 *  Created on: Feb 10, 2011
 *      Author: Jarrod Chesney
 */

#ifndef _ORM_V1_EXCEPTIONS_H_
#define _ORM_V1_EXCEPTIONS_H_

#if defined(BUILD_ORM_V1_DLL)
#define DECL_ORM_V1_DIR __declspec(dllexport)
#elif defined(USE_ORM_V1_DLL)
#define DECL_ORM_V1_DIR __declspec(dllimport)
#else
#define DECL_ORM_V1_DIR
#endif

#include <stdexcept>
#include <string>

namespace orm
{
  namespace v1
  {

    class InvalidJoinException : public std::runtime_error
    {
      public:
        InvalidJoinException() :
          std::runtime_error("Attempt to join from lowest level table")
        {
        }
    };

    // ---------------
    // Storage exceptions

    class StorableDataInvalidException : public std::runtime_error
    {
      public:
        StorableDataInvalidException(std::string info) :
          std::runtime_error("storable contains invalid data\n" + info)
        {
        }
    };

    class EmptyTableNameAndQuerySqlException : public std::runtime_error
    {
      public:
        EmptyTableNameAndQuerySqlException() :
          std::runtime_error("No table name of sql query has been set")
        {
        }
    };

    class ExecuteSqlException : public std::runtime_error
    {
      public:
        ExecuteSqlException(const std::string& info) :
          std::runtime_error(info)
        {
        }
    };

    class TransactionException : public std::runtime_error
    {
      public:
        TransactionException(const std::string& info) :
          std::runtime_error(info)
        {
        }
    };

    // ---------------
    // Storable exceptions

    class InvalidDowncastException : public std::runtime_error
    {
      public:
        InvalidDowncastException(const std::string& info) :
          std::runtime_error(info)
        {
        }
    };

    class StorableConfigI;
    class DECL_ORM_V1_DIR AccessingNullValueException : public std::runtime_error
    {
      public:
        AccessingNullValueException(const StorableConfigI* storableConfig,
            const std::string& fieldName);
    };

    // ---------------
    // Modal exceptions

    struct InvalidObjectException : std::runtime_error
    {
        InvalidObjectException(const std::string& info) :
          std::runtime_error(info)
        {
        }
    };

    struct ObjectDoesNotExistException : std::runtime_error
    {
        ObjectDoesNotExistException(const std::string& info) :
          std::runtime_error(info)
        {
        }
    };

    struct DuplicateObjectException : std::runtime_error
    {
        DuplicateObjectException(const std::string& info) :
          std::runtime_error(info)
        {
        }
    };

    struct DatabaseConnectionException : std::runtime_error
    {
        DatabaseConnectionException(const std::string& info) :
          std::runtime_error(info)
        {
        }
    };

    struct ModelIsReadOnlyException : std::runtime_error
    {
        ModelIsReadOnlyException(const std::string& info) :
          std::runtime_error(info)
        {
        }
    };

    // ---------------
    // Property exceptions

    struct ModelNotSetException : std::runtime_error
    {
        ModelNotSetException(const std::string& info) :
          std::runtime_error(info)
        {
        }
    };

    struct QueryBindException : std::runtime_error
    {
        QueryBindException(const std::string& info) :
          std::runtime_error(info)
        {
        }
    };

    struct QueryGetException : std::runtime_error
    {
        QueryGetException(const std::string& info) :
          std::runtime_error(info)
        {
        }
    };

    // ---------------
    // Mapped Property exceptions

    struct UnmappedValueException : std::runtime_error
    {
        UnmappedValueException(const std::string& info) :
          std::runtime_error(info)
        {
        }
    };

    // ---------------
    // Related Property exceptions

    struct RelatedObjectGetException : std::runtime_error
    {
        RelatedObjectGetException(const std::string& info) :
          std::runtime_error(info)
        {
        }
    };

    // ---------------
    // Util exceptions

    struct GetOneBackrefException : std::runtime_error
    {
        GetOneBackrefException(const std::string& info) :
          std::runtime_error(info)
        {
        }
    };

  }
}

#endif /* _ORM_V1_EXCEPTIONS_H_ */
