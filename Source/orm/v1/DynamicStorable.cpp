/*
 * DynamicStorable.cpp
 *
 *  Created on: Feb 8, 2011
 *      Author: Jarrod Chesney
 */

#include "DynamicStorable.h"

#include "StorableWrapper.ini"
#include "StorageWrapper.ini"

template class orm::v1::StorableWrapper< orm::v1::DynamicModel,
    orm::v1::DynamicStorable >;

namespace orm
{

  namespace v1
  {

    /// -----------------------------------------------------------------------
    /// StorageWrapper< DynamicModel, DynamicStorable, bool > specialisation
    /// -----------------------------------------------------------------------

    template<>
      StorageWrapper< orm::v1::DynamicModel, orm::v1::DynamicStorable, bool >::StorageWrapper()
      {
      }

    template<>
      StorageWrapper< orm::v1::DynamicModel, orm::v1::DynamicStorable, bool >::~StorageWrapper()
      {
      }

    template<>
      void
      StorageWrapper< orm::v1::DynamicModel, orm::v1::DynamicStorable, bool >::setup(//
      Model< orm::v1::DynamicModel >& model,
          QSqlDatabase& db)
      {
        typedef Storage< orm::v1::DynamicModel, orm::v1::DynamicStorable >
            NewStorageT;

        mModel = model;

        boost::shared_ptr< NewStorageT > storage(new NewStorageT(db,
            mManagedList));

        mStorage = storage;
      }

    /* --------------- */
    /* storable accessors */

    template<>
      const DynamicStorable::ListT&
      StorageWrapper< orm::v1::DynamicModel, orm::v1::DynamicStorable, bool >::list()
      {
        return mManagedList;
      }

    template<>
      const DynamicStorable::ListT&
      StorageWrapper< orm::v1::DynamicModel, orm::v1::DynamicStorable, bool >::list() const
      {
        return mManagedList;
      }

    template<>
      void
      StorageWrapper< orm::v1::DynamicModel, orm::v1::DynamicStorable, bool >::add(//
      boost::shared_ptr< orm::v1::DynamicStorable > obj,
          boost::weak_ptr< orm::v1::DynamicModel > model,
          boost::weak_ptr< Model< orm::v1::DynamicModel > > baseModel)
      {
        obj->setModel(model, baseModel);
        mManagedList.push_back(obj);
      }

    template<>
      void
      StorageWrapper< orm::v1::DynamicModel, orm::v1::DynamicStorable, bool >::remove(//
      boost::shared_ptr< orm::v1::DynamicStorable > obj)
      {
        _private_storable_wrapper::removeFromVector(mManagedList, obj);
      }

    template<>
      bool
      StorageWrapper< orm::v1::DynamicModel, orm::v1::DynamicStorable, bool >::contains(//
      const boost::shared_ptr< orm::v1::DynamicStorable >& obj) const
      {
        return std::find(mManagedList.begin(), mManagedList.end(), obj)
            != mManagedList.end();
      }

    /* --------------- */
    /* storage accessors */

    template<>
      void
      StorageWrapper< orm::v1::DynamicModel, orm::v1::DynamicStorable, bool >::loadCompletedEvent()
      {
      }

    template<>
      void
      StorageWrapper< orm::v1::DynamicModel, orm::v1::DynamicStorable, bool >::clear()
      {
        mAllList.clear();
        mManagedList.clear();
      }

    template<>
      std::string
      StorageWrapper< orm::v1::DynamicModel, orm::v1::DynamicStorable, bool >::storableTypeName()
      {
        return typeid(orm::v1::DynamicStorable).name();
      }

    /// -----------------------------------------------------------------------
    /// DynamicStorableConfig implementation
    /// -----------------------------------------------------------------------

    DynamicStorableConfig::DynamicStorableConfig(StorableConfigI* inheritsFrom) :
      mStorable(NULL), //
          mInheritsFrom(inheritsFrom),//
          mSqlOpts(new SqlOpts()),//
          mTemplateFields(new AttrPtrsT())
    {
    }
    /// -----------------------------------------------------------------------

    DynamicStorableConfig::~DynamicStorableConfig()
    {
    }
    /// -----------------------------------------------------------------------

    DynamicStorableConfig::PtrT
    DynamicStorableConfig::createCopy(StorableConfigI* inheritsFrom)
    {
      PtrT ptr(new DynamicStorableConfig(inheritsFrom));
      ptr->mSqlOpts = mSqlOpts;
      ptr->mTemplateFields = mTemplateFields;
      return ptr;
    }
    /// -----------------------------------------------------------------------

    void
    DynamicStorableConfig::InitialiseCopy(boost::shared_ptr< DynamicStorable > storable,
        PtrT& copy)
    {
      for (AttrPtrsT::iterator itr = mTemplateFields->begin();//
      itr != mTemplateFields->end(); ++itr)
      {
        AttrI::PtrT& srcAttr = *itr;
        AttrI::PtrT copyAttr = srcAttr->createCopy(*storable, srcAttr);
        copy->mFields.push_back(copyAttr);
      }
      copy->setup(*storable);
    }
    /// -----------------------------------------------------------------------

    void
    DynamicStorableConfig::setup(StorableI& storable)
    {
      mStorable = &storable;
    }
    /// -----------------------------------------------------------------------

    StorableState&
    DynamicStorableConfig::state()
    {
      return mState;
    }
    /// -----------------------------------------------------------------------

    StorableI&
    DynamicStorableConfig::storable()
    {
      return *mStorable;
    }
    /// -----------------------------------------------------------------------

    void
    DynamicStorableConfig::setUpdated()
    {
      mStorable->setUpdated(*this);
    }
    /// -----------------------------------------------------------------------

    StorableConfigI*
    DynamicStorableConfig::inheritsFrom()
    {
      return mInheritsFrom;
    }
    /// -----------------------------------------------------------------------

    bool
    DynamicStorableConfig::isValid()
    {
      for (AttrPtrsT::const_iterator itr = mFields.begin(); //
      itr != mFields.end(); ++itr)
      {
        const AttrI::PtrT attr = *itr;
        assert(attr.get());

        // configs with null primary keys are invalid
        if (attr->IsPrimaryKey() && attr->isNull())
          return false;

        // If it thinks its valid
        if (!attr->isValid())
          return false;
      }

      return true;
    }
    /// -------------------------------------------------------------------------

    bool
    DynamicStorableConfig::isPrimaryKeyValid()
    {
      for (AttrPtrsT::const_iterator itr = mFields.begin(); //
      itr != mFields.end(); ++itr)
      {
        const AttrI::PtrT attr = *itr;
        assert(attr.get());

        // configs with null primary keys are invalid
        if (attr->IsPrimaryKey() && attr->isNull())
          return false;
      }

      return true;
    }
    /// -------------------------------------------------------------------------

    std::string
    DynamicStorableConfig::invalidReason()
    {
      std::string reason;
      for (AttrPtrsT::const_iterator itr = mFields.begin(); //
      itr != mFields.end(); ++itr)
      {
        const AttrI::PtrT attr = *itr;
        assert(attr.get());

        // configs with null primary keys are invalid
        if (attr->IsPrimaryKey() && attr->isNull())
        {
          boost::format
              msg("%s is invalid in %s - It's a primary key and its null\n");
          msg % attr->FieldName();
          msg % sqlOpts()->name();
          reason += msg.str();
        }
        else if (!attr->isValid()) // If it thinks its valid
        {
          boost::format msg("%s is invalid in %s - %s\n");
          msg % attr->FieldName();
          msg % sqlOpts()->name();
          msg % attr->invalidReason();
          reason += msg.str();
        }
      }

      return reason;
    }
    /// -------------------------------------------------------------------------

    void
    DynamicStorableConfig::clearModel()
    {
      assert(mStorable);
      mStorable->clearModel();
    }
    /// -------------------------------------------------------------------------

    void
    DynamicStorableConfig::setup()
    {
      for (AttrPtrsT::const_iterator itr = mFields.begin(); //
      itr != mFields.end(); ++itr)
      {
        assert(itr->get());
        (*itr)->setup();
      }
    }
    /// -------------------------------------------------------------------------

    void
    DynamicStorableConfig::tearDown()
    {
      for (AttrPtrsT::const_iterator itr = mFields.begin(); //
      itr != mFields.end(); ++itr)
      {
        assert(itr->get());
        (*itr)->tearDown();
      }
    }
    /// -------------------------------------------------------------------------

    /// ---------------
    /// AttrA accessors

    const AttrsT&
    DynamicStorableConfig::fields()
    {
      return mFieldForReturn;
    }
    /// -----------------------------------------------------------------------

    void
    DynamicStorableConfig::addField(AttrI* field)
    {
      // The properties will call this function.
      // DynamicStorableConfig::InitiliseCopy function will assign ownership
      // of the property to this config via the
      // DynamicStorableConfig::AddField(AttrA::PtrT field) function.
      mFieldForReturn.push_back(field);
    }
    /// -----------------------------------------------------------------------

    void
    DynamicStorableConfig::addBackRef(BackRefI* backRef)
    {
    }
    /// -----------------------------------------------------------------------

    const BackRefsT&
    DynamicStorableConfig::backRefs() const
    {
      return mBackRefForReturn;
    }
    /// -----------------------------------------------------------------------

    /// ---------------
    /// AttrMeta accessors

    const AttrPtrsT&
    DynamicStorableConfig::fieldMetas() const
    {
      return *mTemplateFields;
    }
    /// -----------------------------------------------------------------------

    void
    DynamicStorableConfig::addFieldMeta(AttrI::PtrT field)
    {
      assert(mTemplateFields.get() != NULL);
      assert(field.get());
      mTemplateFields->push_back(field);
    }
    /// -----------------------------------------------------------------------

    /// ---------------
    /// SqlOpt accessors

    SqlOpts::PtrT
    DynamicStorableConfig::sqlOpts()
    {
      assert(mSqlOpts.get());
      return mSqlOpts;
    }
    /// -----------------------------------------------------------------------

    const SqlOpts::PtrT&
    DynamicStorableConfig::sqlOpts() const
    {
      assert(mSqlOpts.get());
      return mSqlOpts;
    }
    /// -----------------------------------------------------------------------

    /// -----------------------------------------------------------------------
    /// DynamicStorable implementation
    /// -----------------------------------------------------------------------

    DynamicStorable::DynamicStorable()
    {
    }
    /// -----------------------------------------------------------------------

    DynamicStorable::~DynamicStorable()
    {
    }
    /// -----------------------------------------------------------------------

    void
    DynamicStorable::addToModel(boost::shared_ptr< DynamicModel > model)
    {
      throw std::runtime_error("You can not add a DynamicStorable to the model"
        "with DynamicStorable::addToModel.\n"
        "Use DynamicModel::add() instead");
    }
    /// -----------------------------------------------------------------------

    void
    DynamicStorable::removeFromModel()
    {
      boost::shared_ptr< DynamicModel > model_ptr = model().lock();
      if (model_ptr.get() == NULL)
      {
        throw std::runtime_error("Can not remove from mode.\n"
          "failed to lock model pointer");
      }
      model_ptr->remove(shared_from_this());
      disableUpdates();
    }
    /// -----------------------------------------------------------------------

    bool
    DynamicStorable::isInModel()
    {
      boost::shared_ptr< DynamicModel > model_ptr = model().lock();
      if (model_ptr.get() == NULL)
        return false;

      return model_ptr->contains(shared_from_this());
    }
    /// -----------------------------------------------------------------------

    void
    DynamicStorable::setup()
    {
      DynamicModel::PtrT model_ = model().lock();
      assert(model_.get() != NULL);

      // This will copy over our configs, etc.
      // Normally they are put in the constructor of a storable.
      model_->setupDynamicStorable(shared_from_this(), mConfigs);

      // Call the base class setup, let it do what its going to do
      StorableA< DynamicModel >::setup();
    }
    /// -----------------------------------------------------------------------

    StorableConfigI*
    DynamicStorable::config()
    {
      return mConfigs.empty() ? NULL : mConfigs.back().get();
    }
    /// -----------------------------------------------------------------------

    std::string
    DynamicStorable::typeName() const
    {
      return typeid(DynamicStorable).name();
    }
    /// -----------------------------------------------------------------------

    boost::shared_ptr< DynamicStorable >
    DynamicStorable::shared_from_this()
    {
      PtrT
          ptr(boost::enable_shared_from_this< StorableA< DynamicModel > >::shared_from_this(),
              boost::detail::polymorphic_cast_tag());
      return ptr;
    }
    /// -----------------------------------------------------------------------

    boost::shared_ptr< DynamicStorable >
    DynamicStorable::downcast(boost::shared_ptr< StorableI > baseClass,
        bool warnForNull)
    {
      if (baseClass.get() == NULL)
      {
        if (warnForNull)
        {
          qWarning("Downcasting null pointer to DynamicStorable, returning null pointer");
        }
        return PtrT();
      }

      try
      {
        return PtrT(baseClass, boost::detail::polymorphic_cast_tag());
      }
      catch (std::bad_cast& e)
      {
        assert(baseClass->config());
        assert(baseClass->config()->sqlOpts().get());

        boost::format info("Unable to downcast from storable of type %s"
          " to storable of type DynamicStorable.");
        info % baseClass->config()->sqlOpts()->name();

        throw InvalidDowncastException(info.str());
      }
    }
    /// -------------------------------------------------------------------------

    /// -----------------------------------------------------------------------
    /// DynamicModel implementation
    /// -----------------------------------------------------------------------

    DynamicModel::DynamicModel(const Conn& ormConn) :
      BaseModel(ormConn)
    {
      addStorageWrapper(mStorageWrapper);
    }
    /// -----------------------------------------------------------------------

    void
    DynamicModel::setupDynamicStorable(boost::shared_ptr< DynamicStorable > storable,
        std::vector< DynamicStorableConfig::PtrT >& configs)
    {
      for (DynamicStorableConfig::ListT::iterator cfgItr = mConfigs.begin();//
      cfgItr != mConfigs.end(); ++cfgItr)
      {
        assert(storable.get());
        StorableConfigI* inheritsFrom = storable->config();

        DynamicStorableConfig::PtrT& src = *cfgItr;
        DynamicStorableConfig::PtrT copy = src->createCopy(inheritsFrom);
        configs.push_back(copy);
        src->InitialiseCopy(storable, copy);
      }
    }
    /// -----------------------------------------------------------------------


    DynamicStorableConfig::PtrT
    DynamicModel::addInherited()
    {
      StorableConfigI* inheritsFrom = (mConfigs.empty() ? NULL
          : mConfigs.back().get());

      DynamicStorableConfig::PtrT ptr;
      ptr.reset(new DynamicStorableConfig(inheritsFrom));
      mConfigs.push_back(ptr);
      return ptr;
    }
    /// -----------------------------------------------------------------------

    const DynamicModel::DynamicStorableListT&
    DynamicModel::storables()
    {
      return mStorageWrapper.list();
    }
    /// -----------------------------------------------------------------------

    const DynamicModel::DynamicStorableListT&
    DynamicModel::storables() const
    {
      return mStorageWrapper.list();
    }
    /// -----------------------------------------------------------------------

    DynamicStorable::PtrT
    DynamicModel::add()
    {
      DynamicStorable::PtrT newStorable(new DynamicStorable());
      mStorageWrapper.add(newStorable, shared_from_this(), shared_from_this());
      return newStorable;
    }
    /// -----------------------------------------------------------------------

    void
    DynamicModel::remove(DynamicStorablePtrT obj)
    {
      mStorageWrapper.remove(obj);
    }
    /// -----------------------------------------------------------------------

    bool
    DynamicModel::contains(const DynamicStorablePtrT obj) const
    {
      return mStorageWrapper.contains(obj);
    }
  /// -----------------------------------------------------------------------

  }

}
