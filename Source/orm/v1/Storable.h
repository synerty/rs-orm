/*
 * Storable.h
 *
 *  Created on: Dec 17, 2010
 *      Author: Jarrod Chesney
 */

#ifndef _ORM_V1_STORABLE_H_
#define _ORM_V1_STORABLE_H_

#if defined(BUILD_ORM_V1_DLL)
#define DECL_ORM_V1_DIR __declspec(dllexport)
#elif defined(USE_ORM_V1_DLL)
#define DECL_ORM_V1_DIR __declspec(dllimport)
#else
#define DECL_ORM_V1_DIR
#endif

#include <string>
#include <vector>
#include <auto_ptr.h>
#include <boost/noncopyable.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/weak_ptr.hpp>
#include <boost/format.hpp>

#include "Attribute.h"
#include "SqlOpts.h"
#include "StorableConfig.h"
#include "StorableState.h"
#include "StorableChange.h"
#include "Exceptions.h"
#include "Model.h"
#include "StorableWrapper.h"
#include "StorageWrapper.h"

namespace orm
{
  namespace v1
  {

    template<typename ModelT>
      class Model;

    /*!  Storable
     * A storable object is a data structure that can be moved to/from a database
     * easily.
     */
    template<typename ModelT>
      class StorableA : public virtual StorableI,
          public boost::enable_shared_from_this< StorableA< ModelT > >,
          boost::noncopyable
      {
        public:
          StorableA();
          virtual
          ~StorableA();

        public:
          /// --------------
          /// Actual public interface

          virtual void
          addToModel(boost::shared_ptr< ModelT > model) = 0;

          virtual void
          removeFromModel() = 0;

          virtual bool
          isInModel() = 0;

        public:
          /// --------------
          // Flags

          virtual StorableConfigI*
          config();

          void
          setUpdated(StorableConfigI& config);

        public:
          /// ---------------
          /// Methods setting up for interation with other storables

          virtual void
          setModel(boost::weak_ptr< ModelT > model,
              boost::weak_ptr< Model< ModelT > > baseModel);

          boost::weak_ptr< ModelT >
          model();

          boost::weak_ptr< Model< ModelT > >
          baseModel();

          void
          clearModel();

          virtual bool
          isValid();

          virtual bool
          isPrimaryKeyValid();

          virtual std::string
          invalidReason();

          virtual bool
          initialiseModel(const int pass);

        protected:
          virtual void
          setup();

          virtual void
          tearDown();

          void
          disableUpdates();

          /*! Is Derrived From
           * This flag indicates that this paticular instance of this class is
           * derrived from by another class.
           * This flag should be set from the constructor of the deriving
           *  storable.
           *  Not used by DynamicStorable
           */
          bool mIsDerrivedFrom;

          /*! Is Updates Enabled
           * Flag for blocking updates if the model is being set or is unset.
           */
          bool mIsUpdatesEnabled;

          /*! Is Initialised
           * Flag for indicating that the storable has been initialised.
           */
          bool mIsInitialised;

        protected:

          boost::weak_ptr< ModelT > mModel;
          boost::weak_ptr< Model< ModelT > > mBaseModel;
      };

    /*!  Storable
     * A storable object is a data structure that can be moved to/from a database
     * easily.
     */
    template<class ModelT_, class StorableT, class InheritsFromT = StorableA<
        ModelT_ > >
      class DECL_ORM_V1_DIR Storable : public InheritsFromT
      {
        public:
          typedef InheritsFromT BaseStorableT;
          typedef ModelT_ ModelT;
          typedef StorableT ThisStorableT;
          typedef typename boost::shared_ptr< StorableT > PtrT;
          typedef typename std::vector< PtrT > ListT;

        public:
          Storable();
          virtual
          ~Storable();

        public:
          /// --------------
          /// Actual public interface

          virtual
          void
          addToModel(boost::shared_ptr< ModelT > model);

          virtual
          void
          removeFromModel();

          virtual
          bool
          isInModel();

        public:
          /// --------------
          // Config

          void
          setModel(boost::weak_ptr< ModelT > model_,
              boost::weak_ptr< Model< ModelT > > baseModel);

          StorableConfigI*
          config();

          virtual std::string
          typeName() const;

          boost::shared_ptr< StorableT >
          shared_from_this();

          static PtrT
          downcast(boost::shared_ptr< StorableI > baseClass,
              bool warnForNull = true);

          template<class DerrivedStorableT>
            static ListT
            downcast(std::vector< boost::shared_ptr< DerrivedStorableT > > baseClasses,
                bool warnForNull = true);

        protected:
          /*! Is Derrived From
           * This flag indicates that this paticular instance of this class is
           * derrived from by another class.
           * This flag should be set from the constructor of the deriving
           * storable.
           */
          bool mIsDerrivedFrom;

          typedef void
          (ModelT::*ModelAddFuncT)(PtrT);
          typedef void
          (ModelT::*ModelRemoveFuncT)(PtrT);

          /*! Add Derived To Model Function
           * This is a pointer to a member function of the model, the function
           * should add this class to its list of objects of this type. The special
           * addDerrived... function will only add this list to its list of object
           * but not add it to its list of objects to save/load for that object type.
           */
          static ModelAddFuncT sModelAddDerrivedFunction;

          /*! Remove From Model Function
           * This is a pointer to a member function of the model. The function should
           * removed this object from the models list of objects of this type.
           */
          static ModelAddFuncT sModelRemoveFunction;

        private:
          StorableConfig< StorableT > mConfig;

      };

#define ORM_V1_CALL_STORABLE_SETUP(_class_name_) \
    static bool __ ## _class_name_ ## _setupStaticInit \
      __attribute__((unused)) = _class_name_::setupStatic();
  }
}

#ifndef USE_ORM_V1_DLL
#include "Storable.ini"
#endif

#endif /* _ORM_V1_STORABLE_H_ */
