/*
 * StorableConfigI.h
 *
 *  Copyright NG SCADA Systems Pty Ltd 2011
 *
 *  This software is proprietary, you are not free to copy
 *  or redistribute this code in any format.
 *
 *  All rights to this software are reserved by 
 *  NG SCADA Systems Pty Ltd
 */

#ifndef _ORM_V1_STORABLE_CONFIG_I_H_
#define _ORM_V1_STORABLE_CONFIG_I_H_

#include <string>

#include <boost/shared_ptr.hpp>

#include "StorableI.h"
#include "Attribute.h"

namespace orm
{

  namespace v1
  {
    class StorableState;
    class StorableI;
    class SqlOpts;

    class StorableConfigI
    {
      public:
        typedef boost::shared_ptr< StorableConfigI > PtrT;

      public:

        virtual void
        setup(StorableI& storable) = 0;

        virtual StorableState&
        state() = 0;

        virtual StorableI&
        storable() = 0;

        virtual void
        setUpdated() = 0;

        virtual StorableConfigI*
        inheritsFrom() = 0;

        virtual bool
        isValid() = 0;

        virtual bool
        isPrimaryKeyValid() = 0;

        virtual std::string
        invalidReason() = 0;

        virtual void
        clearModel() = 0;

        virtual void
        setup() = 0;

        virtual void
        tearDown() = 0;

      public:
        /// --------------
        // Setup
        /*! Add Attribute
         * Adds a field to this storable object.
         * @param field the attribute to use as the field
         * Ownership of field is not transferred to this class
         */
        virtual void
        addField(AttrI* field) = 0;

        virtual const AttrsT&
        fields() = 0;

        /*! Add Property Back Reference
         * Adds a property back reference to this storable object.
         * @param backRef the back ref
         * Ownership of back ref is not transferred to this class
         */
        virtual void
        addBackRef(BackRefI* backRef) = 0;

        virtual const BackRefsT&
        backRefs() const = 0;

      public:
        /// ---------------
        /// Instance accesors for static data

        virtual boost::shared_ptr< SqlOpts >
        sqlOpts() = 0;

        virtual const boost::shared_ptr< SqlOpts >&
        sqlOpts() const = 0;

        virtual const AttrPtrsT&
        fieldMetas() const = 0;
    };
  }
}

#endif /* _ORM_V1_STORABLE_CONFIG_I_H_ */
