/*
 * Condition.h
 *
 *  Created on: Feb 3, 2011
 *      Author: Jarrod Chesney
 */

#ifndef _ORM_V1_CONDITION_H_
#define _ORM_V1_CONDITION_H_

namespace orm
{
  namespace v1
  {
    class ConditionI
    {
      public:
        virtual std::string
        Compile() = 0;
    };

    class Condition
    {
      public:
        Condition();
      public:

        std::string
        Compile();

      public:
        Condition operator==(const int val);
        Condition operator like(const int val);
    };
  }
}

#endif /* _ORM_V1_CONDITION_H_ */
