/*
 * MvLink.h
 *
 *  Created on: Feb 7, 2011
 *      Author: Jarrod Chesney
 */

#ifndef _ORM_V1_MVLINK_STANDARD_VALUE_H_
#define _ORM_V1_MVLINK_STANDARD_VALUE_H_

#include "Attribute.h"

#include <string.h>
#include <stdint.h>

#include <boost/variant.hpp>

#include "MvLink.h"

/*! orm::v1::MvLink Brief Description
 * Long comment about the class
 *
 */
namespace orm
{

  namespace v1
  {

    struct MvLinkStandardValueVisitor : boost::static_visitor< >
    {
        enum ValueTypeE
        {
          Bool,
          Float,
          Double,
          Int16,
          Int32,
          Int64,
          String,
          Storable,
          InvalidValue
        };

        ValueTypeE ValueType;
        QVariant Data;

        MvLinkI::StandardValueT
        fromQVariant(const QVariant& qVar);

        void
        operator()(MvLinkI::InvalidT& val);

        void
        operator()(bool& val);

        void
        operator()(float& val);

        void
        operator()(double& val);

        void
        operator()(int16_t& val);

        void
        operator()(int32_t& val);

        void
        operator()(int64_t& val);

        void
        operator()(std::string& val);

        void
        operator()(boost::shared_ptr< StorableI >& val);

    };

  }

}

#endif /* _ORM_V1_MVLINK_STANDARD_VALUE_H_ */
