/*
 * Sequance.cpp
 *
 *  Created on: Feb 4, 2011
 *      Author: Jarrod Chesney
 */

#include "Sequance.h"

#include <assert.h>

#include <QtCore/qvariant.h>
#include <QtSql/qsqldatabase.h>
#include <QtSql/qsqlquery.h>
#include <QtSql/qsqlerror.h>

namespace orm
{

  namespace v1
  {

    Sequance::Sequance(const std::string& name, QSqlDatabase& db) :
      mName(name), mDb(db)
    {
    }

    Sequance::~Sequance()
    {
    }

    uint64_t
    Sequance::next()
    {
      const std::string lSql = "SELECT nextval('" + mName + "');";

      QSqlQuery qry(mDb);
      qry.prepare(lSql.c_str());
      qry.setForwardOnly(true);

      const bool lSuccess = qry.exec();

      if (!lSuccess || !qry.next())
      {
        std::string lMsg = //
        "An error occurred retrieving data from sequance" + mName + "\n" //
            + qry.lastError().text().toStdString() + "\n"//
            + qry.executedQuery().toStdString();
        qCritical(lMsg.c_str());
        return false;
      }

      bool ok = false;
      uint64_t val = qry.value(0).toULongLong(&ok);
      assert(ok);
      return val;

    }
  }

}
