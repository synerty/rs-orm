/*
 * AttributeI.h
 *
 *  Created on: Dec 19, 2010
 *      Author: Jarrod Chesney
 */

#ifndef _ORM_ATTRIBUTE_H_
#define _ORM_ATTRIBUTE_H_

#include <string>
#include <vector>

#include <boost/shared_ptr.hpp>
#include <boost/weak_ptr.hpp>
#include <boost/optional.hpp>

class QSqlQuery;
namespace orm
{
  namespace v1
  {
    template<typename ModelT>
      class Model;

    class AttrI;

    class MvLinkI;
    class DynamicStorable;
    class DynamicModel;
    class StorableI;
    class StorableConfigI;

    class AttrMetaI
    {
      public:
        virtual const std::string&
        FieldName() const = 0;

        virtual bool
        IsPrimaryKey() const = 0;

        virtual bool
        IsValidIfNull() const = 0;
    };

    /*!  Attribute Interface
     * An  Attribute is an attribute of a data structure that can bind
     * and get to/from a qsql query.
     *
     */
    class AttrI : public virtual AttrMetaI
    {
      public:
        typedef boost::shared_ptr< AttrI > PtrT;
        typedef boost::weak_ptr< AttrI > WptrT;

      public:
        /// ---------------
        /// Functions to interact with the database

        virtual bool
        Bind(QSqlQuery& qry, const int& bindIndex) = 0;

        virtual bool
        Get(QSqlQuery& qry, int fieldIndex) = 0;

        /*! Initialise Model
         * Call this function once the plain data has been loaded from the
         * database. The job
         * @param pass Initising the model may take several incrimental steps,
         * this method can be called for several passes.
         * @return
         */
        virtual bool
        initialiseModel(const int pass) = 0;

        /*! Setup
         * This function is called when a storable is added to the model.
         * It will setup any model dependent initialisation.
         */
        virtual void
        setup() = 0;

        /*! Tear Down
         * This function is called when a storable is removed from a model.
         * It will cleanup any references to the model that have been stored
         * such as the lookup binds in RelatedModel
         */
        virtual void
        tearDown() = 0;

        /*! Create Copy
         * NOTE : Used ONLY for the Dynamic Model functionality
         */
        virtual PtrT
        createCopy(DynamicStorable& storable, AttrI::WptrT attrMeta) = 0;

        /*! Is Value Valid
         * Indicates that the value for this attribute is valid.
         * If the value is invalid then the storable will be erased from
         * the model and database.
         * @return true if value is valid
         */
        virtual bool
        isValid() const = 0;

        virtual std::string
        invalidReason() const = 0;

        /*! Is Null
         * This is used mainly for primary keys, if its null then we omit the
         * data from the database.
         * @return value is null
         */
        virtual bool
        isNull() const = 0;

        virtual void
        setNull() = 0;

        virtual void
        setDefault() = 0;

        /*! Storable Config
         * Get the storable config that this attrbute is apart of.
         */
        virtual StorableConfigI&
        storableConfig() = 0;

      public:
        /// ---------------
        /// Functions for interaction with a view

        virtual boost::shared_ptr< MvLinkI >
        mvLink() = 0;
    };

    template<typename TypeT>
      class PropertyI
      {
        public:
          virtual const TypeT&
          get() const = 0;

          virtual void
          set(const TypeT& value) = 0;

          virtual bool
          isNull() const = 0;

          virtual void
          setNull() = 0;

          virtual void
          setDefault() = 0;

          virtual boost::optional<TypeT>
          defaultValue() const = 0;
      };

    class BackRefI
    {
      public:
        virtual void
        get(std::vector< boost::shared_ptr< StorableI > >& storable) const = 0;
    };

    // Not a shared_ptr because the attrs are created on the stack
    typedef std::vector< AttrI* > AttrsT;
    typedef std::vector< AttrI::PtrT > AttrPtrsT;
    typedef std::vector< BackRefI* > BackRefsT;

  }
}

#endif /* ORMATTRIBUTEI_H_ */
