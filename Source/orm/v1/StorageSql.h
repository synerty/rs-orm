/*
 * StorageSql.h
 *
 *  Created on: Dec 17, 2010
 *      Author: Jarrod Chesney
 */

#ifndef _ORM_V1_STORAGE_SQL_H_
#define _ORM_V1_STORAGE_SQL_H_

#include <assert.h>
#include <vector>

#include <boost/shared_ptr.hpp>

#include <QtSql/qsqldatabase.h>
#include <QtSql/qsqldriver.h>

#include "StorableConfig.h"
#include "Exceptions.h"

namespace orm
{
  namespace v1
  {

    /*!  Storage Sql
     * Generates the SQL for interaction with the database
     *
     */
    class StorageSql
    {

      public:
        StorageSql(QSqlDatabase& db);

        virtual
        ~StorageSql();

      public:

        void
        setConfig(StorableConfigI* config);

        std::string
        sqlSelect() const;

        std::string
        sqlInsert(StorableConfigI& config) const;

        std::string
        sqlUpdate(StorableConfigI& config, int& pkStartBindIndex) const;

        std::string
        sqlDelete(StorableConfigI& config) const;

      private:
        /*! Sql Select
         * Construct an sql select expression
         * @return The constructed sql select clause
         */
        std::string
        sqlSelectFields() const;

        std::string
        sqlSelectFrom() const;

        /*! Sql Where - Condition
         * Generates the where clause of an SQL expression based on the
         * condition objects
         * @return WHERE sql clause
         */
        std::string
        sqlWhereCondition() const;

        /*! Sql Where - Primary Key
         * Generates the where clause of an SQL expression based on the
         * primary key objects
         * @return WHERE sql clause
         */
        std::string
        sqlWherePrimaryKey(StorableConfigI& config) const;

        std::string
        sqlOrderBy() const;

      private:
        /*! Field Quote
         * Returns the quote used by the database for quoting field and table
         * names
         * @return quote
         */
        std::string
        fQ(const std::string prmFieldName) const;

        std::string as()const;

        std::string only()const;

        /*! Table or Query SQL
         * @return The TableName or Query SQL in that order
         */
        std::string
        tableOrQuerySql(const SqlOpts& sqlOpt) const;

        void
        sqlPrimaryKeyFieldNames(StorableConfigI& config,
            std::vector< std::string >& names) const ;

      private:

        QSqlDatabase& mDb;

        StorableConfigI* mConfig;
    };
  }
}

#endif /* _ORM_V1_STORAGE_SQL_H_ */
