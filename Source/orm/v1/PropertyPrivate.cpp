/*
 * PropertyPrivate.cpp
 *
 *  Created on: Feb 7, 2011
 *      Author: Jarrod Chesney
 */

#include "PropertyPrivate.h"

#include <QtSql/qsqlrecord.h>

namespace orm
{

  namespace v1
  {

    namespace __property_private
    {

      /// -----------------------------------
      /// String specialisation

      template<>
        void
        BindGetDelegate< std::string >::InitialiseValue(std::string& propValue)
        {
        }
      /// ---------------------------------------------------------------------------

      template<>
        bool
        BindGetDelegate< std::string >::Bind(const std::string& propValue,
            QSqlQuery& qry,
            const int& bindIndex)
        {
          if (propValue.empty())
            qry.bindValue(bindIndex, QVariant());
          else
            qry.bindValue(bindIndex, propValue.c_str());

          return true;
        }
      /// ---------------------------------------------------------------------------

      template<>
        bool
        BindGetDelegate< std::string >::Get(std::string& propValue,
            QSqlQuery& qry,
            int fieldIndex,
            QVariant& qryValue)
        {
          propValue = qryValue.toString().toStdString();
          return true;
        }
      /// ---------------------------------------------------------------------------

      /// -----------------------------------
      /// int specialisation

      template<>
        void
        BindGetDelegate< int >::InitialiseValue(int& propValue)
        {
          propValue = 0;
        }
      /// ---------------------------------------------------------------------------

      template<>
        bool
        BindGetDelegate< int >::Get(int& propValue,
            QSqlQuery& qry,
            int fieldIndex,
            QVariant& qryValue)
        {
          bool ok = false;
          propValue = qryValue.toInt(&ok);

          if (!ok)
            qCritical("Unable to convert value \"%s\" to int for field %s",
                qPrintable(qryValue.toString()),
                qPrintable(qry.record().fieldName(fieldIndex)));

          return ok;
        }
      /// ---------------------------------------------------------------------------

      /// -----------------------------------
      /// int16_t specialisation

      template<>
        void
        BindGetDelegate< int16_t >::InitialiseValue(int16_t& propValue)
        {
          propValue = 0;
        }
      /// ---------------------------------------------------------------------------

      template<>
        bool
        BindGetDelegate< int16_t >::Get(int16_t& propValue,
            QSqlQuery& qry,
            int fieldIndex,
            QVariant& qryValue)
        {
          bool ok = false;
          propValue = qryValue.toInt(&ok);

          if (!ok)
            qCritical("Unable to convert value \"%s\" to unsigned int for field %s",
                qPrintable(qryValue.toString()),
                qPrintable(qry.record().fieldName(fieldIndex)));

          return ok;
        }
      /// ---------------------------------------------------------------------------

      /// -----------------------------------
      /// unsigned int specialisation

      template<>
        void
        BindGetDelegate< unsigned int >::InitialiseValue(unsigned int& propValue)
        {
          propValue = 0;
        }
      /// ---------------------------------------------------------------------------

      template<>
        bool
        BindGetDelegate< unsigned int >::Get(unsigned int& propValue,
            QSqlQuery& qry,
            int fieldIndex,
            QVariant& qryValue)
        {
          bool ok = false;
          propValue = qryValue.toUInt(&ok);

          if (!ok)
            qCritical("Unable to convert value \"%s\" to unsigned int for field %s",
                qPrintable(qryValue.toString()),
                qPrintable(qry.record().fieldName(fieldIndex)));

          return ok;
        }
      /// ---------------------------------------------------------------------------

      /// -----------------------------------
      /// 64bit unsigned int specialisation

      template<>
        void
        BindGetDelegate< uint64_t >::InitialiseValue(uint64_t& propValue)
        {
          propValue = 0;
        }
      /// ---------------------------------------------------------------------------

      template<>
        bool
        BindGetDelegate< uint64_t >::Get(uint64_t& propValue,
            QSqlQuery& qry,
            int fieldIndex,
            QVariant& qryValue)
        {
          bool ok = false;
          propValue = qryValue.toULongLong(&ok);

          if (!ok)
            qCritical("Unable to convert value \"%s\" to unsigned int for field %s",
                qPrintable(qryValue.toString()),
                qPrintable(qry.record().fieldName(fieldIndex)));

          return ok;
        }
      /// ---------------------------------------------------------------------------

      /// -----------------------------------
      /// 64bit int specialisation

      template<>
        void
        BindGetDelegate< int64_t >::InitialiseValue(int64_t& propValue)
        {
          propValue = 0;
        }
      /// ---------------------------------------------------------------------------

      template<>
        bool
        BindGetDelegate< int64_t >::Get(int64_t& propValue,
            QSqlQuery& qry,
            int fieldIndex,
            QVariant& qryValue)
        {
          bool ok = false;
          propValue = qryValue.toLongLong(&ok);

          if (!ok)
            qCritical("Unable to convert value \"%s\" to unsigned int for field %s",
                qPrintable(qryValue.toString()),
                qPrintable(qry.record().fieldName(fieldIndex)));

          return ok;
        }
      /// ---------------------------------------------------------------------------

      /// -----------------------------------
      /// bool specialisation

      template<>
        void
        BindGetDelegate< bool >::InitialiseValue(bool& propValue)
        {
          propValue = false;
        }
      /// ---------------------------------------------------------------------------

      template<>
        bool
        BindGetDelegate< bool >::Get(bool& propValue,
            QSqlQuery& qry,
            int fieldIndex,
            QVariant& qryValue)
        {
          bool ok = false;
          propValue = !!qryValue.toInt(&ok);
          return ok;
        }
      /// ---------------------------------------------------------------------------

      /// -----------------------------------
      /// double specialisation

      template<>
        void
        BindGetDelegate< double >::InitialiseValue(double& propValue)
        {
          propValue = 0.0f;
        }
      /// ---------------------------------------------------------------------------

      template<>
        bool
        BindGetDelegate< double >::Get(double& propValue,
            QSqlQuery& qry,
            int fieldIndex,
            QVariant& qryValue)
        {
          bool ok = false;
          propValue = qryValue.toDouble(&ok);

          if (!ok)
            qCritical("Unable to convert value \"%s\" to double for field %s",
                qPrintable(qryValue.toString()),
                qPrintable(qry.record().fieldName(fieldIndex)));

          return ok;
        }
      /// ---------------------------------------------------------------------------

      /// -----------------------------------
      /// float specialisation

      template<>
        void
        BindGetDelegate< float >::InitialiseValue(float& propValue)
        {
          propValue = 0.0f;
        }
      /// ---------------------------------------------------------------------------

      template<>
        bool
        BindGetDelegate< float >::Get(float& propValue,
            QSqlQuery& qry,
            int fieldIndex,
            QVariant& qryValue)
        {
          bool ok = false;
          propValue = qryValue.toFloat(&ok);

          if (!ok)
            qCritical("Unable to convert value \"%s\" to float for field %s",
                qPrintable(qryValue.toString()),
                qPrintable(qry.record().fieldName(fieldIndex)));

          return ok;
        }
    /// ---------------------------------------------------------------------------

    }

  }

}
