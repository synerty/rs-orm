/*
 * Storage.h
 *
 *  Created on: Dec 17, 2010
 *      Author: Jarrod Chesney
 */

#ifndef ORMSTORAGE_H_
#define ORMSTORAGE_H_

#include <assert.h>
#include <vector>
#include <deque>
#include <set>

#include <boost/shared_ptr.hpp>
#include <boost/format.hpp>

#include <QtCore/qvariant.h>
#include <QtSql/qsqlquery.h>
#include <QtSql/qsqldatabase.h>
#include <QtSql/qsqldriver.h>
#include <QtSql/qsqlerror.h>

#include "ObservableProgress.h"

#include "Storable.h"
#include "StorageI.h"
#include "Attribute.h"
#include "StorageSql.h"
#include "StorableChange.h"
#include "StorableWrapper.h"

namespace orm
{
  namespace v1
  {

    template<typename ModelT>
      class Model;



    /*!  Storage
     * Does the storing and retrieving
     *
     */
    template<class ModelT, class StorableT>
      class Storage : public ObservableProgress, public StorageI< ModelT >
      {

        public:
          // Types
          typedef typename std::vector< boost::shared_ptr< StorableT > >
              StorableListT;
          typedef typename std::set< boost::shared_ptr< StorableT > >
              StorableSetT;

          typedef typename std::deque< StorableConfigI* > StorableConfigListT;
          typedef typename std::set< StorableConfigI* > StorableConfigSetT;

        public:
          Storage(QSqlDatabase& db, StorableListT& data);

          virtual
          ~Storage();

        public:

          void
          setModel(boost::weak_ptr< ModelT > model,
              boost::weak_ptr< Model < ModelT > > baseModel);

          bool
          load();

          void
          save(StorableChange& change);

          bool
          forceSave();

          bool
          isSaveRequired() const;

          /*! Save Complete
           * Lets the storable know that the save operation has completed.
           */
          void
          saveComplete();

          bool
          initialiseModel(const int pass);

          const std::string
          name() const;

        public:

          void
          setDebug(const bool prmDebug);

          bool
          IsDebug() const;

        private:
          bool
          Update(StorableConfigListT& list);

          bool
          Insert(StorableConfigListT& list);

          template<typename IteratorT>
            bool
            Delete(IteratorT begin, const IteratorT& end);

          void
          prepareQryMaps();

          template<typename IteratorT>
            void
            UnlockList(IteratorT begin, const IteratorT& end) const;

          void
          SetAsStored(StorableConfigListT& list) const;

          template<typename IteratorT>
            void
            SetAsDeleted(IteratorT begin, const IteratorT& end) const;

        private:
          bool
          BindFields(const AttrsT& attrs, QSqlQuery& qry);

          bool
          DbToObject(const AttrsT& attrs, QSqlQuery& qry, int& fieldIndex);

          bool
          BindConditions(QSqlQuery& qry);

          bool
          BindPrimaryKeys(const AttrsT& attrs,
              QSqlQuery& qry,
              int startBindIndex);

        private:

          boost::weak_ptr< ModelT > mModel;
          boost::weak_ptr< Model < ModelT > > mBaseModel;

          QSqlDatabase& mDb;

          bool mDebug;

          StorableListT& mData;

          StorableConfigSetT mDataFromDb;

          StorableWrapper< ModelT, StorableT > mStorableWrapper;
          StorableConfigI* mStorableConfig;
          StorageSql mSqlGenerator;

        private:
          /// ---------------
          /// Query map stuff

          struct QryMapData
          {
              boost::shared_ptr< QSqlQuery > qry;
              int pkStartBindIndex;
              QryMapData() :
                pkStartBindIndex(0)
              {
              }
          };

          typedef std::map< SqlOpts*, QryMapData > QryMapT;

          QryMapT mInsertQryMap;
          QryMapT mUpdateQryMap;
          QryMapT mDeleteQryMap;
          bool mQryMapsPrepared;
      };
  }
}


#ifndef USE_ORM_V1_DLL
#include "Storage.ini"
#endif

#endif /* ORMSTORAGE_H_ */
