/*
 * RelatedPropertyPrivate.cpp
 *
 *  Created on: Feb 4, 2011
 *      Author: Jarrod Chesney
 */

#include "RelatedPropertyPrivate.h"

namespace orm
{

  namespace v1
  {

    namespace __related_property_private
    {

      /// ---------------------------------------------------------------------
      /// RelatedPropertyRelatedToManyVisitor class
      /// ---------------------------------------------------------------------

      template<typename ModelT, typename StorableT, typename RelatedStorableT>
        RelatedToManyVisitor< ModelT, StorableT, RelatedStorableT >::RelatedToManyVisitor(boost::shared_ptr<
            RelatedStorableT >& relatedObject,
            VisitFunctionT& visitFunc) :
          relatedToManyProperty(NULL)
        {
          assert(relatedObject.get());
          ((*relatedObject).*visitFunc)(*this);
        }
      /// ---------------------------------------------------------------------

      template<typename ModelT, typename StorableT, typename RelatedStorableT>
        void
        RelatedToManyVisitor< ModelT, StorableT, RelatedStorableT >::operator()(ToManyPropertyT& fromManyProperty)
        {
          relatedToManyProperty = &fromManyProperty;
        }
      /// ---------------------------------------------------------------------

      /// ---------------------------------------------------------------------
      /// CopyDelegate class
      /// ---------------------------------------------------------------------

      template<typename ModelT, typename StorableT, typename UnusedUniqueT,
          typename RelatedStorableT, typename DbValT>
        CopyDelegate< ModelT, StorableT, UnusedUniqueT, RelatedStorableT,
            DbValT >::CopyDelegate(RelatedProperty< ModelT, StorableT,
            UnusedUniqueT, RelatedStorableT, DbValT >& srcProp_)
        {
        }
      /// ---------------------------------------------------------------------

      template<typename ModelT, typename StorableT, typename UnusedUniqueT,
          typename RelatedStorableT, typename DbValT>
        AttrI::PtrT
        CopyDelegate< ModelT, StorableT, UnusedUniqueT, RelatedStorableT,
            DbValT >::operator()(DynamicStorable& dstStorable,
            AttrI::WptrT attrMeta)
        {
          // Only dynamic objects are copyable
          assert(false);
          return AttrI::PtrT();
        }
      /// ---------------------------------------------------------------------

      /// ---------------------------------------------------------------------
      /// CopyDelegate class - DynamicModel specialisation
      /// ---------------------------------------------------------------------

      template<typename UnusedUniqueT, typename RelatedStorableT,
          typename DbValT>
        CopyDelegate< DynamicModel, DynamicStorable, UnusedUniqueT,
            RelatedStorableT, DbValT >::CopyDelegate(PropT& srcProp_) :
          srcProp(srcProp_)
        {
        }
      /// ---------------------------------------------------------------------

      template<typename UnusedUniqueT, typename RelatedStorableT,
          typename DbValT>
        AttrI::PtrT
        CopyDelegate< DynamicModel, DynamicStorable, UnusedUniqueT,
            RelatedStorableT, DbValT >::operator()(DynamicStorable& dstStorable,
            AttrI::WptrT attrMeta)
        {
          // RelatedProperty won't have been setup. And it probably will never work
          // with the dynamic model anyway.
          assert(false);
          std::auto_ptr < PropT > propPtr;
          propPtr.reset(new PropT());
          propPtr->setup(&dstStorable);
          propPtr->mAttrMetaDelegate.setAttrMeta(attrMeta);
          AttrI::PtrT ptr(propPtr.release());
          return ptr;
        }
      /// ---------------------------------------------------------------------

      /// ---------------------------------------------------------------------
      /// AttrMetaDelegate class
      /// ---------------------------------------------------------------------

      /// ---------------
      /// Initialisers

      template<typename UnusedUniqueT, typename DbValT>
        std::string AttrMetaDelegate< UnusedUniqueT, DbValT >::sFieldName
        __attribute__((init_priority(65000)));
      /// ---------------------------------------------------------------------

      template<typename UnusedUniqueT, typename DbValT>
        bool AttrMetaDelegate< UnusedUniqueT, DbValT >::sIsPrimaryKey = false;
      /// ---------------------------------------------------------------------

      template<typename UnusedUniqueT, typename DbValT>
        bool AttrMetaDelegate< UnusedUniqueT, DbValT >::sIsValidIfNull = true;
      /// ---------------------------------------------------------------------

      template<typename UnusedUniqueT, typename DbValT>
        boost::optional< DbValT >
            AttrMetaDelegate< UnusedUniqueT, DbValT >::sDefaultValue
            __attribute__((init_priority(65000)));
      /// ---------------------------------------------------------------------

      /// ---------------
      /// AttrMetaI interface

      template<typename UnusedUniqueT, typename DbValT>
        const std::string&
        AttrMetaDelegate< UnusedUniqueT, DbValT >::FieldName() const
        {
          return sFieldName;
        }
      /// ---------------------------------------------------------------------

      template<typename UnusedUniqueT, typename DbValT>
        bool
        AttrMetaDelegate< UnusedUniqueT, DbValT >::IsPrimaryKey() const
        {
          return sIsPrimaryKey;
        }
      /// ---------------------------------------------------------------------

      template<typename UnusedUniqueT, typename DbValT>
        bool
        AttrMetaDelegate< UnusedUniqueT, DbValT >::IsValidIfNull() const
        {
          return sIsValidIfNull;
        }
      /// ---------------------------------------------------------------------

      template<typename UnusedUniqueT, typename DbValT>
        boost::optional< DbValT >
        AttrMetaDelegate< UnusedUniqueT, DbValT >::DefaultValue() const
        {
          return sDefaultValue;
        }
      /// ---------------------------------------------------------------------

      /// ---------------
      /// Setters

      template<typename UnusedUniqueT, typename DbValT>
        void
        AttrMetaDelegate< UnusedUniqueT, DbValT >::setup(const std::string fieldName,
            const bool isPrimaryKey,
            const bool isValidIfNull,
            const boost::optional< DbValT > defaultValue)
        {
          sFieldName = fieldName;
          sIsPrimaryKey = isPrimaryKey;
          sIsValidIfNull = isValidIfNull;
          sDefaultValue = defaultValue;
        }
      /// ---------------------------------------------------------------------

      // Compiler thinks this is a specialisation of the specialised class,
      // not the implementation for the parially specialised class
      // Not sure if there is a way around that.

      //      /// ---------------------------------------------------------------------
      //      /// AttrMetaDelegate class (DynamicModal specilisation)
      //      /// ---------------------------------------------------------------------
      //
      //      template<>
      //        AttrMetaDelegate< DynamicStorable >::AttrMetaDelegate() :
      //          mIsPrimaryKey(false), mIsValidIfNull(false)
      //        {
      //        }
      //      /// ---------------------------------------------------------------------
      //
      //      /// ---------------
      //      /// Special setter for Dynamic specialisation
      //
      //      template<>
      //        void
      //        AttrMetaDelegate< DynamicStorable >::setAttrMeta(AttrI::WptrT attrMeta)
      //        {
      //          mAttrMeta = attrMeta;
      //        }
      //      /// ---------------------------------------------------------------------
      //
      //      /// ---------------
      //      /// AttrMetaI interface
      //
      //      template<>
      //        const std::string&
      //        AttrMetaDelegate< DynamicStorable >::FieldName() const
      //        {
      //          AttrI::PtrT attr = mAttrMeta.lock();
      //          if (attr.get() != NULL)
      //            return attr->FieldName();
      //
      //          return mFieldName;
      //        }
      //      /// ---------------------------------------------------------------------
      //
      //      template<>
      //        bool
      //        AttrMetaDelegate< DynamicStorable >::IsPrimaryKey() const
      //        {
      //          AttrI::PtrT attr = mAttrMeta.lock();
      //          if (attr.get() != NULL)
      //            return attr->IsPrimaryKey();
      //
      //          return mIsPrimaryKey;
      //        }
      //      /// ---------------------------------------------------------------------
      //
      //      template<>
      //        bool
      //        AttrMetaDelegate< DynamicStorable >::IsValidIfNull() const
      //        {
      //          AttrI::PtrT attr = mAttrMeta.lock();
      //          if (attr.get() != NULL)
      //            return attr->IsValidIfNull();
      //
      //          return mIsValidIfNull;
      //        }
      //      /// ---------------------------------------------------------------------
      //
      //      /// ---------------
      //      /// Setters
      //
      //      template<>
      //        void
      //        AttrMetaDelegate< DynamicStorable >::setup(const std::string fieldName,
      //            const bool isPrimaryKey,
      //            const bool isValidIfNull)
      //        {
      //          assert(mAttrMeta.lock().get() == NULL);
      //          mFieldName = fieldName;
      //          mIsPrimaryKey = isPrimaryKey;
      //          mIsValidIfNull = isValidIfNull;
      //        }
      //      /// ---------------------------------------------------------------------

      /// ---------------------------------------------------------------------
      // Partial specialisation for std::string
      /// ---------------------------------------------------------------------

      template<typename ModelT, typename StorableT, typename UnusedUniqueT,
          typename RelatedStorableT, typename DbValT>
        BindGetDelegate< ModelT, StorableT, UnusedUniqueT, RelatedStorableT,
            DbValT >::BindGetDelegate(RelatedProperty< ModelT, StorableT,
            UnusedUniqueT, RelatedStorableT, DbValT >& prop)
        {
          // Must partially speclialise
          assert(false);
        }
      /// ---------------------------------------------------------------------


      template<typename ModelT, typename StorableT, typename UnusedUniqueT,
          typename RelatedStorableT, typename DbValT>
        bool
        BindGetDelegate< ModelT, StorableT, UnusedUniqueT, RelatedStorableT,
            DbValT >::Bind(QSqlQuery& qry, const int& bindIndex)
        {
          // Must partially speclialise
          assert(false);
          return false;
        }
      /// ---------------------------------------------------------------------

      template<typename ModelT, typename StorableT, typename UnusedUniqueT,
          typename RelatedStorableT, typename DbValT>
        bool
        BindGetDelegate< ModelT, StorableT, UnusedUniqueT, RelatedStorableT,
            DbValT >::Get(QSqlQuery& qry, int fieldIndex, QVariant& value)
        {
          // Must partially speclialise
          assert(false);
          return false;
        }
      /// ---------------------------------------------------------------------

      template<typename ModelT, typename StorableT, typename UnusedUniqueT,
          typename RelatedStorableT, typename DbValT>
        const DbValT&
        BindGetDelegate< ModelT, StorableT, UnusedUniqueT, RelatedStorableT,
            DbValT >::InitStepDbValue() const
        {
          // Must partially speclialise
          assert(false);
          return void();
        }
      /// ---------------------------------------------------------------------

      template<typename ModelT, typename StorableT, typename UnusedUniqueT,
          typename RelatedStorableT, typename DbValT>
        bool
        BindGetDelegate< ModelT, StorableT, UnusedUniqueT, RelatedStorableT,
            DbValT >::IsNull() const
        {
          // Must partially speclialise
          assert(false);
          return void();
        }
      /// ---------------------------------------------------------------------

      template<typename ModelT, typename StorableT, typename UnusedUniqueT,
          typename RelatedStorableT, typename DbValT>
        bool
        BindGetDelegate< ModelT, StorableT, UnusedUniqueT, RelatedStorableT,
            DbValT >::initialiseModel()
        {
          // Must partially speclialise
          assert(false);
          return false;
        }
      /// ---------------------------------------------------------------------

      /// ---------------------------------------------------------------------
      // Partial specialisation for std::string
      /// ---------------------------------------------------------------------

      template<typename ModelT, typename StorableT, typename UnusedUniqueT,
          typename RelatedStorableT>
        BindGetDelegate< ModelT, StorableT, UnusedUniqueT, RelatedStorableT,
            std::string >::BindGetDelegate(RelatedProperty< ModelT, StorableT,
            UnusedUniqueT, RelatedStorableT, std::string >& prop) :
          mProp(prop), mIsNull(true)
        {
        }
      /// ---------------------------------------------------------------------

      template<typename ModelT, typename StorableT, typename UnusedUniqueT,
          typename RelatedStorableT>
        bool
        BindGetDelegate< ModelT, StorableT, UnusedUniqueT, RelatedStorableT,
            std::string >::Bind(QSqlQuery& qry, const int& bindIndex)
        {
          RelatedStorableT& obj = *mProp.mObjectValue;
          const std::string& str = (obj.*mProp.sDbValGetter)();
          qry.bindValue(bindIndex, str.c_str());
          return true;
        }
      /// ---------------------------------------------------------------------

      template<typename ModelT, typename StorableT, typename UnusedUniqueT,
          typename RelatedStorableT>
        bool
        BindGetDelegate< ModelT, StorableT, UnusedUniqueT, RelatedStorableT,
            std::string >::Get(QSqlQuery& qry, int fieldIndex, QVariant& value)
        {
          mDbValue.reset(new std::string(value.toString().toStdString()));
          mIsNull = false;
          return true;
        }
      /// ---------------------------------------------------------------------

      template<typename ModelT, typename StorableT, typename UnusedUniqueT,
          typename RelatedStorableT>
        const std::string&
        BindGetDelegate< ModelT, StorableT, UnusedUniqueT, RelatedStorableT,
            std::string >::InitStepDbValue() const
        {
          if (mDbValue.get() == NULL)
            return sEmptyValue;

          return *mDbValue;
        }
      /// ---------------------------------------------------------------------

      template<typename ModelT, typename StorableT, typename UnusedUniqueT,
          typename RelatedStorableT>
        bool
        BindGetDelegate< ModelT, StorableT, UnusedUniqueT, RelatedStorableT,
            std::string >::IsNull() const
        {
          return mIsNull;
        }
      /// ---------------------------------------------------------------------

      template<typename ModelT, typename StorableT, typename UnusedUniqueT,
          typename RelatedStorableT>
        bool
        BindGetDelegate< ModelT, StorableT, UnusedUniqueT, RelatedStorableT,
            std::string >::initialiseModel()
        {
          if (mIsNull)
          {
            mDbValue.reset();
            return true;
          }

          // Null value in the database is a null value here.
          boost::shared_ptr < ModelT > model = mProp.mStorable->model().lock();
          assert(model.get());

          assert(mDbValue.get());
          mProp.mObjectValue = ((*model).*mProp.sObjectGetter)(*mDbValue);

          if (mProp.mObjectValue.get() == NULL)
          {
            std::string
                dstName = RelatedStorableT().config()->sqlOpts()->name();
            std::string srcName = StorableT().config()->sqlOpts()->name();

            boost::format info("Failed to get/convert database value %s"
              " to object of type %s.\n"
              "Relating from object type %s.");
            info % *mDbValue % dstName;
            info % srcName;

            throw RelatedObjectGetException(info.str());
          }

          mDbValue.reset();
          mIsNull = true;
          return true;
        }
      /// ---------------------------------------------------------------------

      /// ---------------------------------------------------------------------
      // Partial specialisation for uint64_t
      /// ---------------------------------------------------------------------
      template<typename ModelT, typename StorableT, typename UnusedUniqueT,
          typename RelatedStorableT>
        BindGetDelegate< ModelT, StorableT, UnusedUniqueT, RelatedStorableT,
            uint64_t >::BindGetDelegate(RelatedProperty< ModelT, StorableT,
            UnusedUniqueT, RelatedStorableT, uint64_t >& prop) :
          mProp(prop), mIsNull(true)
        {
        }
      /// ---------------------------------------------------------------------

      template<typename ModelT, typename StorableT, typename UnusedUniqueT,
          typename RelatedStorableT>
        bool
        BindGetDelegate< ModelT, StorableT, UnusedUniqueT, RelatedStorableT,
            uint64_t >::Bind(QSqlQuery& qry, const int& bindIndex)
        {
          RelatedStorableT& obj = *mProp.mObjectValue;
          mDbValue = (obj.*mProp.sDbValGetter)();
          qry.bindValue(bindIndex, mDbValue);
          return true;
        }
      /// ---------------------------------------------------------------------

      template<typename ModelT, typename StorableT, typename UnusedUniqueT,
          typename RelatedStorableT>
        bool
        BindGetDelegate< ModelT, StorableT, UnusedUniqueT, RelatedStorableT,
            uint64_t >::Get(QSqlQuery& qry, int fieldIndex, QVariant& value)
        {
          bool ok = false;
          mDbValue = value.toULongLong(&ok);

          if (!ok)
            qCritical("Unable to convert value \"%s\" to uint64_t for field %s",
                qPrintable(value.toString()),
                qPrintable(qry.record().fieldName(fieldIndex)));

          mIsNull = false;
          return ok;
        }
      /// ---------------------------------------------------------------------

      template<typename ModelT, typename StorableT, typename UnusedUniqueT,
          typename RelatedStorableT>
        const uint64_t&
        BindGetDelegate< ModelT, StorableT, UnusedUniqueT, RelatedStorableT,
            uint64_t >::InitStepDbValue() const
        {
          return mDbValue;
        }
      /// ---------------------------------------------------------------------

      template<typename ModelT, typename StorableT, typename UnusedUniqueT,
          typename RelatedStorableT>
        bool
        BindGetDelegate< ModelT, StorableT, UnusedUniqueT, RelatedStorableT,
            uint64_t >::IsNull() const
        {
          return mIsNull;
        }
      /// ---------------------------------------------------------------------

      template<typename ModelT, typename StorableT, typename UnusedUniqueT,
          typename RelatedStorableT>
        bool
        BindGetDelegate< ModelT, StorableT, UnusedUniqueT, RelatedStorableT,
            uint64_t >::initialiseModel()
        {
          if (mIsNull)
            return true;

          boost::shared_ptr < ModelT > model = mProp.mStorable->model().lock();
          assert(model.get());
          mProp.mObjectValue = ((*model).*mProp.sObjectGetter)(mDbValue);

          if (mProp.mObjectValue.get() == NULL)
          {
            std::string
                dstName = RelatedStorableT().config()->sqlOpts()->name();
            std::string srcName = StorableT().config()->sqlOpts()->name();

            std::ostringstream valStr;
            valStr.setf(std::ios_base::dec);
            valStr << mDbValue;

            boost::format info("Failed to get/convert database value %s"
              " to object of type %s.\n"
              "Relating from object type %s.");
            info % valStr.str() % dstName;
            info % srcName;

            throw RelatedObjectGetException(info.str());
          }

          mIsNull = true;
          return true;
        }
      /// ---------------------------------------------------------------------

      /// ---------------------------------------------------------------------
      // Partial specialisation for int64_t
      /// ---------------------------------------------------------------------
      template<typename ModelT, typename StorableT, typename UnusedUniqueT,
          typename RelatedStorableT>
        BindGetDelegate< ModelT, StorableT, UnusedUniqueT, RelatedStorableT,
            int64_t >::BindGetDelegate(RelatedProperty< ModelT, StorableT,
            UnusedUniqueT, RelatedStorableT, int64_t >& prop) :
          mProp(prop), mIsNull(true)
        {
        }
      /// ---------------------------------------------------------------------

      template<typename ModelT, typename StorableT, typename UnusedUniqueT,
          typename RelatedStorableT>
        bool
        BindGetDelegate< ModelT, StorableT, UnusedUniqueT, RelatedStorableT,
            int64_t >::Bind(QSqlQuery& qry, const int& bindIndex)
        {
          RelatedStorableT& obj = *mProp.mObjectValue;
          mDbValue = (obj.*mProp.sDbValGetter)();
          qry.bindValue(bindIndex, mDbValue);
          return true;
        }
      /// ---------------------------------------------------------------------

      template<typename ModelT, typename StorableT, typename UnusedUniqueT,
          typename RelatedStorableT>
        bool
        BindGetDelegate< ModelT, StorableT, UnusedUniqueT, RelatedStorableT,
            int64_t >::Get(QSqlQuery& qry, int fieldIndex, QVariant& value)
        {
          bool ok = false;
          mDbValue = value.toLongLong(&ok);

          if (!ok)
            qCritical("Unable to convert value \"%s\" to int64_t for field %s",
                qPrintable(value.toString()),
                qPrintable(qry.record().fieldName(fieldIndex)));

          mIsNull = false;
          return ok;
        }
      /// ---------------------------------------------------------------------

      template<typename ModelT, typename StorableT, typename UnusedUniqueT,
          typename RelatedStorableT>
        const int64_t&
        BindGetDelegate< ModelT, StorableT, UnusedUniqueT, RelatedStorableT,
            int64_t >::InitStepDbValue() const
        {
          return mDbValue;
        }
      /// ---------------------------------------------------------------------

      template<typename ModelT, typename StorableT, typename UnusedUniqueT,
          typename RelatedStorableT>
        bool
        BindGetDelegate< ModelT, StorableT, UnusedUniqueT, RelatedStorableT,
            int64_t >::IsNull() const
        {
          return mIsNull;
        }
      /// ---------------------------------------------------------------------

      template<typename ModelT, typename StorableT, typename UnusedUniqueT,
          typename RelatedStorableT>
        bool
        BindGetDelegate< ModelT, StorableT, UnusedUniqueT, RelatedStorableT,
            int64_t >::initialiseModel()
        {
          if (mIsNull)
            return true;

          boost::shared_ptr < ModelT > model = mProp.mStorable->model().lock();
          assert(model.get());
          mProp.mObjectValue = ((*model).*mProp.sObjectGetter)(mDbValue);

          if (mProp.mObjectValue.get() == NULL)
          {
            std::string
                dstName = RelatedStorableT().config()->sqlOpts()->name();
            std::string srcName = StorableT().config()->sqlOpts()->name();

            std::ostringstream valStr;
            valStr.setf(std::ios_base::dec);
            valStr << mDbValue;

            boost::format info("Failed to get/convert database value %s"
              " to object of type %s.\n"
              "Relating from object type %s.");
            info % valStr.str() % dstName;
            info % srcName;

            throw RelatedObjectGetException(info.str());
          }

          mIsNull = true;
          return true;
        }
      /// ---------------------------------------------------------------------

      /// ---------------------------------------------------------------------
      // Partial specialisation for int32_t
      /// ---------------------------------------------------------------------
      template<typename ModelT, typename StorableT, typename UnusedUniqueT,
          typename RelatedStorableT>
        BindGetDelegate< ModelT, StorableT, UnusedUniqueT, RelatedStorableT,
            int32_t >::BindGetDelegate(RelatedProperty< ModelT, StorableT,
            UnusedUniqueT, RelatedStorableT, int32_t >& prop) :
          mProp(prop), mIsNull(true)
        {
        }
      /// ---------------------------------------------------------------------

      template<typename ModelT, typename StorableT, typename UnusedUniqueT,
          typename RelatedStorableT>
        bool
        BindGetDelegate< ModelT, StorableT, UnusedUniqueT, RelatedStorableT,
            int32_t >::Bind(QSqlQuery& qry, const int& bindIndex)
        {
          RelatedStorableT& obj = *mProp.mObjectValue;
          mDbValue = (obj.*mProp.sDbValGetter)();
          qry.bindValue(bindIndex, mDbValue);
          return true;
        }
      /// ---------------------------------------------------------------------

      template<typename ModelT, typename StorableT, typename UnusedUniqueT,
          typename RelatedStorableT>
        bool
        BindGetDelegate< ModelT, StorableT, UnusedUniqueT, RelatedStorableT,
            int32_t >::Get(QSqlQuery& qry, int fieldIndex, QVariant& value)
        {
          bool ok = false;
          mDbValue = value.toInt(&ok);

          if (!ok)
            qCritical("Unable to convert value \"%s\" to int32_t for field %s",
                qPrintable(value.toString()),
                qPrintable(qry.record().fieldName(fieldIndex)));

          mIsNull = false;
          return ok;
        }
      /// ---------------------------------------------------------------------

      template<typename ModelT, typename StorableT, typename UnusedUniqueT,
          typename RelatedStorableT>
        const int32_t&
        BindGetDelegate< ModelT, StorableT, UnusedUniqueT, RelatedStorableT,
            int32_t >::InitStepDbValue() const
        {
          return mDbValue;
        }
      /// ---------------------------------------------------------------------

      template<typename ModelT, typename StorableT, typename UnusedUniqueT,
          typename RelatedStorableT>
        bool
        BindGetDelegate< ModelT, StorableT, UnusedUniqueT, RelatedStorableT,
            int32_t >::IsNull() const
        {
          return mIsNull;
        }
      /// ---------------------------------------------------------------------

      template<typename ModelT, typename StorableT, typename UnusedUniqueT,
          typename RelatedStorableT>
        bool
        BindGetDelegate< ModelT, StorableT, UnusedUniqueT, RelatedStorableT,
            int32_t >::initialiseModel()
        {
          if (mIsNull)
            return true;

          boost::shared_ptr < ModelT > model = mProp.mStorable->model().lock();
          assert(model.get());
          mProp.mObjectValue = ((*model).*mProp.sObjectGetter)(mDbValue);

          if (mProp.mObjectValue.get() == NULL)
          {
            std::string
                dstName = RelatedStorableT().config()->sqlOpts()->name();
            std::string srcName = StorableT().config()->sqlOpts()->name();

            std::ostringstream valStr;
            valStr.setf(std::ios_base::dec);
            valStr << mDbValue;

            boost::format info("Failed to get/convert database value %s"
              " to object of type %s.\n"
              "Relating from object type %s.");
            info % valStr.str() % dstName;
            info % srcName;

            throw RelatedObjectGetException(info.str());
          }

          mIsNull = true;
          return true;
        }
      /// ---------------------------------------------------------------------

      /// ---------------------------------------------------------------------
      // Partial specialisation for int16_t
      /// ---------------------------------------------------------------------
      template<typename ModelT, typename StorableT, typename UnusedUniqueT,
          typename RelatedStorableT>
        BindGetDelegate< ModelT, StorableT, UnusedUniqueT, RelatedStorableT,
            int16_t >::BindGetDelegate(RelatedProperty< ModelT, StorableT,
            UnusedUniqueT, RelatedStorableT, int16_t >& prop) :
          mProp(prop), mIsNull(true)
        {
        }
      /// ---------------------------------------------------------------------

      template<typename ModelT, typename StorableT, typename UnusedUniqueT,
          typename RelatedStorableT>
        bool
        BindGetDelegate< ModelT, StorableT, UnusedUniqueT, RelatedStorableT,
            int16_t >::Bind(QSqlQuery& qry, const int& bindIndex)
        {
          RelatedStorableT& obj = *mProp.mObjectValue;
          mDbValue = (obj.*mProp.sDbValGetter)();
          qry.bindValue(bindIndex, mDbValue);
          return true;
        }
      /// ---------------------------------------------------------------------

      template<typename ModelT, typename StorableT, typename UnusedUniqueT,
          typename RelatedStorableT>
        bool
        BindGetDelegate< ModelT, StorableT, UnusedUniqueT, RelatedStorableT,
            int16_t >::Get(QSqlQuery& qry, int fieldIndex, QVariant& value)
        {
          bool ok = false;
          mDbValue = value.toInt(&ok);

          if (!ok)
            qCritical("Unable to convert value \"%s\" to int16_t for field %s",
                qPrintable(value.toString()),
                qPrintable(qry.record().fieldName(fieldIndex)));

          mIsNull = false;
          return ok;
        }
      /// ---------------------------------------------------------------------

      template<typename ModelT, typename StorableT, typename UnusedUniqueT,
          typename RelatedStorableT>
        const int16_t&
        BindGetDelegate< ModelT, StorableT, UnusedUniqueT, RelatedStorableT,
            int16_t >::InitStepDbValue() const
        {
          return mDbValue;
        }
      /// ---------------------------------------------------------------------

      template<typename ModelT, typename StorableT, typename UnusedUniqueT,
          typename RelatedStorableT>
        bool
        BindGetDelegate< ModelT, StorableT, UnusedUniqueT, RelatedStorableT,
            int16_t >::IsNull() const
        {
          return mIsNull;
        }
      /// ---------------------------------------------------------------------

      template<typename ModelT, typename StorableT, typename UnusedUniqueT,
          typename RelatedStorableT>
        bool
        BindGetDelegate< ModelT, StorableT, UnusedUniqueT, RelatedStorableT,
            int16_t >::initialiseModel()
        {
          if (mIsNull)
            return true;

          boost::shared_ptr < ModelT > model = mProp.mStorable->model().lock();
          assert(model.get());
          mProp.mObjectValue = ((*model).*mProp.sObjectGetter)(mDbValue);

          if (mProp.mObjectValue.get() == NULL)
          {
            std::string
                dstName = RelatedStorableT().config()->sqlOpts()->name();
            std::string srcName = StorableT().config()->sqlOpts()->name();

            std::ostringstream valStr;
            valStr.setf(std::ios_base::dec);
            valStr << mDbValue;

            boost::format info("Failed to get/convert database value %s"
              " to object of type %s.\n"
              "Relating from object type %s.");
            info % valStr.str() % dstName;
            info % srcName;

            throw RelatedObjectGetException(info.str());
          }

          mIsNull = true;
          return true;
        }
    /// ---------------------------------------------------------------------

    }

  }

}
