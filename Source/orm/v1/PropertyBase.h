/*
 * Attribute.h
 *
 *  Created on: Dec 17, 2010
 *      Author: Jarrod Chesney
 */

#ifndef _ORM_V1_SIMPLE_PROPERTY_BASE_H_
#define _ORM_V1_SIMPLE_PROPERTY_BASE_H_

#include <string>
#include <assert.h>
#include <stdint.h>

#include <boost/weak_ptr.hpp>

#include <QtCore/qvariant.h>

#include "Storable.h"

#include "Attribute.h"
#include "MvLink.h"
#include "PropertyPrivate.h"

namespace orm
{
  namespace v1
  {

    template<typename ModelT, class StorableT, typename UnusedUniqueT,
        typename TypeT, typename PropT>
      class PropertyBase : public PropertyI< TypeT > , public AttrI
      {
        public:
          PropertyBase();

          void
          setup(StorableT* storable);

          /*! Setup Static
           * Setup the static variables and return and instance of this
           * @param fieldName
           * @param isPrimaryKey
           * @param isValidIfNull
           * @return a new instance
           */
          static AttrI::PtrT
          setupStatic(const std::string fieldName,
              const bool isPrimaryKey = false,
              const bool isValidIfNull = true,
              const boost::optional< TypeT > defaultValue = boost::optional<
                  TypeT >());

          const TypeT&
          get() const;

          void
          set(const TypeT& value);

          bool
          isNull() const;

          void
          setNull();

          void
          setDefault();

          boost::optional< TypeT >
          defaultValue() const;

        protected:
          /// ---------------
          /// AttrMetaI interface

          const std::string&
          FieldName() const;

          bool
          IsPrimaryKey() const;

          bool
          IsValidIfNull() const;

        protected:
          /// ---------------
          /// AttributeI interface

          virtual bool
          Bind(QSqlQuery& qry, const int& bindIndex) = 0;

          virtual bool
          Get(QSqlQuery& qry, int fieldIndex) = 0;

          virtual bool
          initialiseModel(const int pass);

          virtual void
          setup();

          virtual void
          tearDown();

          AttrI::PtrT
          createCopy(DynamicStorable& storable, AttrI::WptrT attrMeta);

          bool
          isValid() const;

          std::string
          invalidReason() const;

          StorableConfigI&
          storableConfig();

          virtual boost::shared_ptr< MvLinkI >
          mvLink();

        protected:
          friend struct __property_private::CopyDelegate< ModelT, StorableT,
              UnusedUniqueT, TypeT, PropT >;

          __property_private ::AttrMetaDelegate< UnusedUniqueT, TypeT >
              mAttrMetaDelegate;

          bool mIsNull;
          bool mIsInitialised;

          TypeT mValue;
          StorableConfigI* mStorableConfig;
      };
  }
}

#ifndef USE_ORM_V1_DLL
#include "PropertyBase.ini"
#endif

#endif /* _ORM_V1_SIMPLE_PROPERTY_BASE_H_ */
