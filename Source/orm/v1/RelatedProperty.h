/*
 * IsAttribute.h
 *
 *  Created on: Dec 20, 2010
 *      Author: Jarrod Chesney
 */

#ifndef _ORM_RELATED_PROPERTY_H_
#define _ORM_RELATED_PROPERTY_H_

#include <auto_ptr.h>
#include <string>

#include <QtSql/qsqlrecord.h>

#include <boost/shared_ptr.hpp>
#include <boost/optional.hpp>

#include "Attribute.h"
#include "MvLink.h"
#include "RelatedPropertyPrivate.h"
#include "RelatedToManyProperty.h"
#include "breakpoint.h"

namespace orm
{
  namespace v1
  {

    template<typename ModelT>
      class Model;

    /*!  Related Propery
     * This class handels the relationships between objects in an orm model.
     * Its value is a shared pointer to an object while it reads and writes
     * to the database in a string, int, etc value.
     *
     * The storable for which this property belongs will need to initiliase
     * this property using the InitStepGetString and InitStepSetObjectt functions.
     * The set function can only be called once.
     */
    template<typename ModelT, class StorableT, typename UnusedUniqueT,
        class RelatedStorableT, typename DbValT>
      class RelatedProperty : private PropertyI< boost::shared_ptr<
          RelatedStorableT > > , public AttrI
      {
          typedef RelatedProperty< ModelT, StorableT, UnusedUniqueT,
              RelatedStorableT, DbValT > ThisT;

        public:
          typedef __related_property_private ::RelatedToManyVisitor< ModelT,
              StorableT, RelatedStorableT > RelatedToManyVisitorT;

          typedef RelatedToManyProperty< ModelT, RelatedStorableT, StorableT >
              RelatedToManyPropertyT;

          typedef DbValT
          (RelatedStorableT::*DbValueGetterT)() const;

          typedef boost::shared_ptr< RelatedStorableT >
          (ModelT::*ObjectGetterT)(const DbValT&);

          typedef void
              (RelatedStorableT::*RelatedToManyVisitFunctionT)(RelatedToManyVisitorT&);

        public:
          RelatedProperty();

          virtual
          ~RelatedProperty();

          /*! Setup
           *
           * @param storable the storable instance for which this property belongs
           * @param dbValGetter a member function of the object which returns the
           * value to store to the db.
           * @param isPrimaryKey indicates this property represents a primary key
           * in the database.
           */
          void
          setup(StorableT* storable);

          /*! Setup Static
           * Setup the static variables and return and instance of this
           * @param fieldName
           * @param dbValGetter
           * @param objectGetter
           * @param relatedToManyVisitFunction
           * @param isPrimaryKey
           * @param isValidIfNull
           * @return a new instance
           */
          static AttrI::PtrT
              setupStatic(const std::string fieldName,
                  DbValueGetterT dbValGetter,
                  ObjectGetterT objectGetter,
                  RelatedToManyVisitFunctionT relatedToManyVisitFunction = NULL,
                  bool isPrimaryKey = false,
                  bool isValidIfNull = true,
                  boost::optional< DbValT > defaultValue = boost::optional<
                      DbValT >());

        public:

          const boost::shared_ptr< RelatedStorableT >&
          get() const;

          void
              set(const DbValT& value,
                  boost::shared_ptr< ModelT > supplementaryModel = boost::shared_ptr<
                      ModelT >());

          void
          set(const boost::shared_ptr< RelatedStorableT >& value);

          bool
          isNull() const;

          void
          setNull();

          void
          setNull(bool reset);

          void
          setDefault();

          boost::optional< boost::shared_ptr< RelatedStorableT > >
          defaultValue() const;

        private:
          /// ---------------
          /// AttrMetaI interface

          const std::string&
          FieldName() const;

          bool
          IsPrimaryKey() const;

          bool
          IsValidIfNull() const;

        public:

          const DbValT&
          InitStepDbValue() const;

        private:
          bool
          Bind(QSqlQuery& qry, const int& bindIndex);

          bool
          Get(QSqlQuery& qry, int fieldIndex);

          virtual bool
          initialiseModel(const int pass);

          virtual void
          setup();

          virtual void
          tearDown();

          AttrI::PtrT
          createCopy(DynamicStorable& storable, AttrI::WptrT attrMeta);

          bool
          isValid() const;

          std::string
          invalidReason() const;

          StorableConfigI&
          storableConfig();

          virtual boost::shared_ptr< MvLinkI >
          mvLink();

          boost::shared_ptr< RelatedStorableT >
              get(const DbValT& value,
                  boost::shared_ptr< ModelT > supplementaryModel = boost::shared_ptr<
                      ModelT >()) const;

        private:
          typedef __related_property_private ::BindGetDelegate< ModelT,
              StorableT, UnusedUniqueT, RelatedStorableT, DbValT > BindGetterT;
          friend struct __related_property_private::BindGetDelegate< ModelT,
              StorableT, UnusedUniqueT, RelatedStorableT, DbValT >;

          typedef __related_property_private ::CopyDelegate< ModelT, StorableT,
              UnusedUniqueT, RelatedStorableT, DbValT > CopierT;
          friend class __related_property_private::CopyDelegate< ModelT,
              StorableT, UnusedUniqueT, RelatedStorableT, DbValT >;

          typedef boost::shared_ptr< RelatedStorableT > RelatedStorablePtrT;

          RelatedStorablePtrT mObjectValue;

          /*! Is Null
           * This flag can be set with setNull to indicate that the value is
           * null with out removing the value. This is usefull for primary
           * keys where Storage<> needs to know what the pk was to delete this.
           */
          bool mIsNull;
          bool mIsInitialised;

          BindGetterT mBindGetDelegate;

          __related_property_private ::AttrMetaDelegate< UnusedUniqueT, DbValT >
              mAttrMetaDelegate;

          /*! Related To Many
           * Stores reference to related to from many property.
           * This property is a member of the relted object and stores a list
           * of objects that are relating to it.
           * It will be on the stack of a storable pointed to by mObjectValue
           * this means that it will exist as long as mObjectValue referes to
           * it.
           */
          RelatedToManyPropertyT* mRelatedToMany;

          StorableT* mStorable;
          StorableConfigI* mStorableConfig;

          static DbValueGetterT sDbValGetter;
          static ObjectGetterT sObjectGetter;
          static RelatedToManyVisitFunctionT sRelatedToManyVisitFunction;
      };
  }
}

//! Adds the orm property to the class using a custom type
#define ORM_V1_DECLARE_RELATED_PROPERTY(_get_name_, _name_, _db_val_type_, _obj_type_) \
           /* BEGIN _name_ RELATED PROPERTY */ \
           private: \
             class __ ## _name_ ## UniqueProp; \
           \
           public: \
             typedef orm::v1::RelatedProperty< \
                 ModelT \
                 , ThisStorableT \
                 , __ ## _name_ ## UniqueProp \
                 , _obj_type_ \
                 , _db_val_type_  \
                 > _name_ ## PropT; \
             \
             virtual void set ## _name_(const _db_val_type_& value, \
                 boost::shared_ptr< ModelT > supplementaryModel \
                  = boost::shared_ptr< ModelT >()); \
             \
             virtual void set ## _name_(const boost::shared_ptr < _obj_type_ >& value); \
             \
             virtual const boost::shared_ptr < _obj_type_ > _get_name_() const; \
             \
             virtual void set ## _name_ ## Null(bool reset = false); \
             \
             virtual bool is ## _name_ ## Null() const; \
             \
           private: \
             _name_ ## PropT mProp ## _name_; \
           /* END _name_ RELATED PROPERTY */

//! Adds the orm property to the class using a custom type
#define ORM_V1_DEFINE_RELATED_PROPERTY(_model_t_, _storable_t_, _get_name_, _name_, _db_val_type_, _obj_type_) \
           /* BEGIN _name_ RELATED PROPERTY */ \
             \
             void \
             _storable_t_::set ## _name_(const _db_val_type_& value, \
                 boost::shared_ptr< _model_t_ > supplementaryModel) \
             { \
               mProp ## _name_.set(value, supplementaryModel); \
             } \
             \
             void \
             _storable_t_::set ## _name_(const boost::shared_ptr < _obj_type_ >& value) \
             { \
               mProp ## _name_.set(value); \
             } \
             \
             const boost::shared_ptr < _obj_type_ > \
             _storable_t_::_get_name_() const \
             { \
               return mProp ## _name_.get(); \
             } \
             \
             void \
             _storable_t_::set ## _name_ ## Null(bool reset) \
             { \
               mProp ## _name_.setNull(reset); \
             } \
             \
             bool \
             _storable_t_::is ## _name_ ## Null() const \
             { \
               return mProp ## _name_.isNull(); \
             } \
             \
           /* END _name_ RELATED PROPERTY */

#ifndef USE_ORM_V1_DLL
#include "RelatedProperty.ini"
#endif

#endif /* _ORM_RELATED_PROPERTY_H_ */
