/*
 * Progress.h
 *
 *  Created on: Dec 17, 2010
 *      Author: Jarrod Chesney
 */

#ifndef _ORM_V1_PROGRESS_H_
#define _ORM_V1_PROGRESS_H_

#if defined(BUILD_ORM_V1_DLL)
#define DECL_ORM_V1_DIR __declspec(dllexport)
#elif defined(USE_ORM_V1_DLL)
#define DECL_ORM_V1_DIR __declspec(dllimport)
#else
#define DECL_ORM_V1_DIR
#endif

#include <stdint.h>
#include <string>
#include <vector>

#include <boost/shared_ptr.hpp>

namespace orm
{
  namespace v1
  {
    /*! Progress Brief Description
     * Long comment about the class
     *
     */
    class DECL_ORM_V1_DIR Progress
    {
      public:
        Progress();

        virtual
        ~Progress();

      public:
        void
        SetMax(uint32_t max);

        void
        Reset();

        void
        Incriment(uint32_t incriment = 1);

        void
        SetDescription(const std::string desc);

        Progress&
        AddSubProgress();

      private:
        typedef std::vector< boost::shared_ptr< Progress > > TdSubProcesses;
        std::vector< boost::shared_ptr< Progress > > mSubProgresses;
    };
  }
}

#endif /* _ORM_V1_PROGRESS_H_ */
