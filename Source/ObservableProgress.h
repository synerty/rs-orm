/*
 * ObservableProgress.h
 *
 *  Created on: Dec 17, 2010
 *      Author: Jarrod Chesney
 */

#ifndef _ORM_V1_OBSERVABLE_PROGRESS_H_
#define _ORM_V1_OBSERVABLE_PROGRESS_H_

#if defined(BUILD_ORM_V1_DLL)
#define DECL_ORM_V1_DIR __declspec(dllexport)
#elif defined(USE_ORM_V1_DLL)
#define DECL_ORM_V1_DIR __declspec(dllimport)
#else
#define DECL_ORM_V1_DIR
#endif

#include <stdint.h>
#include <string>
#include <set>

#include <boost/shared_ptr.hpp>

namespace orm
{
  namespace v1
  {
    class Progress;

    /*! ObservableProgress Brief Description
     * Long comment about the class
     *
     */
    class DECL_ORM_V1_DIR ObservableProgress
    {
      public:
        ObservableProgress();
        virtual
        ~ObservableProgress();

      public:
        void
        AddProgressObserver(boost::shared_ptr< Progress > progress);

        void
        RemoveProgressObserver(boost::shared_ptr< Progress > progress);

      public:
        void
        SetProgressMax(uint32_t max);

        void
        ProgressReset();

        void
        ProgressIncrement(uint32_t incriment = 1);

        void
        SetProgressDescription(const std::string desc);

      private:
        std::set< boost::shared_ptr< Progress > > mProgressObservers;
    };
  }
}

#endif /* _ORM_V1_OBSERVABLE_PROGRESS_H_ */
