/*
 * ObservableProgress.cpp
 *
 *  Created on: Dec 17, 2010
 *      Author: Jarrod Chesney
 */

#include "ObservableProgress.h"

#include "Progress.h"
namespace orm
{
  namespace v1
  {
    ObservableProgress::ObservableProgress()
    {
    }

    ObservableProgress::~ObservableProgress()
    {
    }
    // ----------------------------------------------------------------------------

    void
    ObservableProgress::AddProgressObserver(boost::shared_ptr< Progress > progress)
    {
      mProgressObservers.insert(progress);
    }
    // ----------------------------------------------------------------------------

    void
    ObservableProgress::RemoveProgressObserver(boost::shared_ptr< Progress > progress)
    {
      mProgressObservers.erase(progress);
    }
    // ----------------------------------------------------------------------------

    void
    ObservableProgress::SetProgressMax(uint32_t max)
    {
      for (std::set< boost::shared_ptr< Progress > >::iterator
          itr = mProgressObservers.begin();//
      itr != mProgressObservers.end(); //
      ++itr)
      {
        (*itr)->SetMax(max);
      }
    }
    // ----------------------------------------------------------------------------

    void
    ObservableProgress::ProgressReset()
    {
      for (std::set< boost::shared_ptr< Progress > >::iterator
          itr = mProgressObservers.begin();//
      itr != mProgressObservers.end(); //
      ++itr)
      {
        (*itr)->Reset();
      }
    }
    // ----------------------------------------------------------------------------

    void
    ObservableProgress::ProgressIncrement(uint32_t incriment)
    {
      for (std::set< boost::shared_ptr< Progress > >::iterator
          itr = mProgressObservers.begin();//
      itr != mProgressObservers.end(); //
      ++itr)
      {
        (*itr)->Incriment(incriment);
      }
    }
    // ----------------------------------------------------------------------------

    void
    ObservableProgress::SetProgressDescription(const std::string desc)
    {
      for (std::set< boost::shared_ptr< Progress > >::iterator
          itr = mProgressObservers.begin();//
      itr != mProgressObservers.end(); //
      ++itr)
      {
        (*itr)->SetDescription(desc);
      }
    }
  // ----------------------------------------------------------------------------

  }
}
